<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker.css') ?>" />
  <style>
      tr {
        line-height:33px;
        
      }
      ol {
        line-height:140%;
        margin-top:5px;
      }
      td{
        vertical-align:top;
      }
      th{
        text-align:center;
      }
      .ttd{
        text-align:center;
      }
      .kosong{
        height:80px;
      }
  </style>
</head>

<body>
  <section id="container">
   <!--main content start-->
      <div class="site-container">
        <div class="site-content">
          <section class="wrapper">
              <center><h3><b><u>SURAT TUGAS</u></b></h3></center>
              <center><h4>NO.<span style="margin-left:80px">/SLA/Sm/Srt</h4></center>
              <br>
              <center><h3><b>PEMBERITAHUAN KERJA KEPADA PIHAK KE III (EKSTERN)</b></h3></center>
              <center><h4>(SE No.6/80/INTERN Tgl.30 Desember 2004)</h4></center>
              <br><br>
              <table>
                <tr>
                  <td>Pemberi Kerja</td>
                  <td>:</td>
                  <td>Bank Indonesia Provinsi Jateng</td>
                </tr>
                <tr>
                  <td>Hari / Tanggal</td>
                  <td>:</td>
                  <td>$tanggal</td>
                </tr>
                <tr>
                  <td>Keterangan Pekerjaan </td>
                  <td>:</td>
                  <td>Pemeliharaan Kendaraan Dinas No Pol : $nopol</td>
                </tr>
                <tr>
                  <td>Pelaksana Pekerjaan </td>
                  <td>:</td>
                  <td>PT SIDODADI BERLIAN MOTORS</td>
                </tr>
                <tr>
                  <td>Rincian Pekerjaan </td>
                  <td>:</td>
                  <td><ol>
                        <li>$service</li>
                        <li>$service</li>
                        <li>$service</li>
                      </ol> 
                  </td>
                </tr>
                <tr>
                  <td>Pembayaran</td>
                  <td>:</td>
                  <td>Pembayaran akan kami laksanakan melaui pemindahbkuan ke rekening Saudara pada Bank yang ditunjuk dengan dilampiri surat tugas ini.</td>
                </tr>
                <tr>
                  <td>Pengawaswan Pekerjaan</td>
                  <td>:</td>
                  <td>Satuan layanan dan Administrasi<br>BI Provinsi Jateng.</td>
                </tr>
                <tr>
                  <td>Jangka Waktu Pelaksana</td>
                  <td>:</td>
                  <td>S/D selesai</td>
                </tr>
                <tr>
                  <td>Petugas Pelaksana</td>
                  <td>:</td>
                  <td>PT SIDODADI BERLIAN MOTORS</td>
                </tr>
              </table>
              <br><br>
              <table class="table table-bordered">
                <thead>
                  <th>NO</th>
                  <th>NAMA</th>
                  <th>JABATAN</th>
                  <th>KETERANGAN</th>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>SURADADI</td>
                    <td>Service Advisor</td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
              <br><br>
              <table class="col-sm-12"><td style="text-align:right">Semarang, $tanggal</td></table>
              <br><br><br><br>
              <table class="ttd col-sm-12">
                <tr>
                  <td width="25%">Verificator</td>
                  <td width="50%"></td>
                  <td width="25%">Pengguna</td>
                </tr>
                <tr class="kosong">
                  <td width="25%"></td>
                  <td width="50%"></td>
                  <td width="25%"></td>
                </tr>
                <tr>
                  <td width="25%">$nama</td>
                  <td width="50%"></td>
                  <td width="25%">$nama</td>
                </tr>
              </table>
              <br><br><br>
              <table class="col-sm-12">
                <td align="center">Mengetahui,</td>
              </table>
              <!-- <input class="col-md-12" type="textarea"> -->
            
            </section>

        </div>
        <!--footer start-->
        <footer class="site-footer">
          <?php $this->load->view("partials/footer.php") ?>
        </footer>
        <!--footer end-->
      </div>
    <!--main content end-->
  </section>
</body>

</html>