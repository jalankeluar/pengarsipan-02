<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Gedung_model extends CI_Model
{

  private $_table = "gedung";

  public function view()
  {
    return $this->db->query('SELECT * from gedung')->result();
  }

  public function save($data)
  {
    $this->db->insert($this->_table, $data);
  }

  public function update($where, $data, $_table)
  {
    $this->db->where($where);
    $this->db->update($_table, $data);
  }

  public function delete($id)
  {
    return $this->db->delete($this->_table, array("id" => $id));
  }

  public function all_gedung_count($role)
  {
    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->get('gedung');
    }
    else{
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->where('created_by',$nik)
        ->get('gedung');
    }

    return $query->num_rows();
  }

  public function all_gedung_data($limit, $start, $col, $dir, $role)
  {
    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('gedung.deleted_at',null)
      ->join('g_lantai','g_lantai.id = gedung.lantai','left')
      ->join('g_kerusakan','g_kerusakan.id = gedung.kerusakan','left')
      ->select('gedung.*, g_lantai.lantai as lantai_level, g_kerusakan.kerusakan_gedung')
      ->get('gedung');

    }
    else{

      $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('gedung.deleted_at',null)
      ->where('gedung.created_by',$nik)
      ->join('g_lantai','g_lantai.id = gedung.lantai','left')
      ->join('g_kerusakan','g_kerusakan.id = gedung.kerusakan','left')
      ->select('gedung.*, g_lantai.lantai as lantai_level, g_kerusakan.kerusakan_gedung')
      ->get('gedung');
    }
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_gedung_count($search,$role)
  {
    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->like('g_lantai.lantai', $search)
        ->or_like('g_kerusakan.kerusakan_gedung', $search)
        ->where('gedung.deleted_at',null)
        ->join('g_lantai','g_lantai.id = gedung.lantai','left')
        ->join('g_kerusakan','g_kerusakan.id = gedung.kerusakan','left')
        ->select('gedung.*, g_lantai.lantai as lantai_level, g_kerusakan.kerusakan_gedung')
        ->get('gedung');
    }
    else{
      $query = $this
        ->db
        ->like('g_lantai.lantai', $search)
        ->or_like('g_kerusakan.kerusakan_gedung', $search)
        ->where('gedung.deleted_at',null)
        ->where('gedung.created_by',$nik)
        ->join('g_lantai','g_lantai.id = gedung.lantai','left')
        ->join('g_kerusakan','g_kerusakan.id = gedung.kerusakan','left')
        ->select('gedung.*, g_lantai.lantai as lantai_level, g_kerusakan.kerusakan_gedung')
        ->get('gedung');
    }

    return $query->num_rows();
  }

  public function search_gedung_data($limit, $start, $col, $dir, $search, $role)
  {
    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->like('g_lantai.lantai', $search)
        ->or_like('g_kerusakan.kerusakan_gedung', $search)
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('gedung.deleted_at',null)
        ->join('g_lantai','g_lantai.id = gedung.lantai','left')
        ->join('g_kerusakan','g_kerusakan.id = gedung.kerusakan','left')
        ->select('gedung.*, g_lantai.lantai as lantai_level, g_kerusakan.kerusakan_gedung')
        ->get('gedung');
    }
    else{
      $query = $this
        ->db
        ->like('g_lantai.lantai', $search)
        ->or_like('g_kerusakan.kerusakan_gedung', $search)
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('gedung.deleted_at',null)
        ->where('gedung.created_by',$nik)
        ->join('g_lantai','g_lantai.id = gedung.lantai','left')
        ->join('g_kerusakan','g_kerusakan.id = gedung.kerusakan','left')
        ->select('gedung.*, g_lantai.lantai as lantai_level, g_kerusakan.kerusakan_gedung')
        ->get('gedung');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function all_lantai_count()
  {
    $query = $this
      ->db
      ->where('deleted_at',null)
      ->get('g_lantai');

    return $query->num_rows();
  }

  public function all_lantai_data($limit, $start, $col, $dir)
  {
    $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('g_lantai');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_lantai_count($search)
  {
    $query = $this
      ->db
      ->like('lantai', $search)
      ->where('deleted_at',null)
      ->get('g_lantai');

    return $query->num_rows();
  }

  public function search_lantai_data($limit, $start, $col, $dir, $search)
  {
    $query = $this
      ->db
      ->like('lantai', $search)
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('g_lantai');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }
  
  public function all_kerusakan_gedung_count()
  {
    $query = $this
      ->db
      ->where('deleted_at',null)
      ->get('g_kerusakan');

    return $query->num_rows();
  }

  public function all_kerusakan_gedung_data($limit, $start, $col, $dir)
  {
    $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('g_kerusakan');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_kerusakan_gedung_count($search)
  {
    $query = $this
      ->db
      ->like('kerusakan_gedung', $search)
      ->where('deleted_at',null)
      ->get('g_kerusakan');

    return $query->num_rows();
  }

  public function search_kerusakan_gedung_data($limit, $start, $col, $dir, $search)
  {
    $query = $this
      ->db
      ->like('kerusakan_gedung', $search)
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('g_kerusakan');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }
  
  public function getLantai()
  {
    return $this->db->query('SELECT * from g_lantai where deleted_at is null')->result();
  }
  
  public function getKerusakan()
  {
    return $this->db->query('SELECT * from g_kerusakan where deleted_at is null')->result();
  }

  
  
  public function all_report_gedung_count($awal,$akhir)
  {
    if($awal==$akhir){
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->where('date(gedung.created_at)', $awal)
        ->get('gedung');
    }
    else{
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->where('gedung.created_at >=', $awal)
        ->where('gedung.created_at <=', $akhir)
        ->get('gedung');
    }

    return $query->num_rows();
  }

  public function all_report_gedung_data($limit, $start, $col, $dir, $awal, $akhir)
  {
    if($awal==$akhir){
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('gedung.deleted_at',null)
        ->where('date(gedung.created_at)', $awal)
        ->join('g_lantai','g_lantai.id = gedung.lantai','left')
        ->join('g_kerusakan','g_kerusakan.id = gedung.kerusakan','left')
        ->join('users','users.nik = gedung.created_by','left')
        ->select('gedung.id, gedung.created_at, gedung.keterangan, g_lantai.lantai, g_kerusakan.kerusakan_gedung, users.name')
        ->get('gedung');
    }
    else{
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('gedung.deleted_at',null)
        ->where('gedung.created_at >=', $awal)
        ->where('gedung.created_at <=', $akhir)
        ->join('g_lantai','g_lantai.id = gedung.lantai','left')
        ->join('g_kerusakan','g_kerusakan.id = gedung.kerusakan','left')
        ->join('users','users.nik = gedung.created_by','left')
        ->select('gedung.id, gedung.created_at, gedung.keterangan, g_lantai.lantai, g_kerusakan.kerusakan_gedung, users.name')
        ->get('gedung');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_report_gedung_count($search, $awal, $akhir)
  {
    if($awal==$akhir){
      $query = $this
        ->db
        ->like('lantai', $search)
        ->or_like('kerusakan_gedung', $search)
        ->or_like('name', $search)
        ->where('gedung.deleted_at',null)
        ->where('date(gedung.created_at)', $awal)
        ->join('g_lantai','g_lantai.id = gedung.lantai','left')
        ->join('g_kerusakan','g_kerusakan.id = gedung.kerusakan','left')
        ->join('users','users.nik = gedung.created_by','left')
        ->select('gedung.id, gedung.created_at, gedung.keterangan, g_lantai.lantai, g_kerusakan.kerusakan_gedung, users.name')
        ->get('gedung');
    }
    else{
      $query = $this
        ->db
        ->like('lantai', $search)
        ->or_like('kerusakan_gedung', $search)
        ->or_like('name', $search)
        ->where('gedung.deleted_at',null)
        ->where('gedung.created_at >=', $awal)
        ->where('gedung.created_at <=', $akhir)
        ->join('g_lantai','g_lantai.id = gedung.lantai','left')
        ->join('g_kerusakan','g_kerusakan.id = gedung.kerusakan','left')
        ->join('users','users.nik = gedung.created_by','left')
        ->select('gedung.id, gedung.created_at, gedung.keterangan, g_lantai.lantai, g_kerusakan.kerusakan_gedung, users.name')
        ->get('gedung');
    }

    return $query->num_rows();
  }

  public function search_report_gedung_data($limit, $start, $col, $dir, $search, $awal, $akhir)
  {
    if($awal==$akhir){
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->like('lantai', $search)
        ->or_like('kerusakan_gedung', $search)
        ->or_like('name', $search)
        ->where('gedung.deleted_at',null)
        ->where('date(gedung.created_at)', $awal)
        ->join('g_lantai','g_lantai.id = gedung.lantai','left')
        ->join('g_kerusakan','g_kerusakan.id = gedung.kerusakan','left')
        ->join('users','users.nik = gedung.created_by','left')
        ->select('gedung.id, gedung.created_at, gedung.keterangan, g_lantai.lantai, g_kerusakan.kerusakan_gedung, users.name')
        ->get('gedung');
    }
    else{
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->like('lantai', $search)
        ->or_like('kerusakan_gedung', $search)
        ->or_like('name', $search)
        ->where('gedung.deleted_at',null)
        ->where('gedung.created_at >=', $awal)
        ->where('gedung.created_at <=', $akhir)
        ->join('g_lantai','g_lantai.id = gedung.lantai','left')
        ->join('g_kerusakan','g_kerusakan.id = gedung.kerusakan','left')
        ->join('users','users.nik = gedung.created_by','left')
        ->select('gedung.id, gedung.created_at, gedung.keterangan, g_lantai.lantai, g_kerusakan.kerusakan_gedung, users.name')
        ->get('gedung');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function export_report_gedung($awal, $akhir)
  {    
    if($awal==$akhir){
      $query = $this
        ->db
        ->order_by('gedung.created_at, gedung.lantai, name')
        ->where('gedung.deleted_at',null)
        ->where('date(gedung.created_at)', $awal)
        ->join('g_lantai','g_lantai.id = gedung.lantai','left')
        ->join('g_kerusakan','g_kerusakan.id = gedung.kerusakan','left')
        ->join('users','users.nik = gedung.created_by','left')
        ->select('gedung.id, gedung.created_at, gedung.created_by, gedung.keterangan, g_lantai.lantai, g_kerusakan.kerusakan_gedung, users.name')
        ->get('gedung');
    }
    else{
      $query = $this
        ->db
        ->order_by('gedung.created_at, gedung.lantai, name')
        ->where('gedung.deleted_at',null)
        ->where('gedung.created_at >=', $awal)
        ->where('gedung.created_at <=', $akhir)
        ->join('g_lantai','g_lantai.id = gedung.lantai','left')
        ->join('g_kerusakan','g_kerusakan.id = gedung.kerusakan','left')
        ->join('users','users.nik = gedung.created_by','left')
        ->select('gedung.id, gedung.created_at, gedung.created_by, gedung.keterangan, g_lantai.lantai, g_kerusakan.kerusakan_gedung, users.name')
        ->get('gedung');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  // public function all_jenis_pekerjaan_count()
  // {
  //   $query = $this
  //     ->db
  //     ->where('deleted_at',null)
  //     ->get('g_jenis_pekerjaan');
  
  //   return $query->num_rows();
  // }
  
  // public function all_jenis_pekerjaan_data($limit, $start, $col, $dir)
  // {
  //   $query = $this
  //     ->db
  //     ->limit($limit, $start)
  //     ->order_by($col, $dir)
  //     ->where('deleted_at',null)
  //     ->get('g_jenis_pekerjaan');
  
  //   if ($query->num_rows() > 0) {
  //     return $query->result();
  //   } else {
  //     return null;
  //   }
  // }
  
  // public function search_jenis_pekerjaan_count($search)
  // {
  //   $query = $this
  //     ->db
  //     ->like('jenis_pekerjaan', $search)
  //     ->where('deleted_at',null)
  //     ->get('g_jenis_pekerjaan');
  
  //   return $query->num_rows();
  // }
  
  // public function search_jenis_pekerjaan_data($limit, $start, $col, $dir, $search)
  // {
  //   $query = $this
  //     ->db
  //     ->like('jenis_pekerjaan', $search)
  //     ->limit($limit, $start)
  //     ->order_by($col, $dir)
  //     ->where('deleted_at',null)
  //     ->get('g_jenis_pekerjaan');
  
  //   if ($query->num_rows() > 0) {
  //     return $query->result();
  //   } else {
  //     return null;
  //   }
  // }
}
