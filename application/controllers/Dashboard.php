<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	public function __construct() {
		
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Login_model');
	}
    public function index()
	{
		$check_session = $this->session->userdata('nik');
		if ($check_session != null) {
			$this->load->view('dashboard');
		} 
		else{
			redirect('login');
		}
		// $this->load->view('dashboard');
	}
}
