<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url('assets/lib/jquery/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/bootstrap/js/bootstrap.min.js') ?>"></script>
<script class="include" type="text/javascript" src="<?php echo base_url('assets/lib/jquery.dcjqaccordion.2.7.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/jquery.scrollTo.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/lib/jquery.nicescroll.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/jquery.sparkline.js') ?>"></script>
<!--common script for all pages-->
<script src="<?php echo base_url('assets/lib/common-scripts.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/gritter/js/jquery.gritter.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/gritter-conf.js') ?>"></script>
<!--script for this page-->
<script src="<?php echo base_url('assets/lib/sparkline-chart.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/zabuto_calendar.js') ?>"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/daterangepicker.js"></script>
<!--BACKSTRETCH-->
<!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
<script type="text/javascript" src="<?php echo base_url('assets/lib/jquery.backstretch.min.js') ?>"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/datatables/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/datatables/datatables-demo.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/select2.min.js"></script>
<script src="<?php echo base_url('assets/lib/bootstrap-select.min.js')?>"></script>

<!-- <script src="<?php echo base_url('assets/lib/jquery-ui-1.9.2.custom.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js')?>"></script>
  
  <script src="<?php echo base_url('assets/lib/advanced-form-components.js')?>"></script> -->
