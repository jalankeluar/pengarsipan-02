<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view("partials/head.php") ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker.css')?>" />
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
        <?php $this->load->view("partials/navbar.php") ?>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
        <?php $this->load->view("partials/sidebar.php") ?>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Form Validation</h3>
        <!-- BASIC FORM VALIDATION -->
        <!-- FORM VALIDATION -->
        <div class="row mt">
          <div class="col-lg-12">
            <h4><i class="fa fa-angle-right"></i> Form Validations</h4>
            <div class="form-panel">
              <div class=" form">
                <form class="cmxform form-horizontal style-form" id="commentForm" method="post" action="<?php echo base_url(). 'Kendaraan/simpan'; ?>">
                  <div class="form-group ">
                    <label for="no_polisi" class="control-label col-lg-2">No Polisi </label>
                    <div class="col-lg-10">
                      <input class=" form-control" id="no_polisi" name="no_polisi" type="text" placeholder="Masukkan No Polisi" required />
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="service" class="control-label col-lg-2">Service </label>
                    <div class="col-lg-10">
                      <input class=" form-control" id="service" name="service" type="text" placeholder="Masukkan Service" required />
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="pengajuan_date" class="control-label col-lg-2">Tanggal Pengajuan </label>
                    <div class="col-md-3 col-xs-10">
                      <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="01-01-2014" class="input-append date dpYears">
                        <input type="text" readonly="" value="01-01-2014" size="16" class="form-control" name="pengajuan_date">
                        <span class="input-group-btn add-on">
                          <button class="btn btn-theme" type="button"><i class="fa fa-calendar"></i></button>
                          </span>
                      </div>
                    </div>
                  </div>
                <div class="form-group ">
                    <label for="harga" class="control-label col-lg-2">Harga </label>
                    <div class="col-lg-10">
                      <input class=" form-control" id="harga" name="harga" type="text" placeholder="Masukkan harga" required />
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="keterangan" class="control-label col-lg-2">Keterangan</label>
                    <div class="col-lg-10">
                      <textarea class="form-control " id="keterangan" name="keterangan" placeholder="max. 255 karakter"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                      <button class="btn btn-theme" type="submit">Save</button>
                      <button class="btn btn-theme04" type="button">Cancel</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- /form-panel -->
          </div>
          <!-- /col-lg-12 -->
        </div>
        <!-- /row -->

      </section>
      <!-- /wrapper -->
    </section>
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
        <?php $this->load->view("partials/footer.php") ?>
    </footer>
    <!--footer end-->
  </section>

  <!-- js placed at the end of the document so the pages load faster -->
  <?php $this->load->view("partials/js.php") ?>

  <script src="<?php echo base_url('assets/lib/jquery-ui-1.9.2.custom.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js')?>"></script>
  <script src="<?php echo base_url('assets/lib/advanced-form-components.js')?>"></script>

</body>

</html>
