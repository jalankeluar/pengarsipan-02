<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker.css') ?>" />
</head>

<body>
  <section id="container">
    <!-- ******************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        ******************************************************************************************************************************************************* -->
    <!--header start-->
    <header class="header black-bg">
      <?php $this->load->view("partials/navbar.php") ?>
    </header>
    <!--header end-->
    <!-- ******************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        ******************************************************************************************************************************************************* -->
    <!--sidebar start-->
    <aside>
      <?php $this->load->view("partials/sidebar.php") ?>
    </aside>
    <!--sidebar end-->
    <!-- ******************************************************************************************************************************************************
        MAIN CONTENT
        ******************************************************************************************************************************************************* -->
    <!--main content start-->
    <section id="main-content">
      <div class="site-container">
        <div class="site-content">
          <section class="wrapper">
              <div class="table-responsive container col-sm-12" style="margin-top:20px">
                <h1>Data <small>Kendaraan</small>
                  
                </h1><br>
                <div class="col-lg-12 mt">
                  <div class="row content-panel">
                    <div class="panel-heading">
                      <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                          <a data-toggle="tab" href="#nopolisi" onclick="changeTab('nopolisi')">No Polisi</a>
                        </li>
                        <li>
                          <a data-toggle="tab" href="#service" onclick="changeTab('service')">Service</a>
                        </li>
                      </ul>
                    </div>
                    <input type="text" id="tab-active" class="hidden" value="">

                    <!-- /panel-heading -->
                    <div class="panel-body">
                      <div class="tab-content">
                        <div id="nopolisi" class="tab-pane active">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="pull-right"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_nopolisi_add_new"> Tambah Data</a></div>
                              <br><br><br>
                              <div class="alert alert-warning fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                  <i class="icon-remove"></i>
                                </button>
                                <strong>INFORMASI</strong><br> Pencarian data berdasarkan <b>no polisi</b>, <b>tahun</b> dan <b>type</b>.
                              </div>
                              <table class="table table-bordered table-striped" id="nopolisi-table" width="100%" cellspacing="0">
                                <thead>
                                  <tr class="info">
                                    <th style="text-align:center">No</th>
                                    <th style="text-align:center">No Polisi</th>
                                    <th style="text-align:center">Type</th>
                                    <th style="text-align:center">Tahun</th>
                                    <th style="text-align:center">Tanggal Pajak</th>
                                    <th style="text-align:center">Tanggal STNK</th>
                                    <th style="text-align:center">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tbody>
                              </table>
                              <!-- /detailed -->
                            </div>
                            <!-- /col-md-6 -->
                          </div>
                          <!-- /OVERVIEW -->
                        </div>
                        <!-- /tab-pane -->
                        <div id="service" class="tab-pane">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="pull-right"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_service_add_new"> Tambah Data</a></div>
                              <br><br><br>
                              <div class="alert alert-warning fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                  <i class="icon-remove"></i>
                                </button>
                                <strong>INFORMASI</strong><br> Pencarian data berdasarkan <b>servis</b>.
                              </div>
                              <table class="table table-bordered table-striped" id="service-table" width="100%" cellspacing="0">
                                <thead>
                                  <tr class="info">
                                    <th style="text-align:center">No</th>
                                    <th style="text-align:center">Service</th>
                                    <th style="text-align:center">Keterangan</th>
                                    <th style="text-align:center">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tbody>
                              </table>
                              <!-- /detailed -->
                            </div>
                            <!-- /col-md-6 -->
                          </div>
                          <!-- /row -->
                        </div>
                        <!-- /tab-pane -->
                      </div>
                      <!-- /tab-content -->
                    </div>
                    <!-- /panel-body -->
                  </div>
                <!-- </div> -->
               
              </div>
          </section>

        </div>
        <button type="button" class="hidden" id="refresh"></button>
        <!--footer start-->
        <footer class="site-footer">
          <?php $this->load->view("partials/footer.php") ?>
        </footer>
        <!--footer end-->
      </div>
    </section>
    <!--main content end-->
  </section>

  <!-- ============ MODAL ADD BARANG =============== -->
  <div class="modal fade" id="modal_nopolisi_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Tambah Data No Polisi</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/simpan_nopolisi' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">No Polisi</label>
              <div class="col-xs-8">
                <input name="no_polisi" id="no_polisi" class="form-control" type="text" placeholder="No Polisi" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Type</label>
              <div class="col-xs-8">
                <input class="form-control " id="nopolisi_type" name="nopolisi_type" type="text" placeholder="Type Kendaraan" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Tahun</label>
              <div class="col-xs-8">
                <input class="form-control " id="nopolisi_tahun" name="nopolisi_tahun" type="text" placeholder="Tahun Kendaraan" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Tanggal Pajak</label>
              <div class="col-xs-8">
                <input type="text" class="form-control input-tanggal" name="nopolisi_pajak" id="nopolisi_pajak" readonly>
                <span class="input-group-btn add-on">
                  <button class="btn btn-theme" type="button"><i class="fa fa-calendar"></i></button>
                </span>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Tanggal STNK</label>
              <div class="col-xs-8">
                <input type="text" class="form-control input-tanggal" name="nopolisi_stnk" id="nopolisi_stnk" readonly>
                <span class="input-group-btn add-on">
                  <button class="btn btn-theme" type="button"><i class="fa fa-calendar"></i></button>
                </span>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <!-- ============ MODAL ADD BARANG =============== -->
  <div class="modal fade" id="modal_service_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Tambah Data Service</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/simpan_service' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">Service</label>
              <div class="col-xs-8">
                <input name="service" id="service" class="form-control" type="text" placeholder="Service" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="service_keterangan" name="service_keterangan" placeholder="max. 255 karakter"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <!-- ============ MODAL EDIT BARANG =============== -->
  <div class="modal fade" id="modal_nopolisi_edit_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Edit Data No Polisi</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/update_nopolisi' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">No Polisi</label>
              <div class="col-xs-8">
                <input name="edit_nopolisi" id="edit_nopolisi" class="form-control" type="text" placeholder="No Polisi" required>
              </div>
            </div>

            <input type="text" id="edit_nopolisi_id" class="form-control hidden" name="edit_nopolisi_id">

            <div class="form-group">
              <label class="control-label col-xs-3">Type</label>
              <div class="col-xs-8">
                <input class="form-control " id="edit_nopolisi_type" name="edit_nopolisi_type" type="text" placeholder="Type Kendaraan" required>
              </div>
            </div>

            

            <div class="form-group">
              <label class="control-label col-xs-3">Tahun</label>
              <div class="col-xs-8">
                <input class="form-control " id="edit_nopolisi_tahun" name="edit_nopolisi_tahun" type="text" placeholder="Tahun Kendaraan" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Tanggal Pajak</label>
              <div class="col-xs-8">
                <input type="text" class="form-control input-tanggal" name="edit_nopolisi_pajak" id="edit_nopolisi_pajak" readonly>
                <span class="input-group-btn add-on">
                  <button class="btn btn-theme" type="button"><i class="fa fa-calendar"></i></button>
                </span>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Tanggal STNK</label>
              <div class="col-xs-8">
                <input type="text" class="form-control input-tanggal" name="edit_nopolisi_stnk" id="edit_nopolisi_stnk" readonly>
                <span class="input-group-btn add-on">
                  <button class="btn btn-theme" type="button"><i class="fa fa-calendar"></i></button>
                </span>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->
  
  <!-- ============ MODAL EDIT BARANG =============== -->
  <div class="modal fade" id="modal_service_edit_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Edit Data Service</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/update_service' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">Service</label>
              <div class="col-xs-8">
                <input name="edit_service" id="edit_service" class="form-control" type="text" placeholder="Service" required>
              </div>
            </div>

            <input type="text" id="edit_service_id" class="form-control hidden" name="edit_service_id">

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="edit_service_keterangan" name="edit_service_keterangan" placeholder="max. 255 karakter"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <?php $this->load->view("partials/modal.php") ?>

  <!-- js placed at the end of the document so the pages load faster -->
  <?php $this->load->view("partials/js.php") ?>

<!-- date -->
  
  <script type="text/javascript">
    $(document).ready(function() {
      $('.input-tanggal').daterangepicker({
        format          : "dd-mm-yyyy",
        singleDatePicker: true,
        autoApply       : true,
        todayHighlight  : true,
        locale: {
          format: "DD-MM-YYYY",
          // separator: " - ",
        }
      });

      changeTab('nopolisi');
      changeTab('servis');
    });
    
    // $('#tabs-active').change(function()
    $('#tab-active').on('change',function()
    {
      var active =  $('#tab-active').val();
      console.log(active);
      // console.log((active=='nopolisi'));
      if(active=='nopolisi'){
        tableNoPolisi();
      }
      else{
        tableService();
      }
    });

    function changeTab(kode) {
      // console.log(kode);
      $('#tab-active').val(kode).trigger('change');
    }
    
    function tableNoPolisi(){
      
      $('#nopolisi-table').DataTable().destroy();
      $('#nopolisi-table tbody').empty();
      var table = $('#nopolisi-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "data_nopolisi",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "no_polisi", "className": "text-center"},
          { "data": "type", 'sortable': false},
          { "data": "tahun", "className": "text-center", 'sortable': false},
          { "data": "pajak", 'sortable': false},
          { "data": "stnk", 'sortable': false},
          { "data": "action", 'sortable': false},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#nopolisi-table_filter input').unbind();
      
			var dtable = $('#nopolisi-table').dataTable().api();
      $('#nopolisi-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
						// Call the API search function
						dtable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtable.search("").draw();
					}
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#nopolisi-table').DataTable().ajax.reload();
      });
    }

    function tableService(){
      $('#service-table').DataTable().destroy();
      $('#service-table tbody').empty();
      var table = $('#service-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "data_service",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "servis"},
          { "data": "keterangan", 'sortable': false},
          { "data": "action", 'sortable': false},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#service-table_filter input').unbind();
      $('#service-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0) {
          table.search($(this).val()).draw();
        }
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#service-table').DataTable().ajax.reload();
      });
    }

    function edit_nopolisi(id) {
      $.ajax({
        type: "get",
        url: 'edit_nopolisi/'+id,
      })
      .done(function (response) {
          var result = JSON.parse(response)
          
          $('#edit_nopolisi_id').val(result.query.id);
          $('#edit_nopolisi').val(result.query.no_polisi);
          $('#edit_nopolisi_type').val(result.query.type);
          $('#edit_nopolisi_tahun').val(result.query.tahun);
          $('#edit_nopolisi_pajak').val(result.tgl_pajak);
          $('#edit_nopolisi_stnk').val(result.tgl_stnk);
          
          $("#modal_nopolisi_edit_new").modal('show');
          
      });
    }

    function edit_service(id) {
      $.ajax({
        type: "get",
        url: 'edit_service/'+id,
      })
      .done(function (response) {
          var result = JSON.parse(response)


          $('#edit_service_id').val(result.query.id);
          $('#edit_service').val(result.query.servis);
          $('#edit_service_keterangan').val(result.query.keterangan);
          
          $("#modal_service_edit_new").modal('show');
          
      });
    }

    // <a onclick="deleteConfirm('<?php //echo site_url('Admin/deleteSub/'.$s->id_sub_dept) ?>')"
    // href="#!" class="btn btn-danger btn-sm" type="submit" name="btn-delete">Hapus</a>


    function delete_nopolisi(id){
      var url = '<?php echo site_url('masterdata/delete_nopolisi/') ?>'+id;
      // console.log(url);
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }

    function delete_service(id){
      var url = '<?php echo site_url('masterdata/delete_service/') ?>'+id;
      // console.log(url);
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }
  </script>

</body>

</html>