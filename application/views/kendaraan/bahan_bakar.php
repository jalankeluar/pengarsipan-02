<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker.css') ?>" />
</head>

<body>
  <section id="container">
    <!-- ******************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        ******************************************************************************************************************************************************* -->
    <!--header start-->
    <header class="header black-bg">
      <?php $this->load->view("partials/navbar.php") ?>
    </header>
    <!--header end-->
    <!-- ******************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        ******************************************************************************************************************************************************* -->
    <!--sidebar start-->
    <aside>
      <?php $this->load->view("partials/sidebar.php") ?>
    </aside>
    <!--sidebar end-->
    <!-- ******************************************************************************************************************************************************
        MAIN CONTENT
        ******************************************************************************************************************************************************* -->
    <!--main content start-->
    <section id="main-content">
      <div class="site-container">
        <div class="site-content">
          <section class="wrapper">
              <div class="table-responsive container col-sm-12" style="margin-top:20px">
                <h1>Data <small>Bahan Bakar</small>
                  <div class="pull-right"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_add_new"> Tambah Data</a></div>
                </h1><br>
                <div class="alert alert-warning fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="icon-remove"></i>
                  </button>
                  <strong>INFORMASI</strong><br> Pencarian data berdasarkan <b>no nota</b> dan <b>no polisi</b>.
                </div>
                <table class="table table-bordered table-striped" id="bbm-table" width="100%" cellspacing="0">
                  <thead>
                    <tr class="info">
                    <th style="text-align:center">No</th>
                      <th style="text-align:center">No Nota</th>
                      <th style="text-align:center">No Polisi</th>
                      <th style="text-align:center">Tanggal</th>
                      <th style="text-align:center">SPBU</th>
                      <th style="text-align:center">Harga/liter</th>
                      <th style="text-align:center">Total Bayar</th>
                      <th style="text-align:center">Jumlah Liter</th>
                      <th style="text-align:center">Action</th>
                      <!-- <th style="text-align:center">Keterangan</th> -->
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
          </section>

        </div>
        <button type="button" class="hidden" id="refresh"></button>
        <!--footer start-->
        <footer class="site-footer">
          <?php $this->load->view("partials/footer.php") ?>
        </footer>
        <!--footer end-->
      </div>
    </section>
    <!--main content end-->
  </section>

  <!-- ============ MODAL ADD BARANG =============== -->
  <div class="modal fade" id="modal_add_new" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Tambah Data Kendaraan</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'Kendaraan/simpan_bahan_bakar' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">No Nota</label>
              <div class="col-xs-8">
                <input name="no_nota" class="form-control" type="text" placeholder="No Nota" required>
              </div>
            </div>

            <div class="form-group">
            <label class="control-label col-xs-3">No Polisi</label>
              <div class="col-xs-8">
                <select class="form-control"  style="width: 100%" id="no_polisi" name="no_polisi" required>
                  <option value="">Pilih No Polisi</option>
                  <?php
                      foreach ($no_polisi as $m) {
                          echo '<option value="' . $m->id . '">';
                          echo $m->no_polisi;
                          echo '</option>';
                      }
                  ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Tanggal</label>
              <div class="col-xs-8">

                <input type="text" class="form-control input-tanggal" name="tgl" id="tgl" readonly>
                <span class="input-group-btn add-on">
                  <button class="btn btn-theme" type="button"><i class="fa fa-calendar"></i></button>
                </span>

                <!-- <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="01-01-2014" class="input-append date dpYears">
                  <input type="text" readonly="" value="01-01-2014" size="16" class="form-control" name="tgl">
                  <span class="input-group-btn add-on">
                    <button class="btn btn-theme" type="button"><i class="fa fa-calendar"></i></button>
                  </span>
                </div> -->
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">SPBU</label>
              <div class="col-xs-8">
                <input name="spbu" class="form-control" type="text" placeholder="SPBU" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Harga/liter</label>
              <div class="col-xs-8">
                <input name="harga" id="harga" class="form-control" type="number" onkeypress="return onlyNumberKey(event)" onkeyup="ttl_liter();" placeholder="Harga" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Total Bayar</label>
              <div class="col-xs-8">
                <input name="ttl_bayar" id="ttl_bayar" class="form-control" type="number" onkeypress="return onlyNumberKey(event)" onkeyup="ttl_liter();" placeholder="Total Bayar" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Jumlah Liter</label>
              <div class="col-xs-8">
                <input name="jml_liter" id="jml_liter" class="form-control" type="text" readonly>
              </div>
            </div>

            <!-- <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="keterangan" name="keterangan"></textarea>
              </div>
            </div> -->

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <!-- js placed at the end of the document so the pages load faster -->
  <?php $this->load->view("partials/js.php") ?>
  <?php $this->load->view("partials/modal.php") ?>
  
  <script type="text/javascript">
    $(document).ready(function() {

      $('#no_polisi').select2({
        placeholder: 'Pilih No Polisi'
      });

      $('.input-tanggal').daterangepicker({
        format          : "dd-mm-yyyy",
        singleDatePicker: true,
        autoApply       : true,
        todayHighlight  : true,
        locale: {
          format: "DD-MM-YYYY",
          // separator: " - ",
        }
      });


      var table = $('#bbm-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "data_bahan_bakar",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "no_nota"},
          { "data": "no_polisi", "className": "text-center"},
          { "data": "tgl", "className": "text-center"},
          { "data": "spbu"},
          { "data": "harga", "className": "text-right"},
          { "data": "ttl_bayar", "className": "text-right"},
          { "data": "jml_liter"},
          { "data": "action"},
          // { "data": "keterangan", 'sortable': false},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#bbm-table_filter input').unbind();
      $('#bbm-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0) {
          table.search($(this).val()).draw();
        }
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#bbm-table').DataTable().ajax.reload();
      });

    });

    function delete_bahan_bakar(id){
      var url = '<?php echo site_url('Kendaraan/delete_bahan_bakar/') ?>'+id;
      // console.log(url);
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }
  </script>

<script>
function ttl_liter() {
      var txtFirstNumberValue = document.getElementById('ttl_bayar').value;
      var txtSecondNumberValue = document.getElementById('harga').value;
      // var txtTigaNumberValue = document.getElementById('txt3').value;
      var result = parseFloat(txtFirstNumberValue) / parseFloat(txtSecondNumberValue);
      if (!isNaN(result)) {
         document.getElementById('jml_liter').value = result;
      }
}
</script>
<script> 
    function onlyNumberKey(evt) { 
          
        // Only ASCII charactar in that range allowed 
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode 
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57)) 
            return false; 
        return true; 
    } 
</script> 

</body>

</html>