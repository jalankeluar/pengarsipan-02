<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker.css') ?>" />
</head>

<body>
  <section id="container">
    <!-- ******************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        ******************************************************************************************************************************************************* -->
    <!--header start-->
    <header class="header black-bg">
      <?php $this->load->view("partials/navbar.php") ?>
    </header>
    <!--header end-->
    <!-- ******************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        ******************************************************************************************************************************************************* -->
    <!--sidebar start-->
    <aside>
      <?php $this->load->view("partials/sidebar.php") ?>
    </aside>
    <!--sidebar end-->
    <!-- ******************************************************************************************************************************************************
        MAIN CONTENT
        ******************************************************************************************************************************************************* -->
    <!--main content start-->
    <section id="main-content">
      <div class="site-container">
        <div class="site-content">
          <section class="wrapper">
              <div class="table-responsive container col-sm-12" style="margin-top:20px">
                <h1>Data <small>SPBU</small>
                  
                </h1><br>
                <div class="col-lg-12 mt">
                  <div class="row content-panel">
                    
                    <input type="text" id="tab-active" class="hidden" value="">

                    <!-- /panel-heading -->
                    <div class="panel-body">
                      <div class="tab-content">

                          <div class="row">
                            <div class="col-md-12">
                              <div class="pull-right"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_spbu_add_new"> Tambah Data</a></div>
                              <br><br><br>
                              <div class="alert alert-warning fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                  <i class="icon-remove"></i>
                                </button>
                                <strong>INFORMASI</strong><br> Pencarian data berdasarkan <b>spbu</b>, <b>alamat</b>.
                              </div>
                              <table class="table table-bordered table-striped" id="spbu-table" width="100%" cellspacing="0">
                                <thead>
                                  <tr class="info">
                                    <th style="text-align:center">No</th>
                                    <th style="text-align:center">SPBU</th>
                                    <th style="text-align:center">Alamat</th>
                                    <th style="text-align:center">Keterangan</th>
                                    <th style="text-align:center">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tbody>
                              </table>
                              <!-- /detailed -->
                            </div>
                            <!-- /col-md-6 -->
                          </div>
                          <!-- /OVERVIEW -->

                        
                      </div>
                      <!-- /tab-content -->
                    </div>
                    <!-- /panel-body -->
                  </div>
                <!-- </div> -->
               
              </div>
          </section>

        </div>
        <button type="button" class="hidden" id="refresh"></button>
        <!--footer start-->
        <footer class="site-footer">
          <?php $this->load->view("partials/footer.php") ?>
        </footer>
        <!--footer end-->
      </div>
    </section>
    <!--main content end-->
  </section>

  <!-- ============ MODAL ADD BARANG =============== -->
  <div class="modal fade" id="modal_spbu_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Tambah Data SPBU</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/simpan_spbu' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">SPBU</label>
              <div class="col-xs-8">
                <input name="spbu" id="spbu" class="form-control" type="text" placeholder="SPBU" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Alamat</label>
              <div class="col-xs-8">
                <input class="form-control " id="spbu_alamat" name="spbu_alamat" type="text" placeholder="Alamat SPBU" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="spbu_keterangan" name="spbu_keterangan"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <!-- ============ MODAL EDIT BARANG =============== -->
  <div class="modal fade" id="modal_spbu_edit_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Edit Data SPBU</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/update_spbu' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">SPBU</label>
              <div class="col-xs-8">
                <input name="edit_spbu" id="edit_spbu" class="form-control" type="text" placeholder="SPBU" required>
              </div>
            </div>

            <input type="text" id="edit_spbu_id" class="form-control hidden" name="edit_spbu_id">

            <div class="form-group">
              <label class="control-label col-xs-3">Alamat</label>
              <div class="col-xs-8">
                <input class="form-control " id="edit_spbu_alamat" name="edit_spbu_alamat" type="text" placeholder="Alamat SPBU" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="edit_spbu_keterangan" name="edit_spbu_keterangan"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <?php $this->load->view("partials/modal.php") ?>

  <!-- js placed at the end of the document so the pages load faster -->
  <?php $this->load->view("partials/js.php") ?>

<!-- date -->
  
<script type="text/javascript">
    $(document).ready(function() {
      $('.input-tanggal').daterangepicker({
        format          : "dd-mm-yyyy",
        singleDatePicker: true,
        autoApply       : true,
        todayHighlight  : true,
        locale: {
          format: "DD-MM-YYYY",
          // separator: " - ",
        }
      });

        tableSpbu();///
      // changeTab('spbu');
      // changeTab('servis');
    });
    
    // // $('#tabs-active').change(function()
    // $('#tab-active').on('change',function()
    // {
    //   var active =  $('#tab-active').val();
    //   console.log(active);
    //   // console.log((active=='spbu'));
    //   if(active=='spbu'){
    //   }
    //   else{
    //     tableService();
    //   }
    // });

    // function changeTab(kode) {
    //   // console.log(kode);
    //   $('#tab-active').val(kode).trigger('change');
    // }
    
    function tableSpbu(){
      
      $('#spbu-table').DataTable().destroy();
      $('#spbu-table tbody').empty();
      var table = $('#spbu-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "data_spbu",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "spbu", "className": "text-center"},
          { "data": "alamat", 'sortable': false},
          { "data": "keterangan", 'sortable': false},
          { "data": "action", 'sortable': false},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#spbu-table_filter input').unbind();
      
			var dtable = $('#spbu-table').dataTable().api();
      $('#spbu-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
						// Call the API search function
						dtable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtable.search("").draw();
					}
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#spbu-table').DataTable().ajax.reload();
      });
    }

    function edit_spbu(id) {
      $.ajax({
        type: "get",
        url: 'edit_spbu/'+id,
      })
      .done(function (response) {
          var result = JSON.parse(response)
          
          $('#edit_spbu_id').val(result.query.id);
          $('#edit_spbu').val(result.query.spbu);
          $('#edit_spbu_alamat').val(result.query.alamat);
          $('#edit_spbu_keterangan').val(result.query.keterangan);
          
          $("#modal_spbu_edit_new").modal('show');
          
      });
    }

    function delete_spbu(id){
      var url = '<?php echo site_url('masterdata/delete_spbu/') ?>'+id;
      // console.log(url);
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }

  </script>  

</body>

</html>