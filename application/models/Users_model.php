<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends CI_Model
{

  private $_table = "users";

  public function view()
  {
    return $this->db->query('SELECT * from users')->result();
  }

  public function save($data)
  {
    $this->db->insert($this->_table, $data);
  }

  public function update($where, $data, $_table)
  {
    $this->db->where($where);
    $this->db->update($_table, $data);
  }

  public function delete($id)
  {
    return $this->db->delete($this->_table, array("id" => $id));
  }

  public function all_users_count()
  {
    $query = $this
      ->db
      ->where('deleted_at',null)
      ->get('users');

    return $query->num_rows();
  }

  public function all_users_data($limit, $start, $col, $dir)
  {
    $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('users');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_users_count($search)
  {
    $query = $this
      ->db
      ->like('nik', $search)
      ->or_like('name', $search)
      ->where('deleted_at',null)
      ->get('users');

    return $query->num_rows();
  }

  public function search_users_data($limit, $start, $col, $dir, $search)
  {
    $query = $this
      ->db
      ->like('nik', $search)
      ->or_like('name', $search)
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('users');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  
}
