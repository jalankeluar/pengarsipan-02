<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker.css') ?>" />
</head>

<body>
  <section id="container">
    <!-- ******************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        ******************************************************************************************************************************************************* -->
    <!--header start-->
    <header class="header black-bg">
      <?php $this->load->view("partials/navbar.php") ?>
    </header>
    <!--header end-->
    <!-- ******************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        ******************************************************************************************************************************************************* -->
    <!--sidebar start-->
    <aside>
      <?php $this->load->view("partials/sidebar.php") ?>
    </aside>
    <!--sidebar end-->
    <!-- ******************************************************************************************************************************************************
        MAIN CONTENT
        ******************************************************************************************************************************************************* -->
    <!--main content start-->
    <section id="main-content">
      <div class="site-container">
        <div class="site-content">
          <section class="wrapper">
              <div class="table-responsive container col-sm-12" style="margin-top:20px">
                <h1>Data <small>Vendor</small>
                  
                </h1><br>
                <div class="col-lg-12 mt">
                  <div class="row content-panel">
                    
                    <input type="text" id="tab-active" class="hidden" value="">

                    <!-- /panel-heading -->
                    <div class="panel-body">
                      <div class="tab-content">

                          <div class="row">
                            <div class="col-md-12">
                              <div class="pull-right"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_vendor_add_new"> Tambah Data</a></div>
                              <br><br><br>
                              <div class="alert alert-warning fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                  <i class="icon-remove"></i>
                                </button>
                                <strong>INFORMASI</strong><br> Pencarian data berdasarkan <b>vendor</b>, <b>alamat</b>.
                              </div>
                              <table class="table table-bordered table-striped" id="vendor-table" width="100%" cellspacing="0">
                                <thead>
                                  <tr class="info">
                                    <th style="text-align:center">No</th>
                                    <th style="text-align:center">Vendor</th>
                                    <th style="text-align:center">Alamat</th>
                                    <th style="text-align:center">Kategori</th>
                                    <th style="text-align:center">Keterangan</th>
                                    <th style="text-align:center">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tbody>
                              </table>
                              <!-- /detailed -->
                            </div>
                            <!-- /col-md-6 -->
                          </div>
                          <!-- /OVERVIEW -->

                        
                      </div>
                      <!-- /tab-content -->
                    </div>
                    <!-- /panel-body -->
                  </div>
                <!-- </div> -->
               
              </div>
          </section>

        </div>
        <button type="button" class="hidden" id="refresh"></button>
        <!--footer start-->
        <footer class="site-footer">
          <?php $this->load->view("partials/footer.php") ?>
        </footer>
        <!--footer end-->
      </div>
    </section>
    <!--main content end-->
  </section>

  <!-- ============ MODAL ADD BARANG =============== -->
  <div class="modal fade" id="modal_vendor_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Tambah Data Vendor</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/simpan_vendor' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">Vendor</label>
              <div class="col-xs-8">
                <input name="vendor" id="vendor" class="form-control" type="text" placeholder="Vendor" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Alamat</label>
              <div class="col-xs-8">
                <input class="form-control " id="vendor_alamat" name="vendor_alamat" type="text" placeholder="Alamat Vendor" required>
              </div>
            </div>

            <div class="form-group">
            <label class="control-label col-xs-3">Kategori</label>
              <div class="col-xs-8">
                <div class="multi-select-full">
                  <select class="multiselect-filtering" multiple="multiple" id="vendor_kategori"style="width: 100%"  name="vendor_kategori[]" required>
                    <!-- <option value="">Pilih Service</option> -->
                    <option value="mesin">Mesin</option>
                    <option value="rumah_dinas">Rumah Dinas</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="vendor_keterangan" name="vendor_keterangan"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <!-- ============ MODAL EDIT BARANG =============== -->
  <div class="modal fade" id="modal_vendor_edit_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Edit Data Vendor</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/update_vendor' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">Vendor</label>
              <div class="col-xs-8">
                <input name="edit_vendor" id="edit_vendor" class="form-control" type="text" placeholder="Vendor" required>
              </div>
            </div>

            <input type="text" id="edit_vendor_id" class="form-control hidden" name="edit_vendor_id">

            <div class="form-group">
              <label class="control-label col-xs-3">Alamat</label>
              <div class="col-xs-8">
                <input class="form-control " id="edit_vendor_alamat" name="edit_vendor_alamat" type="text" placeholder="Alamat Vendor" required>
              </div>
            </div>

            <div class="form-group">
            <label class="control-label col-xs-3">Kategori</label>
              <div class="col-xs-8">
                <div class="multi-select-full">
                  <select class="multiselect-filtering" multiple="multiple" id="edit_vendor_kategori"style="width: 100%"  name="edit_vendor_kategori[]" required>
                    <!-- <option value="">Pilih Service</option> -->
                    <option value="mesin">Mesin</option>
                    <option value="rumah_dinas">Rumah Dinas</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="edit_vendor_keterangan" name="edit_vendor_keterangan"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <?php $this->load->view("partials/modal.php") ?>

  <!-- js placed at the end of the document so the pages load faster -->
  <?php $this->load->view("partials/js.php") ?>

<!-- date -->
  
<script type="text/javascript">
    $(document).ready(function() {

      $('#vendor_kategori').select2({
        placeholder: 'Pilih Kategori'
      });

      $('.input-tanggal').daterangepicker({
        format          : "dd-mm-yyyy",
        singleDatePicker: true,
        autoApply       : true,
        todayHighlight  : true,
        locale: {
          format: "DD-MM-YYYY",
          // separator: " - ",
        }
      });

        tableVendor();///
      // changeTab('vendor');
      // changeTab('servis');
    });
    
    // // $('#tabs-active').change(function()
    // $('#tab-active').on('change',function()
    // {
    //   var active =  $('#tab-active').val();
    //   console.log(active);
    //   // console.log((active=='vendor'));
    //   if(active=='vendor'){
    //   }
    //   else{
    //     tableService();
    //   }
    // });

    // function changeTab(kode) {
    //   // console.log(kode);
    //   $('#tab-active').val(kode).trigger('change');
    // }
    
    function tableVendor(){
      
      $('#vendor-table').DataTable().destroy();
      $('#vendor-table tbody').empty();
      var table = $('#vendor-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "data_vendor",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "vendor", "className": "text-center"},
          { "data": "alamat", 'sortable': false},
          { "data": "kategori", 'sortable': false},
          { "data": "keterangan", 'sortable': false},
          { "data": "action", 'sortable': false},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#vendor-table_filter input').unbind();
      
			var dtable = $('#vendor-table').dataTable().api();
      $('#vendor-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
						// Call the API search function
						dtable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtable.search("").draw();
					}
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#vendor-table').DataTable().ajax.reload();
      });
    }

    function edit_vendor(id) {
      $.ajax({
        type: "get",
        url: 'edit_vendor/'+id,
      })
      .done(function (response) {
          var result = JSON.parse(response)
          
          $('#edit_vendor_id').val(result.query.id);
          $('#edit_vendor').val(result.query.vendor);
          $('#edit_vendor_alamat').val(result.query.alamat);
          $('#edit_vendor_keterangan').val(result.query.keterangan);
          $('#edit_vendor_kategori').val(result.query.kategori);
          
          $("#modal_vendor_edit_new").modal('show');
          
      });
    }

    function delete_vendor(id){
      var url = '<?php echo site_url('masterdata/delete_vendor/') ?>'+id;
      // console.log(url);
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }

  </script>  

</body>

</html>