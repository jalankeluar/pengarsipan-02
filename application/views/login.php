<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <style type="text/css">
    .tengah {
        margin: auto;
        position: absolute;
        top: 0; left: 0; bottom: 0; right: 0;
        height: 405px;
        /* border: 1px solid #999;  jika ingin melihat posisi div anda lebih jelas */
    }
  </style>
</head>

<body>
  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
  <div id="login-page">
    <div class="container tengah">
      <?php if (isset($error)) echo $error; ?>
      <form role="form" class="form-login" method="POST" id="form-login">
        <!-- <form class="form-login" action="index.html"> -->
        <h2 class="form-login-heading">
            <img src="<?php echo base_url() ?>assets/img/jayakarta.png" style="width: 90%">
          <!-- sign in now -->
        </h2>
        <div class="login-wrap">
          <input type="text" class="form-control" placeholder="NIK" name="nik" id="nik" autofocus>
          <br>
          <input type="password" class="form-control" placeholder="Password" name="password" id="password">
          <!-- <label class="checkbox">
            <input type="checkbox" value="remember-me"> Remember me
            <span class="pull-right">
              <a data-toggle="modal" href="login.html#myModal"> Forgot Password?</a>
            </span>
          </label> -->
          <br>
          <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
          <hr>
          <div class="login-social-link centered">
            <p>Belum punya akun?<br>Silahkan hubungi admin untuk pembuatan akun!</p>
            <!-- <p><i>Dont have an account yet?<br>Please ask admin to create one for you!</i></p> -->
            <!-- <br> -->
            <!-- <button class="btn btn-facebook" type="submit"><i class="fa fa-facebook"></i> Facebook</button>
            <button class="btn btn-twitter" type="submit"><i class="fa fa-twitter"></i> Twitter</button> -->
          </div>
          <div class="registration">
            <!-- Don't have an account yet?<br /> -->
            <!-- <a class="" href="#">
              Create an account
            </a> -->
          </div>
        </div>
      </form>
    </div>
  </div>

  <?php $this->load->view("partials/alert.php") ?>
  <?php $this->load->view("partials/js.php") ?>
  <script>
    $('#form-login').submit(function(event) {
      event.preventDefault();
      var nik = $('#nik').val();
      var password = $('#password').val();

      if (!nik) {
        $("#alert_warning_login").trigger("click");
        return false;
      }
      if (!password) {
        $("#alert_warning_login").trigger("click");
        return false;
      }

      $.ajax({
        type: "POST",
        url: "Login/logon",
        data: $('#form-login').serialize(),
        success: function(response) {
          var result = JSON.parse(response);
          
          if (result.status == 422) {
            $("#alert_warning_login").trigger("click");
          }
          else{
            window.location = "<?php echo base_url('Dashboard')?>";
          }
        },
        error: function(response) {
          $("#alert_warning_login").trigger("click");
        }
      });

    });
  </script>
  <script>
    $.backstretch("assets/img/login.jpg", {
      speed: 500
    });
  </script>
</body>

</html>