

<div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          
        <!-- <h2 class="form-login-heading centered"><img src="</?php echo base_url(); ?>assets/img/jayakarta.png" width="190px"></h2> -->
          <?php
            $role = $this->session->userdata('role');
            $name = $this->session->userdata('name');
          ?>
          <p class="centered"><img src="<?php echo base_url(); ?>assets/img/man.png" class="img-circle" width="80"></a></p>
          <h5 class="centered"><?php echo ucwords($name);?></h5>
          <li class="mt">
            <a class="<?php if($this->uri->segment(1)=="Dashboard" || $this->uri->segment(1)=="dashboard"){echo "active";}?>" href="<?php echo base_url('Dashboard')?>">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>

          <?php
            if ($role == 'super_admin' || $role == 'admin'){
          ?>
          <li class="">
            <a class="<?php if($this->uri->segment(1)=="Users" || $this->uri->segment(1)=="users"){echo "active";}?>" href="<?php echo base_url('Users')?>">
              <i class="fa fa-users"></i>
              <span>Users</span>
            </a>
          </li>
          
          <li class="sub-menu">
          <a href="#" class="<?php if($this->uri->segment(1)=="MasterData" || $this->uri->segment(1)=="masterdata" ){echo "active";}?>">
              <i class="fa fa-database"></i>
              <span>Master Data</span>
              </a>
            <ul class="sub">
            <li class=""><a href="<?php echo site_url('MasterData/kendaraan')?>">Kendaraan</a></li>
            <li class=""><a href="<?php echo site_url('MasterData/rumahdinas')?>">Rumah Dinas</a></li>
            <li class=""><a href="<?php echo site_url('MasterData/gedung')?>">Gedung</a></li>
            <li class=""><a href="<?php echo site_url('MasterData/mesin')?>">Mesin</a></li>
            <li class=""><a href="<?php echo site_url('MasterData/spbu')?>">SPBU</a></li>
            <li class=""><a href="<?php echo site_url('MasterData/bengkel')?>">Bengkel</a></li>
            <li class=""><a href="<?php echo site_url('MasterData/vendor')?>">Vendor</a></li>
            
              <!-- <li><a href="general.html">Input</a></li>
              <li><a href="buttons.html">Data</a></li>
              <li><a href="panels.html">Report</a></li> -->
            </ul>
          </li>
          <li class="sub-menu">
            <a href="#" class="<?php if($this->uri->segment(1)=="Kendaraan" || $this->uri->segment(1)=="kendaraan"){echo "active";}?>">
              <i class="fa fa-truck"></i>
              <span>Kendaraan</span>
              </a>
            <ul class="sub">
              <!-- <li class=""><a href="<?php echo site_url('Kendaraan/add')?>">Input</a></li> -->
              <li class=""><a href="<?php echo site_url('Kendaraan/service')?>">Service</a></li>
              <li class=""><a href="<?php echo site_url('Kendaraan/bahan_bakar')?>">Bahan Bakar</a></li>
              
              <li class=""><a href="<?php echo site_url('Kendaraan/report_service')?>">Report Service</a></li>
              <li class=""><a href="<?php echo site_url('Kendaraan/report_bahan_bakar')?>">Report Bahan Bakar</a></li>
            </ul>
          </li>
          <li class="sub-menu">
          <a href="#" class="<?php if($this->uri->segment(1)=="Rumdin" || $this->uri->segment(1)=="rumdin"){echo "active";}?>">
              <i class="fa fa-home"></i>
              <span>Rumah Dinas</span>
              </a>
            <ul class="sub">
              <li class=""><a href="<?php echo site_url('Rumdin')?>">Pengajuan</a></li>
              <li class=""><a href="<?php echo site_url('Rumdin/report_rumah_dinas')?>">Report Rumah Dinas</a></li>
              
            </ul>
          </li>
          <li class="sub-menu">
          <a href="#" class="<?php if($this->uri->segment(1)=="Gedung" || $this->uri->segment(1)=="gedung"){echo "active";}?>">
              <i class="fa fa-building"></i>
              <span>Gedung</span>
              </a>
            <ul class="sub">
              <li class=""><a href="<?php echo site_url('Gedung')?>">Pengajuan</a></li>
              <li class=""><a href="<?php echo site_url('Gedung/report_gedung')?>">Report Gedung</a></li>
            </ul>
          </li>
          <li class="sub-menu">
          <a href="#" class="<?php if($this->uri->segment(1)=="Mesin" || $this->uri->segment(1)=="mesin"){echo "active";}?>">
              <i class="fa fa-wrench"></i>
              <span>Mesin</span>
              </a>
            <ul class="sub">
              <li class=""><a href="<?php echo site_url('Mesin')?>">Pengajuan</a></li>
              <li class=""><a href="<?php echo site_url('Mesin/report_mesin')?>">Report Mesin</a></li>
            </ul>
          </li>
          <?php               
            }
          ?>
          <?php
            if ($role == 'driver'){
          ?>
          <li class="sub-menu">
            <a href="#" class="<?php if($this->uri->segment(1)=="Kendaraan" || $this->uri->segment(1)=="kendaraan"){echo "active";}?>">
              <i class="fa fa-truck"></i>
              <span>Kendaraan</span>
              </a>
            <ul class="sub">
              <!-- <li class=""><a href="<?php echo site_url('Kendaraan/add')?>">Input</a></li> -->
              <li class=""><a href="<?php echo site_url('Kendaraan/service')?>">Service</a></li>
              <li class=""><a href="<?php echo site_url('Kendaraan/bahan_bakar')?>">Bahan Bakar</a></li>
            </ul>
          </li>
          <?php               
            }
          ?>
          <?php
            if ($role == 'mechanical_engineer' || $role == 'penghuni_rumah'){
          ?>
          <li class="sub-menu">
          <a href="#" class="<?php if($this->uri->segment(1)=="Rumdin" || $this->uri->segment(1)=="rumdin"){echo "active";}?>">
              <i class="fa fa-home"></i>
              <span>Rumah Dinas</span>
              </a>
            <ul class="sub">
              <li class=""><a href="<?php echo site_url('Rumdin')?>">Pengajuan</a></li>
              
            </ul>
          </li>
          <?php               
            }
          ?>
          <?php
            if ($role == 'mechanical_engineer' || $role =='safety'){
          ?>
          <li class="sub-menu">
          <a href="#" class="<?php if($this->uri->segment(1)=="Gedung" || $this->uri->segment(1)=="gedung"){echo "active";}?>">
              <i class="fa fa-building"></i>
              <span>Gedung</span>
              </a>
            <ul class="sub">
              <li class=""><a href="<?php echo site_url('Gedung')?>">Pengajuan</a></li>
            </ul>
          </li>
          <?php               
            }
          ?>
          <?php
            if ($role == 'mechanical_engineer'){
          ?>
          <li class="sub-menu">
          <a href="#" class="<?php if($this->uri->segment(1)=="Mesin" || $this->uri->segment(1)=="mesin"){echo "active";}?>">
              <i class="fa fa-wrench"></i>
              <span>Mesin</span>
              </a>
            <ul class="sub">
              <li class=""><a href="<?php echo site_url('Mesin')?>">Pengajuan</a></li>
            </ul>
          </li>
          <?php               
            }
          ?>
        </ul>
        <!-- sidebar menu end-->
      </div>