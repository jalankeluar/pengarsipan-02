<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Mesin extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Mesin_model');
		$this->load->model('Login_model');
	}

	public function index()
	{
		$check_session = $this->session->userdata('nik');

		if ($check_session != null) {
			$data = array(
				'jenis_mesin' => $this->Mesin_model->getJenisMesin(),
				'kerusakan'   => $this->Mesin_model->getKerusakan(),
				'vendor'      => $this->Mesin_model->getVendor(),
			);

			$this->load->view('mesin/index',$data);
		} 
		else{
			redirect('login');
		}
	}

	public function data_mesin()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'jenis_mesin',
			2 => 'kerusakan'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');
		
		$role = $this->session->userdata('role');

		$totalData = $this->Mesin_model->all_mesin_count($role);

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Mesin_model->all_mesin_data($limit, $start, $order, $dir, $role);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Mesin_model->search_mesin_data($limit, $start, $order, $dir, $search, $role);

			$totalFiltered = $this->Mesin_model->search_mesin_count($search, $role);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp            = '"'.$value->id.'"';

				$kerusakan_mesin = explode(',',$value->kerusakan);
				
				$hasil_kerusakan = '';
				foreach ($kerusakan_mesin as $key => $detail) {
					$nama = $this->Mesin_model->getKerusakan_id($detail);
					if($nama!=null){
						$hasil_kerusakan .= ''.$nama->kerusakan_mesin.', ';
					}
				}
				$hasil_kerusakan = rtrim($hasil_kerusakan, ", ");

				if($value->harga == null){
					$harga = null;
				}
				else{
					$harga = 'Rp. '.number_format($value->harga ,0,",",".");
				}

				$nestedData['no_aset']     = ucwords($value->no_aset);
				$nestedData['jenis_mesin'] = ucwords($value->jenis_mesin);
				// $nestedData['kerusakan']   = ucwords($value->kerusakan_mesin);
				$nestedData['kerusakan']   = ucwords($hasil_kerusakan);
				$nestedData['keterangan']  = ucwords($value->keterangan);
				$nestedData['harga']       = $harga;
				$nestedData['vendor']      = $value->vendor;
				// $nestedData['status']         = ucwords($value->status);
				// $nestedData['status_service'] = ucwords($value->status_service);
				// $nestedData['action']         = "<center>
				// <a onclick='return delete_mesin($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>"
				if($value->status == 'approved'){
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-success'>DISETUJUI</a>
					</center>";
				}
				elseif($value->status == 'cancel'){
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-danger'>DITOLAK</a>
					</center>";
				}
				else{
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-warning'>MENUNGGU</a>
					</center>";
				}

				if($value->status_service == 'done'){
					
					$nestedData['status_service']     = "<center>
					<a class='btn btn-xs btn-success'>SELESAI</a>
					</center>";
				}
				elseif($value->status_service == 'process'){
					$nestedData['status_service']     = "<center>
					<a class='btn btn-xs btn-warning'>PROSES</a>
					</center>";
				}
				else{
					$nestedData['status_service']     = "";
				}
				// $nestedData['status']        = ucwords($value->status);
				
			
				if ($role == 'verifikator' || $role == 'admin' || $role == 'super_admin'){
					
					if($value->status == 'approved'){
						if($value->status_service == 'process'){
							$nestedData['action']     = "<center>
							<a onclick='return update_service_berkas($_temp)' class='btn btn-xs btn-default'><i class='fa fa-pencil'></i> Update Berkas</a>
							<br><a onclick='return download_catatan($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-file'></i> Catatan</a> 
							<br><a onclick='return update_service($_temp)' class='btn btn-xs btn-default'><i class='fa fa-pencil'></i> Update Service</a>
							<br><a onclick='return delete_service($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> 
							</center>";
						}
						else{
							$nestedData['action']     = "<center>
							<a onclick='return download_catatan($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-file'></i> Catatan</a> 
							<br><a onclick='return download_catatan2($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-file'></i> Surat Tugas</a> 
							<br><a onclick='return update_service($_temp)' class='btn btn-xs btn-default'><i class='fa fa-pencil'></i> Update Service</a>
							<br><a onclick='return delete_service($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> 
							</center>";
						}
					}
					else{
						
						// <a onclick='return update_service($_temp)' class='btn btn-xs btn-default'><i class='fa fa-pencil'></i> Update</a>
						$nestedData['action']     = "<center>
						<a onclick='return update_service_berkas($_temp)' class='btn btn-xs btn-default'><i class='fa fa-pencil'></i> Update Berkas</a>
						<br><a onclick='return delete_service($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

					}
				}
				else{
					if($value->status == 'approved'){
						if($value->status_service == 'process'){
							$nestedData['action']     = "<center>
							<a onclick='return download_catatan($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-file'></i> Catatan</a> 
							<br><a onclick='return delete_service($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> 
							</center>";
						}
						else{
							$nestedData['action']     = "<center>
							<a onclick='return download_catatan($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-file'></i> Catatan</a> 
							<br><a onclick='return download_catatan2($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-file'></i> Surat Tugas</a> 
							<br><a onclick='return delete_service($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> 
							</center>";
						}
						
					}
					else{

						$nestedData['action']     = "<center>
						<a onclick='return delete_service($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

					}

				}

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function simpan()
	{
		
		$check_session = $this->session->userdata('nik');
		if ($check_session != null) {	
			$mesin = $this->Mesin_model;
			$nik   = $this->session->userdata('nik');
			$now   = date('Y-m-d H:i:s');

			$list_kerusakan = $this->input->post('kerusakan');
			$kerusakan = '';
			foreach ($list_kerusakan as $key => $value) {
				$kerusakan .= $value.',';
			}

			$data = array(
				'id'             => $this->uuid->v4(),
				'jenis_mesin'    => $this->input->post('jenis_mesin'),
				// 'kerusakan'      => $this->input->post('kerusakan'),
				'kerusakan'      => $kerusakan,
				'keterangan'     => $this->input->post('keterangan'),
				'status'         => 'menunggu',
				'status_service' => '',
				'created_by'     => $nik,
				'created_at'     => $now,
				// 'jenis_pekerjaan' => $this->input->post('jenis_pekerjaan'),
			);

			$mesin->save($data);

			$this->load->view("mesin/index", $data);
			redirect("mesin");
		} 
		else{
			redirect('login');
		}
		
	}

	public function edit_status_berkas($id)
	{
		$query = $this->db->query("SELECT * FROM mesin where id = '$id'")->row();

		$data = array(
			'query'     => $query,
		);
		echo json_encode($data);
	}

	
	public function edit_status($id)
	{
		$query = $this->db->query("SELECT * FROM mesin where id = '$id'")->row();

		$data = array(
			'query'     => $query,
		);
		echo json_encode($data);
	}

	public function update_status_berkas()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {
			$id     = $this->input->post('update_service_berkas_id');
			$status = $this->input->post('update_status_service_berkas');
			$vendor = $this->input->post('update_vendor_service_berkas');
			
			if($status=='approved'){
				$status_service = 'process';
			}
			else{
				$status_service = '';
			}
			$data = array(
				'status'         => $status,
				'status_service' => $status_service,
				'vendor'         => $vendor
			);
					
			$this->db->where('id', $id);
			$this->db->update('mesin', $data);
			redirect("Mesin");
		} else {

			redirect('login');
		}
		
	}

	public function update_status()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {
			$id     = $this->input->post('update_service_id');
			$status = $this->input->post('update_status_service');
			$harga  = $this->input->post('update_harga_service');
			
			$data = array(
				'status_service' => $status,
				'harga'          => $harga
			);
					
			$this->db->where('id', $id);
			$this->db->update('mesin', $data);
			redirect("mesin");
		} else {

			redirect('login');
		}
		
	}

	public function delete_mesin($id=null)
    {   
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {			
			if (!isset($id)) show_404();
			else{
				
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('mesin', $data);
				
				redirect("mesin");
			}
		} else {
			redirect('login');
		}
		
	}
	
	//========================================= REPORT MESIN ====================================
	public function report_mesin()
	{
		$check_session = $this->session->userdata('nik');
		if ($check_session != null) {
			$this->load->view('mesin/report_mesin');
		} 
		else{
			redirect('login');
		}
		
	}

	public function data_report_mesin()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'jenis_mesin',
			2 => 'kerusakan_mesin',
			3 => 'keterangan',
			4 => 'name',
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$tanggal1 = $this->input->post('tanggal1');
		$tanggal2 = $this->input->post('tanggal2');
		$awal     = ($tanggal1!=''?date('Y-m-d',strtotime($tanggal1)):date('Y-m-d'));
		$akhir    = ($tanggal2!=''?date('Y-m-d',strtotime($tanggal2)):date('Y-m-d'));
		
		$totalData = $this->Mesin_model->all_report_mesin_count($awal, $akhir);

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Mesin_model->all_report_mesin_data($limit, $start, $order, $dir, $awal, $akhir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Mesin_model->search_report_mesin_data($limit, $start, $order, $dir, $search, $awal, $akhir);

			$totalFiltered = $this->Mesin_model->search_report_mesin_count($search, $awal, $akhir);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp              = '"'.$value->id.'"';

				$kerusakan_mesin = explode(',',$value->kerusakan_mesin);
				$hasil_kerusakan = '';
				foreach ($kerusakan_mesin as $key => $detail) {
					$nama = $this->Mesin_model->getKerusakan_id($detail);
					if($nama!=null){
						$hasil_kerusakan .= ''.$nama->kerusakan_mesin.', ';
					}
				}
				$hasil_kerusakan = rtrim($hasil_kerusakan, ", ");

				if($value->harga == null){
					$harga = null;
				}
				else{
					$harga = 'Rp. '.number_format($value->harga ,0,",",".");
				}
				

				$nestedData['created_at']      = ucwords(date('d-m-Y',strtotime($value->created_at)));
				$nestedData['jenis_mesin']     = ucwords($value->jenis_mesin);
				$nestedData['no_aset']    	   = ucwords($value->no_aset);
				// $nestedData['kerusakan_mesin'] = ucwords($value->kerusakan_mesin);
				$nestedData['kerusakan_mesin'] = ucwords($kerusakan_mesin);
				$nestedData['harga']          = $harga;
				$nestedData['keterangan']      = ucwords($value->keterangan);
				$nestedData['name']            = ucwords($value->name);

				if($value->status == 'approved'){
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-success'>DISETUJUI</a>
					</center>";
				}
				elseif($value->status == 'cancel'){
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-danger'>DITOLAK</a>
					</center>";
				}
				else{
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-warning'>MENUNGGU</a>
					</center>";
				}

				if($value->status_service == 'done'){
					
					$nestedData['status_service']     = "<center>
					<a class='btn btn-xs btn-success'>SELESAI</a>
					</center>";
				}
				elseif($value->status_service == 'process'){
					$nestedData['status_service']     = "<center>
					<a class='btn btn-xs btn-warning'>PROSES</a>
					</center>";
				}
				else{
					$nestedData['status_service']     = "";
				}


				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	 
    public function export_report_mesin(){
		// include APPPATH.'third_party/Classes/PHPsheet.php';
        
        // include $_SERVER["DOCUMENT_ROOT"].'/pengarsipan-02/application/third_party/Classes/PHPsheet/IOFactory.php';

		$tanggal1 = $this->input->get('tanggal1');
		$tanggal2 = $this->input->get('tanggal2');
		$dari     = ($tanggal1!=''?date('Y-m-d',strtotime($tanggal1)):date('Y-m-d'));
		$sampai   = ($tanggal2!=''?date('Y-m-d',strtotime($tanggal2)):date('Y-m-d'));

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();;

		$sheet->setCellValue('A1', "NO");
		$sheet->getColumnDimension('A')->setAutoSize(true); 

		$sheet->setCellValue('B1', "TANGGAL");
		$sheet->getColumnDimension('B')->setAutoSize(true); 

		$sheet->setCellValue('C1', "NO ASET");
		$sheet->getColumnDimension('C')->setAutoSize(true); 

		$sheet->setCellValue('D1', "JENIS MESIN");
		$sheet->getColumnDimension('D')->setAutoSize(true); 

		$sheet->setCellValue('E1', "HARGA");
		$sheet->getColumnDimension('E')->setAutoSize(true); 

		$sheet->setCellValue('F1', "KERUSAKAN");
		$sheet->getColumnDimension('F')->setAutoSize(true); 

		$sheet->setCellValue('G1', "KETERANGAN");
		$sheet->getColumnDimension('G')->setAutoSize(true); 

		$sheet->setCellValue('H1', "DIAJUKAN OLEH (NIK)");
		$sheet->getColumnDimension('H')->setAutoSize(true); 

		$sheet->setCellValue('I1', "DIAJUKAN OLEH (NAMA)");
		$sheet->getColumnDimension('I')->setAutoSize(true); 

		$sheet->setCellValue('J1', "STATUS BERKAS");
		$sheet->getColumnDimension('J')->setAutoSize(true); 
		
		$sheet->setCellValue('K1', "STATUS SERVICE");
		$sheet->getColumnDimension('K')->setAutoSize(true); 

		$details = $this->Mesin_model->export_report_mesin($dari,$sampai);

		$no     = 1;
		$numrow = 2;
		foreach ($details as $key => $detail) {

			$sheet->setCellValue('A'.$numrow, $no);
			$sheet->setCellValue('B'.$numrow, date('d-m-Y',strtotime($detail->created_at)));
			$sheet->setCellValue('C'.$numrow, ucwords($detail->no_aset));
			$sheet->setCellValue('D'.$numrow, ucwords($detail->jenis_mesin));
			
			$kerusakan_mesin = explode(',',$detail->kerusakan_mesin);
			$hasil_kerusakan = '';
			foreach ($kerusakan_mesin as $key => $value) {
				$nama = $this->Mesin_model->getKerusakan_id($value);
				if($nama!=null){
					$hasil_kerusakan .= ''.$nama->kerusakan_mesin.', ';
				}
			}
			$hasil_kerusakan = rtrim($hasil_kerusakan, ", ");

			if($detail->harga == null){
				$harga = null;
			}
			else{
				$harga = 'Rp. '.number_format($detail->harga ,0,",",".");
			}
			$sheet->setCellValue('E'.$numrow, ucwords($kerusakan_mesin));
			$sheet->setCellValue('F'.$numrow, ucwords($harga));
			$sheet->setCellValue('G'.$numrow, ucwords($detail->keterangan));
			$sheet->setCellValue('H'.$numrow, ucwords($detail->created_by));
			$sheet->setCellValue('I'.$numrow, ucwords($detail->name));
			if($detail->status == 'approved'){
				$status = 'disetujui';
			}
				elseif($detail->status == 'cancel'){
			$status = 'ditolak';
			}
			else{
				$status = 'menunggu';
			}

			if($detail->status_service == 'done'){		
				$status_service = 'selesai';
			}
			elseif($detail->status_service == 'process'){
				$status_service = 'proses';
			}
			else{
				$status_service = "-";
			}
			
			$sheet->setCellValue('J'.$numrow, ucwords($status));
			$sheet->setCellValue('K'.$numrow, ucwords($status_service));

			$no++;
			$numrow++;
		}

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		
		$sheet->getDefaultRowDimension()->setRowHeight(-1);

		$writer   = new Xlsx($spreadsheet);
		$filename = 'Laporan Mesin '.$tanggal1.' sd '.$tanggal2.' ';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
		
		ob_end_clean();+
		$writer->save('php://output');
    }
}
