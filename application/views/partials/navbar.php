<div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="<?php echo base_url('Dashboard')?>" class="logo"><img src="<?php echo base_url();?>assets/img/jayakarta.png" width="100"></a>
      <!--logo end-->
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="<?php echo site_url('login/logout'); ?>">Logout</a></li>
        </ul>
      </div>