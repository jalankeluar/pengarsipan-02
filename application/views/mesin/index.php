<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker.css') ?>" />
</head>

<body>
  <section id="container">
    <!-- ******************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        ******************************************************************************************************************************************************* -->
    <!--header start-->
    <header class="header black-bg">
      <?php $this->load->view("partials/navbar.php") ?>
    </header>
    <!--header end-->
    <!-- ******************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        ******************************************************************************************************************************************************* -->
    <!--sidebar start-->
    <aside>
      <?php $this->load->view("partials/sidebar.php") ?>
    </aside>
    <!--sidebar end-->
    <!-- ******************************************************************************************************************************************************
        MAIN CONTENT
        ******************************************************************************************************************************************************* -->
    <!--main content start-->
    <section id="main-content">
      <div class="site-container">
        <div class="site-content">
          <section class="wrapper">
              <div class="table-responsive container col-sm-12" style="margin-top:20px">
                <h1>Data <small>Mesin</small>
                  <div class="pull-right"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_add_new"> Tambah Data</a></div>
                </h1><br>
                <div class="alert alert-warning fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="icon-remove"></i>
                  </button>
                  <strong>INFORMASI</strong><br> Pencarian data berdasarkan <b>jenis mesin</b> dan <b>kerusakan</b>.
                </div>
                <table class="table table-bordered table-striped" id="mesin-table" width="100%" cellspacing="0">
                  <thead>
                    <tr class="info">
                    <th style="text-align:center">No</th>
                      <th style="text-align:center">No Aset</th>
                      <th style="text-align:center">Jenis Mesin</th>
                      <th style="text-align:center">Kerusakan</th>
                      <th style="text-align:center">Harga</th>
                      <th style="text-align:center">Vendor</th>
                      <!-- <th style="text-align:center">Jenis Pekerjaan</th> -->
                      <th style="text-align:center">Keterangan</th>
                      <th style="text-align:center">Status Berkas</th>
                      <th style="text-align:center">Status Service</th>
                      <th style="text-align:center">action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
          </section>

        </div>
        <button type="button" class="hidden" id="refresh"></button>
        <!--footer start-->
        <footer class="site-footer">
          <?php $this->load->view("partials/footer.php") ?>
        </footer>
        <!--footer end-->
      </div>
    </section>
    <!--main content end-->
  </section>

  <!-- ============ MODAL ADD BARANG =============== -->
  <div class="modal fade" id="modal_add_new" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Tambah Data Mesin</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'Mesin/simpan' ?>">
          <div class="modal-body">

            <!-- <div class="form-group">
              <label class="control-label col-xs-3">Lantai</label>
              <div class="col-xs-8">
                <input name="lantai" class="form-control" type="text" placeholder="Lantai" required>
              </div>
            </div> -->

            <div class="form-group">
            <label class="control-label col-xs-3">Jenis Mesin</label>
              <div class="col-xs-8">
                <select class="form-control" style="width: 100%" name="jenis_mesin" id="jenis_mesin" required>
                  <option value="">Pilih Jenis Mesin</option>
                  <?php
                      foreach ($jenis_mesin as $l) {
                          echo '<option value="' . $l->id . '">';
                          echo $l->no_aset." - ".$l->jenis_mesin;
                          echo '</option>';
                      }
                  ?>
                </select>
              </div>
            </div>

            <!-- <div class="form-group">
              <label class="control-label col-xs-3">Kerusakan</label>
              <div class="col-xs-8">
                <input name="kerusakan" class="form-control" type="text" placeholder="Kerusakan" required>
              </div>
            </div> -->

            <div class="form-group">
              <label class="control-label col-xs-3">Kerusakan</label>
              <div class="col-xs-8">
                  <div class="multi-select-full">
                    <select class="multiselect-filtering" multiple="multiple" id="kerusakan" style="width: 100%"
                      name="kerusakan[]" required>
                    <!-- <select class="form-control"  style="width: 100%" name="kerusakan" id="kerusakan" required> -->
                      <!-- <option value="">Pilih Kerusakan</option> -->
                      <?php
                          foreach ($kerusakan as $kg) {
                              echo '<option value="' . $kg->id . '">';
                              echo $kg->kerusakan_mesin;
                              echo '</option>';
                          }
                      ?>
                    </select>
                  </div>
              </div>
            </div>

            <!-- <div class="form-group">
              <label class="control-label col-xs-3">Jenis Pekerjaan</label>
              <div class="col-xs-8">
                <input name="jenis_pekerjaan" class="form-control" type="text" placeholder="Jenis Pekerjaan..." required>
              </div>
            </div> -->

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="keterangan" name="keterangan" placeholder="max. 255 karakter"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  
	<div class="modal fade" id="modal_service_update_new" tabindex="-1" role="dialog" aria-labelledby="largeModal"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 class="modal-title" id="myModalLabel">Update Status Data</h3>
				</div>
				<form class="form-horizontal" method="post" action="<?php echo base_url() . 'Mesin/update_status' ?>">
					<div class="modal-body">

						<input type="text" id="update_service_id" class="form-control hidden" name="update_service_id">

						<div class="form-group">
							<label class="control-label col-xs-3">Status</label>
							<div class="col-xs-8">
								<select class="form-control" name="update_status_service" style="width: 100%" id="update_status_service"
									required>
									<option value="">Pilih Status</option>
									<!-- <option value="process">Proses</option> -->
									<option value="done">Selesai</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-xs-3">Harga</label>
							<div class="col-xs-8">
								<input name="update_harga_service" id="update_harga_service" class="form-control" type="number"
									onkeypress="return onlyNumberKey(event)" placeholder="Harga" required>
							</div>
						</div>


					</div>

					<div class="modal-footer">
						<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
						<button class="btn btn-info">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal_service_berkas_update_new" tabindex="-1" role="dialog" aria-labelledby="largeModal"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 class="modal-title" id="myModalLabel">Update Status Data</h3>
				</div>
				<form class="form-horizontal" method="post"
					action="<?php echo base_url() . 'Mesin/update_status_berkas' ?>">
					<div class="modal-body">

						<input type="text" id="update_service_berkas_id" class="form-control hidden"
							name="update_service_berkas_id">

						<div class="form-group">
							<label class="control-label col-xs-3">Status</label>
							<div class="col-xs-8">
								<select class="form-control" name="update_status_service_berkas" style="width: 100%"
									id="update_status_service_berkas" required>
									<option value="">Pilih Status</option>
									<option value="approved">Setujui</option>
									<option value="cancel">Tolak</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-xs-3">Vendor</label>
							<div class="col-xs-8">
								<select class="form-control" name="update_vendor_service_berkas" style="width: 100%"
									id="update_vendor_service_berkas" required>
									<option value="">Pilih Vendor</option>
									<?php
                        foreach ($vendor as $m) {
                            echo '<option value="' . $m->id . '">';
                            echo $m->vendor;
                            echo '</option>';
                        }
                    ?>
								</select>
							</div>
						</div>

					</div>

					<div class="modal-footer">
						<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
						<button class="btn btn-info">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>

  <!-- js placed at the end of the document so the pages load faster -->
  <?php $this->load->view("partials/js.php") ?>
  <?php $this->load->view("partials/modal.php") ?>
  
  <script type="text/javascript">
    $(document).ready(function() {
      // $('.select-search').select2();
      $('#kerusakan').select2({
        placeholder: 'Pilih Kerusakan'
      });

      var table = $('#mesin-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "mesin/data_mesin",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "no_aset"},
          { "data": "jenis_mesin"},
          { "data": "kerusakan"},
          { "data": "harga"},
          { "data": "vendor"},
          { "data": "keterangan", 'sortable': false},
          { "data": "status", 'sortable': false},
          { "data": "status_service", 'sortable': false},
          { "data": "action"},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#mesin-table_filter input').unbind();
      $('#mesin-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0) {
          table.search($(this).val()).draw();
        }
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#mesin-table').DataTable().ajax.reload();
      });

    });

    
		function update_service(id) {
			$.ajax({
					type: "get",
					url: 'Mesin/edit_status/' + id,
				})
				.done(function (response) {
					var result = JSON.parse(response)

					$('#update_service_id').val(result.query.id);
					$('#update_harga_service').val(result.query.harga);

					$("#modal_service_update_new").modal('show');

				});
		}

		function update_service_berkas(id) {
			$.ajax({
					type: "get",
					url: 'Mesin/edit_status_berkas/' + id,
				})
				.done(function (response) {
					var result = JSON.parse(response)
					$('#update_service_berkas_id').val(result.query.id);

					$("#modal_service_berkas_update_new").modal('show');

				});
		}


    function delete_mesin(id){
      var url = '<?php echo site_url('mesin/delete_mesin/') ?>'+id;
      // console.log(url);
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }

    
		function onlyNumberKey(evt) {
      // Only ASCII charactar in that range allowed 
      var ASCIICode = (evt.which) ? evt.which : evt.keyCode
      if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
      return true;
    }
  </script>

</body>

</html>