-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2020 at 03:23 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.2.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pengarsipan`
--

-- --------------------------------------------------------

--
-- Table structure for table `gedung`
--

CREATE TABLE `gedung` (
  `id` char(36) NOT NULL,
  `lantai` varchar(100) NOT NULL,
  `kerusakan` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `gedung`
--

INSERT INTO `gedung` (`id`, `lantai`, `kerusakan`, `keterangan`, `created_by`, `created_at`, `deleted_by`, `deleted_at`) VALUES
('0c0bd411-5993-4a15-9e42-5bf4bf411cc0', '60bc2214-0645-45ea-94b4-41c349cf9f4a', NULL, '', '', '2020-08-02 14:12:17.607031', NULL, NULL),
('3dffab98-e1b5-45b4-a642-9d0a788f7e5e', 'de98b13f-2102-473b-b68c-6538c7846133', 'cb5e2162-4a20-4e78-bddd-82b66636e411', 'm', '1111', '2020-08-26 15:50:29.000000', NULL, NULL),
('51e78a09-579a-4c83-ab37-560b03439a62', 'de98b13f-2102-473b-b68c-6538c7846133', '18b132f3-b1f9-4da9-8c63-720979d5a07d', '', '1111', '2020-08-03 22:40:26.000000', NULL, NULL),
('67cc6eb8-87d7-479c-82a9-d6bb7918fcaf', '60bc2214-0645-45ea-94b4-41c349cf9f4a', '18b132f3-b1f9-4da9-8c63-720979d5a07d', '', '1111', '2020-08-03 22:40:18.000000', NULL, NULL),
('7ea2f079-4b93-4e80-bdc8-e35664345ba3', 'de98b13f-2102-473b-b68c-6538c7846133', 'cb5e2162-4a20-4e78-bddd-82b66636e411', '', '1111', '2020-08-02 09:20:45.000000', NULL, NULL),
('9e5fc9c3-a279-4cde-99c2-b453595acae4', '', '', '', '1111', '2020-08-02 09:20:50.000000', NULL, NULL),
('d3092ab5-2c9e-4fdf-b2ec-1f846948afed', 'de98b13f-2102-473b-b68c-6538c7846133', '18b132f3-b1f9-4da9-8c63-720979d5a07d', '', '1111', '2020-08-02 09:21:15.000000', NULL, NULL),
('ecabcc06-63c1-4a6f-9577-21ebf7366d8d', '60bc2214-0645-45ea-94b4-41c349cf9f4a', 'cb5e2162-4a20-4e78-bddd-82b66636e411', 'ajajjaja', '1111', '2020-08-03 22:54:43.000000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `g_kerusakan`
--

CREATE TABLE `g_kerusakan` (
  `id` char(36) NOT NULL,
  `kerusakan_gedung` varchar(30) DEFAULT NULL,
  `keterangan` varchar(300) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `g_kerusakan`
--

INSERT INTO `g_kerusakan` (`id`, `kerusakan_gedung`, `keterangan`, `created_by`, `created_at`, `deleted_by`, `deleted_at`) VALUES
('18b132f3-b1f9-4da9-8c63-720979d5a07d', 'lantai rusak', 'betulin dong ok', '1111', '2020-08-02 09:11:47.000000', NULL, NULL),
('cb5e2162-4a20-4e78-bddd-82b66636e411', 'pintu patah', 'betulin', '1111', '2020-08-02 09:12:02.000000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `g_lantai`
--

CREATE TABLE `g_lantai` (
  `id` char(36) NOT NULL,
  `lantai` varchar(30) DEFAULT NULL,
  `keterangan` varchar(300) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `g_lantai`
--

INSERT INTO `g_lantai` (`id`, `lantai`, `keterangan`, `created_by`, `created_at`, `deleted_by`, `deleted_at`) VALUES
('60bc2214-0645-45ea-94b4-41c349cf9f4a', '1', 'ground', '1111', '2020-08-02 09:11:31.000000', NULL, NULL),
('de98b13f-2102-473b-b68c-6538c7846133', '10', 'rooftop', '1111', '2020-07-28 07:57:32.000000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kendaraan`
--

CREATE TABLE `kendaraan` (
  `id` char(36) NOT NULL,
  `no_polisi` varchar(100) NOT NULL,
  `service` varchar(255) DEFAULT NULL,
  `pengajuan_date` timestamp(6) NULL DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `harga` varchar(100) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `created_by` varchar(100) NOT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL,
  `deleted_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `kendaraan`
--

INSERT INTO `kendaraan` (`id`, `no_polisi`, `service`, `pengajuan_date`, `keterangan`, `harga`, `status`, `created_at`, `created_by`, `deleted_at`, `deleted_by`) VALUES
('123', '123', '123', '2020-06-27 06:14:20.000000', '1', '12', 'approved', '2020-06-27 06:14:32.094883', 'hussein', NULL, NULL),
('62710a97-76aa-4c9e-872e-34821e483b77', '1', '123', '2020-07-02 17:00:00.000000', '', '10000', NULL, '2020-07-30 21:05:33.872106', '', NULL, NULL),
('7913b9aa-1b51-459a-92c2-400bb61bec04', '1', '1', '2020-07-06 17:00:00.000000', 'asd', '12333', NULL, '2020-07-07 14:58:03.908938', '', NULL, NULL),
('8371d98b-4544-4743-ac2c-4a104c53d9bd', '1', '123', '2020-08-03 17:00:00.000000', '', '5000', NULL, '2020-08-02 13:41:48.583910', '', NULL, NULL),
('970f8c96-1e6c-4825-9b05-9e7c4e972f43', '1', '123', '2020-07-09 17:00:00.000000', 'ma', '12333', NULL, '2020-07-10 09:40:59.712100', '', NULL, NULL),
('976d5c2e-203d-4742-8e44-12813aa90c80', '6d4d77ee-d60f-419a-bbc5-5b1588b8897f', '1fe39840-10d7-4e9a-818c-ae09dea00e01', '2020-08-02 17:00:00.000000', '', '5000', NULL, '2020-08-02 18:47:54.000000', '1111', NULL, NULL),
('bfda8871-300e-43a7-8058-5a576a15dc76', '1', '123', '2020-08-01 17:00:00.000000', '', '5000', NULL, '2020-08-02 08:44:12.000000', '1111', NULL, NULL),
('eb0319a1-552b-411f-b5c0-d99c923fb091', '6d4d77ee-d60f-419a-bbc5-5b1588b8897f', '123', '2020-08-04 17:00:00.000000', '', '5000', NULL, '2020-08-05 07:53:15.000000', '1111', NULL, NULL),
('ec41b11e-c9be-49ec-81ce-9a8574794637', '6d4d77ee-d60f-419a-bbc5-5b1588b8897f', '1fe39840-10d7-4e9a-818c-ae09dea00e01', '2020-08-03 17:00:00.000000', 'asd', '10000', NULL, '2020-08-03 23:34:13.000000', '1111', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `k_bahan_bakar`
--

CREATE TABLE `k_bahan_bakar` (
  `id` char(36) NOT NULL,
  `no_nota` varchar(30) DEFAULT NULL,
  `no_polisi` varchar(36) DEFAULT NULL,
  `tgl` timestamp(6) NULL DEFAULT current_timestamp(6),
  `spbu` varchar(100) DEFAULT NULL,
  `harga` int(13) DEFAULT NULL,
  `ttl_bayar` int(100) DEFAULT NULL,
  `jml_liter` varchar(10) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `k_bahan_bakar`
--

INSERT INTO `k_bahan_bakar` (`id`, `no_nota`, `no_polisi`, `tgl`, `spbu`, `harga`, `ttl_bayar`, `jml_liter`, `created_by`, `created_at`, `deleted_by`, `deleted_at`) VALUES
('34c320e3-260b-436a-b53b-2c8d00880fdd', 'A123', '1', '2020-08-03 17:00:00.000000', 'pamularsih', 12333, 20000, '1.62166545', '1111', '2020-08-03 23:34:31.000000', NULL, NULL),
('4fada61a-ce08-4535-bf7f-9ac7e4d62aec', 'W99', '6d4d77ee-d60f-419a-bbc5-5b1588b8897f', '2020-08-02 17:00:00.000000', 'pamularsih', 5000, 90000, '18', '1111', '2020-08-02 21:37:21.000000', NULL, NULL),
('7da922f4-2454-4686-81f0-1e4a755f36a7', 'A123', '6d4d77ee-d60f-419a-bbc5-5b1588b8897f', '2020-08-03 17:00:00.000000', 'pamularsih', 5000, 90000, '18', '1111', '2020-08-02 21:37:35.000000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `k_nopolisi`
--

CREATE TABLE `k_nopolisi` (
  `id` char(36) NOT NULL,
  `no_polisi` varchar(30) DEFAULT NULL,
  `pajak` date DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  `stnk` date DEFAULT NULL,
  `type` varchar(300) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `k_nopolisi`
--

INSERT INTO `k_nopolisi` (`id`, `no_polisi`, `pajak`, `tahun`, `stnk`, `type`, `created_by`, `created_at`, `deleted_by`, `deleted_at`) VALUES
('1', '1299', '2020-08-13', '2005asd', '0000-00-00', '123asd', '1111', '2020-07-01 16:31:33.000000', NULL, NULL),
('6d4d77ee-d60f-419a-bbc5-5b1588b8897f', 'H 4553 IN', '2020-08-27', '2020', '0000-00-00', 'porsche', '1111', '2020-08-02 15:37:56.000000', NULL, NULL),
('e678afbb-8f2b-468a-acf6-16ea9058bd55', 'H 2987 KI', '2020-08-28', '2014', '2020-08-10', '-', '1111', '2020-08-28 08:18:26.000000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `k_service`
--

CREATE TABLE `k_service` (
  `id` char(36) NOT NULL,
  `servis` varchar(30) DEFAULT NULL,
  `keterangan` varchar(300) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `k_service`
--

INSERT INTO `k_service` (`id`, `servis`, `keterangan`, `created_by`, `created_at`, `deleted_by`, `deleted_at`) VALUES
('123', '132sssssssssssssss', '12asdasd', '1111', '2020-07-01 16:31:50.353653', NULL, NULL),
('1fe39840-10d7-4e9a-818c-ae09dea00e01', 'cuci mobil', 'halo', '1111', '2020-08-02 15:35:22.000000', NULL, NULL),
('5cf3ed29-b14e-4ed4-b9be-d03186fcdd95', 'hal', 'la', '1111', '2020-08-04 01:26:44.000000', NULL, NULL),
('63666b78-0833-4429-a6e7-cab4735ec82f', 'cuci mobil', '', '1111', '2020-07-03 07:14:04.000000', '1111', '2020-07-05 00:18:17.000000');

-- --------------------------------------------------------

--
-- Table structure for table `mesin`
--

CREATE TABLE `mesin` (
  `id` char(36) NOT NULL,
  `jenis_mesin` varchar(100) NOT NULL,
  `kerusakan` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `mesin`
--

INSERT INTO `mesin` (`id`, `jenis_mesin`, `kerusakan`, `keterangan`, `created_by`, `created_at`, `deleted_by`, `deleted_at`) VALUES
('dd4fbd51-5089-4b10-b707-06b4698f441b', 'f3b2745c-0d79-4312-9fed-eaee23cfee43', 'cb8e8f7d-f2c6-4f28-afaf-681dac86b58f', '', '1111', '2020-08-04 00:32:08.000000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_jenis_mesin`
--

CREATE TABLE `m_jenis_mesin` (
  `id` char(36) NOT NULL,
  `jenis_mesin` varchar(30) DEFAULT NULL,
  `no_aset` varchar(100) NOT NULL,
  `keterangan` varchar(300) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_jenis_mesin`
--

INSERT INTO `m_jenis_mesin` (`id`, `jenis_mesin`, `no_aset`, `keterangan`, `created_by`, `created_at`, `deleted_by`, `deleted_at`) VALUES
('e7d42a4e-e298-49da-9e4b-5459d0b46fa8', 'as', '', '', '1111', '2020-08-04 00:00:58.000000', '1111', '2020-08-04 00:01:37.000000'),
('f3b2745c-0d79-4312-9fed-eaee23cfee43', 'x', '', '', '1111', '2020-08-04 00:01:33.000000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_kerusakan`
--

CREATE TABLE `m_kerusakan` (
  `id` char(36) NOT NULL,
  `kerusakan_mesin` varchar(30) DEFAULT NULL,
  `keterangan` varchar(300) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_kerusakan`
--

INSERT INTO `m_kerusakan` (`id`, `kerusakan_mesin`, `keterangan`, `created_by`, `created_at`, `deleted_by`, `deleted_at`) VALUES
('654f9724-09ec-4c95-a562-06953a81d8c3', 'asd', 'asd', '1111', '2020-08-04 01:30:22.000000', NULL, NULL),
('9930ffc2-4603-4643-92c1-4c195b20fdb8', 'sdf', '', '1111', '2020-08-04 00:01:13.000000', '1111', '2020-08-04 00:01:42.000000'),
('cb8e8f7d-f2c6-4f28-afaf-681dac86b58f', 'asd', '', '1111', '2020-08-04 00:01:27.000000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rd_alamat`
--

CREATE TABLE `rd_alamat` (
  `id` char(36) NOT NULL,
  `nama_penghuni` varchar(255) DEFAULT NULL,
  `alamat` varchar(30) DEFAULT NULL,
  `keterangan` varchar(300) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `rd_alamat`
--

INSERT INTO `rd_alamat` (`id`, `nama_penghuni`, `alamat`, `keterangan`, `created_by`, `created_at`, `deleted_by`, `deleted_at`) VALUES
('c5bafed3-2922-488d-87fe-2c9f30f32047', 'll', 'qwe', '', '1111', '2020-07-30 15:49:27.000000', '1111', '2020-08-05 06:56:34.000000'),
('ce0bd047-eda7-4918-992a-d9252108ccae', 'we', 'pkealongan', 'we', '1111', '2020-08-05 07:32:07.000000', NULL, NULL),
('dd729cbc-17d2-49c4-8d70-9b16ea392638', 'll', 'pemalang', 'ad', '1111', '2020-08-05 07:31:57.000000', NULL, NULL),
('ebfe0347-8273-4e86-982e-3fbe0edcff04', 'mmasd', 'b', '', '1111', '2020-07-30 14:40:18.000000', '1111', '2020-08-05 06:57:54.000000');

-- --------------------------------------------------------

--
-- Table structure for table `rd_kerusakan`
--

CREATE TABLE `rd_kerusakan` (
  `id` char(36) NOT NULL,
  `kerusakan` varchar(30) DEFAULT NULL,
  `keterangan` varchar(300) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `rd_kerusakan`
--

INSERT INTO `rd_kerusakan` (`id`, `kerusakan`, `keterangan`, `created_by`, `created_at`, `deleted_by`, `deleted_at`) VALUES
('36c7d08d-a91b-4268-a8b9-cf418a0adea5', 'asd', 'asdasd', '1111', '2020-07-30 14:39:57.000000', NULL, NULL),
('8199f485-dc42-4bfc-9828-91c1dc26d9b0', 'pop', 'pop\r\n', '1111', '2020-08-04 01:30:38.000000', '1111', '2020-08-04 01:30:43.000000'),
('beb6da00-6c4b-4d3b-9607-58f3485df1ef', 's', '', '1111', '2020-07-30 14:40:36.000000', '1111', '2020-08-04 01:28:14.000000');

-- --------------------------------------------------------

--
-- Table structure for table `rumah_dinas`
--

CREATE TABLE `rumah_dinas` (
  `id` char(36) NOT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `kerusakan` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `rumah_dinas`
--

INSERT INTO `rumah_dinas` (`id`, `alamat`, `kerusakan`, `keterangan`, `status`, `created_by`, `created_at`, `deleted_by`, `deleted_at`) VALUES
('1f0e97a1-b673-43fd-b4be-41ed09f598c8', 'dd729cbc-17d2-49c4-8d70-9b16ea392638', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', 'kk', 'cancel', '1111', '2020-08-27 04:00:22.000000', NULL, NULL),
('4b7a44c5-7ad2-422f-ba3b-d560426a3235', 'dd729cbc-17d2-49c4-8d70-9b16ea392638', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', NULL, NULL, '1111', '2020-08-05 07:32:36.000000', NULL, NULL),
('69a6d2b7-0df0-4ada-9426-01fb3b5d4402', 'dd729cbc-17d2-49c4-8d70-9b16ea392638', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', 'as', 'approved', '28', '2020-08-27 04:04:29.000000', NULL, NULL),
('72847694-fea9-4538-bed3-9d08880b998f', 'ebfe0347-8273-4e86-982e-3fbe0edcff04', 'beb6da00-6c4b-4d3b-9607-58f3485df1ef', NULL, 'cancel', '', '2020-08-02 20:11:51.668770', NULL, NULL),
('843580b8-b1a3-400e-96c0-ddabbe9826f8', 'ebfe0347-8273-4e86-982e-3fbe0edcff04', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', NULL, NULL, '1111', '2020-08-03 23:37:07.000000', NULL, NULL),
('a62774d8-f81c-4460-88dc-07204007cea7', 'c5bafed3-2922-488d-87fe-2c9f30f32047', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', NULL, 'approved', '1111', '2020-08-03 22:11:38.000000', NULL, NULL),
('b43af1f7-c8f9-4a15-ae64-b2cea3e9d7dc', 'ebfe0347-8273-4e86-982e-3fbe0edcff04', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', NULL, NULL, '1111', '2020-08-03 22:11:46.000000', NULL, NULL),
('b7c73024-9f46-4c07-901c-032426df673f', 'dd729cbc-17d2-49c4-8d70-9b16ea392638', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', NULL, NULL, '1111', '2020-08-05 07:32:24.000000', NULL, NULL),
('e7c87e7f-972d-4c6a-8b8a-96ed19d12e37', 'c5bafed3-2922-488d-87fe-2c9f30f32047', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', NULL, 'approved', '', '2020-08-02 20:11:44.288182', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `nik` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `role` varchar(100) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nik`, `name`, `email`, `password`, `role`, `created_by`, `created_at`, `deleted_by`, `deleted_at`) VALUES
('9987dc98-8237-4727-833c-c8f638704a55', '28', 'mm', 'math@gmail', '28', 'mechanical_engineer', '1111', '2020-08-09 08:36:36.000000', NULL, NULL),
('a', '1111', 'super admin', 'tes@tes.com', '1111', 'super_admin', 'superadmin', '2020-06-21 06:49:22.519552', NULL, NULL),
('b3384fc9-fbef-4bdd-af71-214f26ea2104', '2222', 'mat', 'mat@tes.com', 'hus', 'Admin', '', '2020-07-07 15:08:43.250602', '1111', '2020-07-30 08:54:43.000000'),
('d5b28d86-7be0-4bc5-8916-06fe119f3759', '5', 'math', 'math@gmail', '5555', 'admin', '1111', '2020-07-30 08:58:07.000000', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gedung`
--
ALTER TABLE `gedung`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `g_kerusakan`
--
ALTER TABLE `g_kerusakan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `g_lantai`
--
ALTER TABLE `g_lantai`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `k_bahan_bakar`
--
ALTER TABLE `k_bahan_bakar`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `k_nopolisi`
--
ALTER TABLE `k_nopolisi`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `k_service`
--
ALTER TABLE `k_service`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `mesin`
--
ALTER TABLE `mesin`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `m_jenis_mesin`
--
ALTER TABLE `m_jenis_mesin`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `m_kerusakan`
--
ALTER TABLE `m_kerusakan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `rd_alamat`
--
ALTER TABLE `rd_alamat`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `rd_kerusakan`
--
ALTER TABLE `rd_kerusakan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `rumah_dinas`
--
ALTER TABLE `rumah_dinas`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
