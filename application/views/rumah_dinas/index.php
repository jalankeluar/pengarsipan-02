<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker.css') ?>" />
</head>

<body>
  <section id="container">
    <!-- ******************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        ******************************************************************************************************************************************************* -->
    <!--header start-->
    <header class="header black-bg">
      <?php $this->load->view("partials/navbar.php") ?>
    </header>
    <!--header end-->
    <!-- ******************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        ******************************************************************************************************************************************************* -->
    <!--sidebar start-->
    <aside>
      <?php $this->load->view("partials/sidebar.php") ?>
    </aside>
    <!--sidebar end-->
    <!-- ******************************************************************************************************************************************************
        MAIN CONTENT
        ******************************************************************************************************************************************************* -->
    <!--main content start-->
    <section id="main-content">
      <div class="site-container">
        <div class="site-content">
          <section class="wrapper">
              <div class="table-responsive container col-sm-12" style="margin-top:20px">
                <h1>Data <small>Rumah Dinas</small>
                  <div class="pull-right"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_add_new"> Tambah Data</a></div>
                </h1><br>
                <div class="alert alert-warning fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="icon-remove"></i>
                  </button>
                  <strong>INFORMASI</strong><br> Pencarian data berdasarkan <b>alamat</b> dan <b>kerusakan</b>.
                </div>
                <table class="table table-bordered table-striped" id="rumdin-table" width="100%" cellspacing="0">
                  <thead>
                    <tr class="info">
                    <th style="text-align:center">No</th>
                      <th style="text-align:center">Nama Penghuni</th>
                      <th style="text-align:center">Alamat</th>
                      <th style="text-align:center">Kerusakan</th>
                      <th style="text-align:center">Keterangan</th>
                      <th style="text-align:center">Diajukan Oleh</th>
                      <th style="text-align:center">Status</th>
                      <th style="text-align:center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
          </section>

        </div>
        <button type="button" class="hidden" id="refresh"></button>
        <!--footer start-->
        <footer class="site-footer">
          <?php $this->load->view("partials/footer.php") ?>
        </footer>
        <!--footer end-->
      </div>
    </section>
    <!--main content end-->
  </section>

  <!-- ============ MODAL ADD BARANG =============== -->
  <div class="modal fade" id="modal_add_new" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Tambah Data Rumah Dinas</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'Rumdin/simpan' ?>">
          <div class="modal-body">

          <div class="form-group">
            <label class="control-label col-xs-3">Alamat</label>
              <div class="col-xs-8">
                <select class="form-control" id="alamat" style="width: 100%" name="alamat" required>
                  <option value="">Pilih Alamat</option>
                  <?php
                      foreach ($alamat as $m) {
                          echo '<option value="' . $m->id . '">';
                          echo $m->alamat;
                          echo '</option>';
                      }
                  ?>
                </select>
              </div>
            </div>

            <!-- <div class="form-group">
              <label class="control-label col-xs-3">Alamat</label>
              <div class="col-xs-8">
                <input name="alamat" class="form-control" type="text" placeholder="Alamat..." required>
              </div>
            </div> -->

            <!-- <div class="form-group">
              <label class="control-label col-xs-3">Kerusakan</label>
              <div class="col-xs-8">
                <input name="kerusakan" class="form-control" type="text" placeholder="Kerusakan" required>
              </div>
            </div> -->

            <div class="form-group">
            <label class="control-label col-xs-3">Kerusakan</label>
              <div class="col-xs-8">
                <select class="form-control" id="kerusakan" style="width: 100%" name="kerusakan" required>
                  <option value="">Pilih Kerusakan</option>
                  <?php
                      foreach ($kerusakan as $k) {
                          echo '<option value="' . $k->id . '">';
                          echo $k->kerusakan;
                          echo '</option>';
                      }
                  ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="keterangan" name="keterangan" placeholder="max. 255 karakter"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <!-- ============ MODAL EDIT BARANG =============== -->
  <div class="modal fade" id="modal_rumdin_update_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Update Status Data</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'Rumdin/update_status' ?>">
          <div class="modal-body">

            <input type="text" id="update_rumdin_id" class="form-control hidden" name="update_rumdin_id">

            <div class="form-group">
              <label class="control-label col-xs-3">Status</label>
              <div class="col-xs-8">
              <select class="form-control" name="update_status_rumdin" style="width: 100%" id="update_status_rumdin" required>
                  <option value="">Pilih Status</option>    
                  <option value="approved">Setujui</option>
                  <option value="cancel">Tolak</option>
                </select>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <!-- js placed at the end of the document so the pages load faster -->
  <?php $this->load->view("partials/js.php") ?>
  <?php $this->load->view("partials/modal.php") ?>
  
  <script type="text/javascript">
    $(document).ready(function() {
      $('#alamat').select2({
        placeholder: 'Pilih Alamat'
      });
  
      $('#kerusakan').select2({
        placeholder: 'Pilih Kerusakan'
      });

      var table = $('#rumdin-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "rumdin/data_rumdin",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "nama_penghuni"},
          { "data": "alamat"},
          { "data": "kerusakan"},
          { "data": "keterangan"},
          { "data": "dibuat"},
          { "data": "status"},
          { "data": "action"},
          // { "data": "action"}, 'sortable': false},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#rumdin-table_filter input').unbind();
      $('#rumdin-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0) {
          table.search($(this).val()).draw();
        }
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#rumdin-table').DataTable().ajax.reload();
      });

    });

    function update_rumdin(id) {
      $.ajax({
        type: "get",
        url: 'Rumdin/edit_status/'+id,
      })
      .done(function (response) {
          var result = JSON.parse(response)

          $('#update_rumdin_id').val(result.query.id);
          
          $("#modal_rumdin_update_new").modal('show');
          
      });
    }

    function delete_rumdin(id){
      var url = '<?php echo site_url('Rumdin/delete_rumdin/') ?>'+id;
      // console.log(url);
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }
  </script>

</body>

</html>