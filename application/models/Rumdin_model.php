<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Rumdin_model extends CI_Model
{

  private $_table = "rumah_dinas";

  public function view()
  {
    return $this->db->query('SELECT * from rumah_dinas')->result();
  }

  public function save($data)
  {
    $this->db->insert($this->_table, $data);
  }

  public function update($where, $data, $_table)
  {
    $this->db->where($where);
    $this->db->update($_table, $data);
  }

  public function delete($id)
  {
    return $this->db->delete($this->_table, array("id" => $id));
  }

  public function all_rumdin_count($role)
  {
    $nik  = $this->session->userdata('nik');

    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->get('rumah_dinas');
    }
    else{
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->where('created_by',$nik)
        ->get('rumah_dinas');
    }

    return $query->num_rows();
  }

  public function all_rumdin_data($limit, $start, $col, $dir, $role)
  {
    $nik  = $this->session->userdata('nik');

    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('rumah_dinas.deleted_at',null)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('rd_kerusakan','rd_kerusakan.id = rumah_dinas.kerusakan','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.*, rd_alamat.alamat as jalan, rd_alamat.nama_penghuni, 
        rd_kerusakan.kerusakan, users.name')
        ->get('rumah_dinas');
    }
    else{
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('rumah_dinas.deleted_at',null)
        ->where('rumah_dinas.created_by',$nik)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('rd_kerusakan','rd_kerusakan.id = rumah_dinas.kerusakan','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.*, rd_alamat.alamat as jalan, rd_alamat.nama_penghuni, 
        rd_kerusakan.kerusakan, users.name')
        ->get('rumah_dinas');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_rumdin_count($search, $role)
  {
    $nik  = $this->session->userdata('nik');

    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->like('rd_alamat.alamat', $search)
        ->or_like('rd_kerusakan.kerusakan', $search)
        ->or_like('rd_alamat.nama_penghuni', $search)
        ->where('rumah_dinas.deleted_at',null)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('rd_kerusakan','rd_kerusakan.id = rumah_dinas.kerusakan','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.*, rd_alamat.alamat as jalan, rd_alamat.nama_penghuni, 
        rd_kerusakan.kerusakan, users.name')
        ->get('rumah_dinas');
    }
    else{
      $query = $this
        ->db
        ->like('rd_alamat.alamat', $search)
        ->or_like('rd_kerusakan.kerusakan', $search)
        ->or_like('rd_alamat.nama_penghuni', $search)
        ->where('rumah_dinas.deleted_at',null)
        ->where('rumah_dinas.created_by',$nik)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('rd_kerusakan','rd_kerusakan.id = rumah_dinas.kerusakan','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.*, rd_alamat.alamat as jalan, rd_alamat.nama_penghuni, 
        rd_kerusakan.kerusakan, users.name')
        ->get('rumah_dinas');
    }

    return $query->num_rows();
  }

  public function search_rumdin_data($limit, $start, $col, $dir, $search, $role)
  {
    $nik  = $this->session->userdata('nik');

    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->like('rd_alamat.alamat', $search)
        ->or_like('rd_kerusakan.kerusakan', $search)
        ->or_like('rd_alamat.nama_penghuni', $search)
        ->limit($limit, $start)
        ->where('rumah_dinas.deleted_at',null)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('rd_kerusakan','rd_kerusakan.id = rumah_dinas.kerusakan','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.*, rd_alamat.alamat as jalan, rd_alamat.nama_penghuni, 
        rd_kerusakan.kerusakan, users.name')
        ->order_by($col, $dir)
        ->get('rumah_dinas');
    }
    else{
      $query = $this
        ->db
        ->like('rd_alamat.alamat', $search)
        ->or_like('rd_kerusakan.kerusakan', $search)
        ->or_like('rd_alamat.nama_penghuni', $search)
        ->limit($limit, $start)
        ->where('rumah_dinas.deleted_at',null)
        ->where('rumah_dinas.created_by',$nik)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('rd_kerusakan','rd_kerusakan.id = rumah_dinas.kerusakan','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.*, rd_alamat.alamat as jalan, rd_alamat.nama_penghuni, 
        rd_kerusakan.kerusakan, users.name')
        ->order_by($col, $dir)
        ->get('rumah_dinas');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function all_alamat_count()
  {
    $query = $this
      ->db
      ->where('deleted_at',null)
      ->get('rd_alamat');

    return $query->num_rows();
  }

  public function all_alamat_data($limit, $start, $col, $dir)
  {
    $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('rd_alamat');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_alamat_count($search)
  {
    $query = $this
      ->db
      ->like('alamat', $search)
      ->or_like('nama_penghuni', $search)
      ->where('deleted_at',null)
      ->get('rd_alamat');

    return $query->num_rows();
  }

  public function search_alamat_data($limit, $start, $col, $dir, $search)
  {
    $query = $this
      ->db
      ->like('alamat', $search)
      ->or_like('nama_penghuni', $search)
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('rd_alamat');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }
  public function all_kerusakan_count()
  {
    $query = $this
      ->db
      ->where('deleted_at',null)
      ->get('rd_kerusakan');

    return $query->num_rows();
  }

  public function all_kerusakan_data($limit, $start, $col, $dir)
  {
    $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('rd_kerusakan');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_kerusakan_count($search)
  {
    $query = $this
      ->db
      ->like('kerusakan', $search)
      ->where('deleted_at',null)
      ->get('rd_kerusakan');

    return $query->num_rows();
  }

  public function search_kerusakan_data($limit, $start, $col, $dir, $search)
  {
    $query = $this
      ->db
      ->like('kerusakan', $search)
      ->where('deleted_at',null)
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->get('rd_kerusakan');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function getAlamat()
  {
    return $this->db->query('SELECT * from rd_alamat where deleted_at is null')->result();
  }
  
  public function getKerusakan()
  {
    return $this->db->query('SELECT * from rd_kerusakan where deleted_at is null')->result();
  }

  
  public function all_report_rumah_dinas_count($awal,$akhir)
  {
    if($awal==$akhir){
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->where('date(rumah_dinas.created_at)', $awal)
        ->get('rumah_dinas');
    }
    else{
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->where('rumah_dinas.created_at >=', $awal)
        ->where('rumah_dinas.created_at <=', $akhir)
        ->get('rumah_dinas');
    }

    return $query->num_rows();
  }

  public function all_report_rumah_dinas_data($limit, $start, $col, $dir, $awal, $akhir)
  {
    if($awal==$akhir){
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('rumah_dinas.deleted_at',null)
        ->where('date(rumah_dinas.created_at)', $awal)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('rd_kerusakan','rd_kerusakan.id = rumah_dinas.kerusakan','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.id, rumah_dinas.created_at, rd_alamat.alamat, rd_alamat.nama_penghuni, rd_kerusakan.kerusakan, users.name, rumah_dinas.status')
        ->get('rumah_dinas');
    }
    else{
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('rumah_dinas.deleted_at',null)
        ->where('rumah_dinas.created_at >=', $awal)
        ->where('rumah_dinas.created_at <=', $akhir)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('rd_kerusakan','rd_kerusakan.id = rumah_dinas.kerusakan','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.id, rumah_dinas.created_at, rd_alamat.alamat, rd_alamat.nama_penghuni, rd_kerusakan.kerusakan, users.name, rumah_dinas.status')
        ->get('rumah_dinas');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_report_rumah_dinas_count($search, $awal, $akhir)
  {
    if($awal==$akhir){
      $query = $this
        ->db
        ->like('nama_penghuni', $search)
        ->or_like('name', $search)
        ->or_like('alamat', $search)
        ->where('rumah_dinas.deleted_at',null)
        ->where('date(rumah_dinas.created_at)', $awal)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('rd_kerusakan','rd_kerusakan.id = rumah_dinas.kerusakan','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.id, rumah_dinas.created_at, rd_alamat.alamat, rd_alamat.nama_penghuni, rd_kerusakan.kerusakan, users.name, rumah_dinas.status')
        ->get('rumah_dinas');
    }
    else{
      $query = $this
        ->db
        ->like('nama_penghuni', $search)
        ->or_like('name', $search)
        ->or_like('alamat', $search)
        ->where('rumah_dinas.deleted_at',null)
        ->where('rumah_dinas.created_at >=', $awal)
        ->where('rumah_dinas.created_at <=', $akhir)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('rd_kerusakan','rd_kerusakan.id = rumah_dinas.kerusakan','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.id, rumah_dinas.created_at, rd_alamat.alamat, rd_alamat.nama_penghuni, rd_kerusakan.kerusakan, users.name, rumah_dinas.status')
        ->get('rumah_dinas');
    }

    return $query->num_rows();
  }

  public function search_report_rumah_dinas_data($limit, $start, $col, $dir, $search, $awal, $akhir)
  {
    if($awal==$akhir){
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->like('nama_penghuni', $search)
        ->or_like('name', $search)
        ->or_like('alamat', $search)
        ->where('rumah_dinas.deleted_at',null)
        ->where('date(rumah_dinas.created_at)', $awal)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('rd_kerusakan','rd_kerusakan.id = rumah_dinas.kerusakan','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.id, rumah_dinas.created_at, rd_alamat.alamat, rd_alamat.nama_penghuni, rd_kerusakan.kerusakan, users.name, rumah_dinas.status')
        ->get('rumah_dinas');
    }
    else{
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->like('nama_penghuni', $search)
        ->or_like('name', $search)
        ->or_like('alamat', $search)
        ->where('rumah_dinas.deleted_at',null)
        ->where('rumah_dinas.created_at >=', $awal)
        ->where('rumah_dinas.created_at <=', $akhir)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('rd_kerusakan','rd_kerusakan.id = rumah_dinas.kerusakan','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.id, rumah_dinas.created_at, rd_alamat.alamat, rd_alamat.nama_penghuni, rd_kerusakan.kerusakan, users.name, rumah_dinas.status')
        ->get('rumah_dinas');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function export_report_rumah_dinas($awal, $akhir)
  {    
    if($awal==$akhir){
      $query = $this
        ->db
        ->order_by('rumah_dinas.created_at, rd_alamat.alamat, name')
        ->where('rumah_dinas.deleted_at',null)
        ->where('date(rumah_dinas.created_at)', $awal)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('rd_kerusakan','rd_kerusakan.id = rumah_dinas.kerusakan','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.created_at, rumah_dinas.created_by, rd_alamat.alamat, rd_alamat.nama_penghuni, rd_kerusakan.kerusakan, users.name, rumah_dinas.status')
        ->get('rumah_dinas');
    }
    else{
      $query = $this
        ->db
        ->order_by('rumah_dinas.created_at, rd_alamat.alamat, name')
        ->where('rumah_dinas.deleted_at',null)
        ->where('rumah_dinas.created_at >=', $awal)
        ->where('rumah_dinas.created_at <=', $akhir)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('rd_kerusakan','rd_kerusakan.id = rumah_dinas.kerusakan','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.created_at, rumah_dinas.created_by, rd_alamat.alamat, rd_alamat.nama_penghuni, rd_kerusakan.kerusakan, users.name, rumah_dinas.status')
        ->get('rumah_dinas');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }
}
