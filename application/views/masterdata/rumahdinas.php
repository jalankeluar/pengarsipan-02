<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker.css') ?>" />
</head>

<body>
  <section id="container">
    <!-- ******************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        ******************************************************************************************************************************************************* -->
    <!--header start-->
    <header class="header black-bg">
      <?php $this->load->view("partials/navbar.php") ?>
    </header>
    <!--header end-->
    <!-- ******************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        ******************************************************************************************************************************************************* -->
    <!--sidebar start-->
    <aside>
      <?php $this->load->view("partials/sidebar.php") ?>
    </aside>
    <!--sidebar end-->
    <!-- ******************************************************************************************************************************************************
        MAIN CONTENT
        ******************************************************************************************************************************************************* -->
    <!--main content start-->
    <section id="main-content">
      <div class="site-container">
        <div class="site-content">
          <section class="wrapper">
              <div class="table-responsive container col-sm-12" style="margin-top:20px">
                <h1>Data <small>Rumah Dinas</small>
                  
                </h1><br>
                <div class="col-lg-12 mt">
                  <div class="row content-panel">
                    <div class="panel-heading">
                      <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                          <a data-toggle="tab" href="#alamat" onclick="changeTab('alamat')">Alamat</a>
                        </li>
                        <li>
                          <a data-toggle="tab" href="#kerusakan" onclick="changeTab('kerusakan')">Kerusakan</a>
                        </li>
                      </ul>
                    </div>
                    <input type="text" id="tab-active" class="hidden" value="">

                    <!-- /panel-heading -->
                    <div class="panel-body">
                      <div class="tab-content">
                        <div id="alamat" class="tab-pane active">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="pull-right"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_alamat_add_new"> Tambah Data</a></div>
                              <br><br><br>
                              <div class="alert alert-warning fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                  <i class="icon-remove"></i>
                                </button>
                                <strong>INFORMASI</strong><br> Pencarian data berdasarkan <b>alamat</b> dan <b>nama penghuni</b>.
                              </div>
                              <table class="table table-bordered table-striped" id="alamat-table" width="100%" cellspacing="0">
                                <thead>
                                  <tr class="info">
                                    <th style="text-align:center">No</th>
                                    <th style="text-align:center">Alamat</th>
                                    <th style="text-align:center">Nama Penghuni</th>
                                    <th style="text-align:center">Keterangan</th>
                                    <th style="text-align:center">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tbody>
                              </table>
                              <!-- /detailed -->
                            </div>
                            <!-- /col-md-6 -->
                          </div>
                          <!-- /OVERVIEW -->
                        </div>
                        <!-- /tab-pane -->
                        <div id="kerusakan" class="tab-pane">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="pull-right"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_kerusakan_add_new"> Tambah Data</a></div>
                              <br><br><br>
                              <div class="alert alert-warning fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                  <i class="icon-remove"></i>
                                </button>
                                <strong>INFORMASI</strong><br> Pencarian data berdasarkan <b>kerusakan</b>.
                              </div>
                              <table class="table table-bordered table-striped" id="kerusakan-table" width="100%" cellspacing="0">
                                <thead>
                                  <tr class="info">
                                    <th style="text-align:center">No</th>
                                    <th style="text-align:center">Kerusakan</th>
                                    <th style="text-align:center">Keterangan</th>
                                    <th style="text-align:center">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tbody>
                              </table>
                              <!-- /detailed -->
                            </div>
                            <!-- /col-md-6 -->
                          </div>
                          <!-- /row -->
                        </div>
                        <!-- /tab-pane -->
                      </div>
                      <!-- /tab-content -->
                    </div>
                    <!-- /panel-body -->
                  </div>
                <!-- </div> -->
               
              </div>
          </section>

        </div>
        <button type="button" class="hidden" id="refresh"></button>
        <!--footer start-->
        <footer class="site-footer">
          <?php $this->load->view("partials/footer.php") ?>
        </footer>
        <!--footer end-->
      </div>
    </section>
    <!--main content end-->
  </section>

  <!-- ============ MODAL ADD BARANG =============== -->
  <div class="modal fade" id="modal_alamat_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Tambah Data Alamat</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/simpan_alamat' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">Alamat</label>
              <div class="col-xs-8">
                <input name="alamat" id="alamat" class="form-control" type="text" placeholder="Alamat" required>
              </div>
            </div>
            
            <div class="form-group">
              <label class="control-label col-xs-3">Nama Penghuni</label>
              <div class="col-xs-8">
                <input name="nama_penghuni" id="nama_penghuni" class="form-control" type="text" placeholder="Nama Penghuni" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="alamat_keterangan" name="alamat_keterangan" placeholder="max. 255 karakter"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <!-- ============ MODAL ADD BARANG =============== -->
  <div class="modal fade" id="modal_kerusakan_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Tambah Data Kerusakan</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/simpan_kerusakan' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">Kerusakan</label>
              <div class="col-xs-8">
                <input name="kerusakan" id="kerusakan" class="form-control" type="text" placeholder="Kerusakan" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="kerusakan_keterangan" name="kerusakan_keterangan" placeholder="max. 255 karakter"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <!-- ============ MODAL EDIT BARANG =============== -->
  <div class="modal fade" id="modal_alamat_edit_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Edit Data Alamat</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/update_alamat' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">Alamat</label>
              <div class="col-xs-8">
                <input name="edit_alamat" id="edit_alamat" class="form-control" type="text" placeholder="Alamat" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Nama Penghuni</label>
              <div class="col-xs-8">
                <input name="edit_nama_penghuni" id="edit_nama_penghuni" class="form-control" type="text" placeholder="Nama Penghuni" required>
              </div>
            </div>

            <input type="text" id="edit_alamat_id" class="form-control hidden" name="edit_alamat_id">

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="edit_alamat_keterangan" name="edit_alamat_keterangan" placeholder="max. 255 karakter"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->
  
  <!-- ============ MODAL EDIT BARANG =============== -->
  <div class="modal fade" id="modal_kerusakan_edit_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Edit Data Kerusakan</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/update_kerusakan' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">Kerusakan</label>
              <div class="col-xs-8">
                <input name="edit_kerusakan" id="edit_kerusakan" class="form-control" type="text" placeholder="Kerusakan" required>
              </div>
            </div>

            <input type="text" id="edit_kerusakan_id" class="form-control hidden" name="edit_kerusakan_id">

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="edit_kerusakan_keterangan" name="edit_kerusakan_keterangan" placeholder="max. 255 karakter"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <?php $this->load->view("partials/modal.php") ?>

  <!-- js placed at the end of the document so the pages load faster -->
  <?php $this->load->view("partials/js.php") ?>

<!-- date -->
<script src="<?php echo base_url('assets/lib/jquery-ui-1.9.2.custom.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js')?>"></script>
  <!-- <script src="<?php echo base_url('assets/lib/advanced-form-components.js')?>"></script> -->
  
  <script type="text/javascript">
    $(document).ready(function() {
      changeTab('alamat');
      changeTab('kerusakan');
    });
    
    // $('#tabs-active').change(function()
    $('#tab-active').on('change',function()
    {
      var active =  $('#tab-active').val();
      console.log(active);
      // console.log((active=='nopolisi'));
      if(active=='alamat'){
        tableAlamat();
      }
      else{
        tableKerusakan();
      }
    });

    function changeTab(kode) {
      // console.log(kode);
      $('#tab-active').val(kode).trigger('change');
    }
    
    function tableAlamat(){
      
      $('#alamat-table').DataTable().destroy();
      $('#alamat-table tbody').empty();
      var table = $('#alamat-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "data_alamat",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "alamat"},
          { "data": "nama_penghuni"},
          { "data": "keterangan", 'sortable': false},
          { "data": "action", 'sortable': false},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#alamat-table_filter input').unbind();
      
			var dtable = $('#alamat-table').dataTable().api();
      $('#alamat-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
						// Call the API search function
						dtable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtable.search("").draw();
					}
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#alamat-table').DataTable().ajax.reload();
      });
    }

    function tableKerusakan(){
      $('#kerusakan-table').DataTable().destroy();
      $('#kerusakan-table tbody').empty();
      var table = $('#kerusakan-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "data_kerusakan",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "kerusakan"},
          { "data": "keterangan", 'sortable': false},
          { "data": "action", 'sortable': false},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#kerusakan-table_filter input').unbind();
      $('#kerusakan-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0) {
          table.search($(this).val()).draw();
        }
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#kerusakan-table').DataTable().ajax.reload();
      });
    }

    function edit_alamat(id) {
      $.ajax({
        type: "get",
        url: 'edit_alamat/'+id,
      })
      .done(function (response) {
          var result = JSON.parse(response)
          
          $('#edit_alamat_id').val(result.query.id);
          $('#edit_alamat').val(result.query.alamat);
          $('#edit_nama_penghuni').val(result.query.nama_penghuni);
          $('#edit_alamat_keterangan').val(result.query.keterangan);
          
          $("#modal_alamat_edit_new").modal('show');
          
      });
    }

    function edit_kerusakan(id) {
      $.ajax({
        type: "get",
        url: 'edit_kerusakan/'+id,
      })
      .done(function (response) {
          var result = JSON.parse(response)


          $('#edit_kerusakan_id').val(result.query.id);
          $('#edit_kerusakan').val(result.query.kerusakan);
          $('#edit_kerusakan_keterangan').val(result.query.keterangan);
          
          $("#modal_kerusakan_edit_new").modal('show');
          
      });
    }

    // <a onclick="deleteConfirm('<?php //echo site_url('Admin/deleteSub/'.$s->id_sub_dept) ?>')"
    // href="#!" class="btn btn-danger btn-sm" type="submit" name="btn-delete">Hapus</a>


    function delete_alamat(id){
      var url = '<?php echo site_url('masterdata/delete_alamat/') ?>'+id;
      // console.log(url);
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }

    function delete_kerusakan(id){
      var url = '<?php echo site_url('masterdata/delete_kerusakan/') ?>'+id;
      // console.log(url);
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }
  </script>

</body>

</html>