<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view("partials/head.php") ?>
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/zabuto_calendar.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/gritter/css/jquery.gritter.css')?>" />
    <!-- Custom styles for this template -->
    <script src="<?php echo base_url('assets/lib/chart-master/Chart.js')?>"></script>
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
        <?php $this->load->view("partials/navbar.php") ?>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
        <?php $this->load->view("partials/sidebar.php") ?>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <div class="site-container">
        <div class="site-content">
          <section class="wrapper">
            <div class="row">
              <div class="col-lg-9 main-chart">
                <!--CUSTOM CHART START -->
                <div class="border-head">
                  <h3>WELCOME TO JOYO KARTO</h3>
                </div>
              </div>
            </div>
            <!-- /row -->
          </section>
        </div>
      </div>
      <!--footer start-->
      <footer class="site-footer">
          <?php $this->load->view("partials/footer.php") ?>
      </footer>
      <!--footer end-->
    </section>
    <!--main content end-->
  </section>

  <!-- js placed at the end of the document so the pages load faster -->
  <?php $this->load->view("partials/js.php") ?>
</body>

</html>
