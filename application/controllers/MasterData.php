<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MasterData extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Kendaraan_model');
		$this->load->model('Gedung_model');
		$this->load->model('Rumdin_model');
		$this->load->model('Mesin_model');
		$this->load->model('Login_model');
	}

	public function kendaraan()
	{
		// $data['kendaraan'] = $this->Kendaraan_model->view();
		$check_session = $this->session->userdata('nik');
		
		// var_dump($check_session);
		if ($check_session != null) {
			$this->load->view('masterdata/kendaraan');
		} else {

			redirect('login');
		}
	}
	
	public function data_nopolisi()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'no_polisi'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$totalData = $this->Kendaraan_model->all_nopolisi_count();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Kendaraan_model->all_nopolisi_data($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Kendaraan_model->search_nopolisi_data($limit, $start, $order, $dir, $search);

			$totalFiltered = $this->Kendaraan_model->search_nopolisi_count($search);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp       = '"'.$value->id.'"';
				$nestedData['no_polisi'] = ucwords($value->no_polisi);
				$nestedData['type']      = ucwords($value->type);
				$nestedData['tahun']     = ucwords($value->tahun);
				$nestedData['pajak']     = ucwords(date('d-m-Y',strtotime($value->pajak)));
				$nestedData['stnk']     = ucwords(date('d-m-Y',strtotime($value->stnk)));
				$nestedData['action']    = "<center>
				<a onclick='return edit_nopolisi($_temp)' class='btn btn-xs btn-warning'><i class='fa fa-pencil'></i> Edit</a>
				<a onclick='return delete_nopolisi($_temp)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function data_service()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'servis'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$totalData = $this->Kendaraan_model->all_servis_count();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Kendaraan_model->all_servis_data($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Kendaraan_model->search_servis_data($limit, $start, $order, $dir, $search);

			$totalFiltered = $this->Kendaraan_model->search_servis_count($search);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp        = '"'.$value->id.'"';
				$nestedData['servis']     = ucwords($value->servis);
				$nestedData['keterangan'] = ucwords($value->keterangan);
				$nestedData['action']     = "<center>
				<a onclick='return edit_service($_temp)' href='javascript:void(0)' class='btn btn-xs btn-warning'><i class='fa fa-pencil'></i> Edit</a>
				<a onclick='return delete_service($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function simpan_nopolisi()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {
			$nik = $this->session->userdata('nik');
			$now = date('Y-m-d H:i:s');

			$data = array(
				'id'         => $this->uuid->v4(),
				'no_polisi'  => $this->input->post('no_polisi'),
				'type'       => $this->input->post('nopolisi_type'),
				'tahun'      => $this->input->post('nopolisi_tahun'),
				'pajak'      => date('Y-m-d',strtotime($this->input->post('nopolisi_pajak'))),
				'stnk'      => date('Y-m-d',strtotime($this->input->post('nopolisi_stnk'))),
				'created_by' => $nik,
				'created_at' => $now,
			);

			$this->db->insert('k_nopolisi', $data);
			redirect("masterdata/kendaraan");
		} else {

			redirect('login');
		}
	}

	public function simpan_service()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {
			$nik = $this->session->userdata('nik');
			$now = date('Y-m-d H:i:s');

			$data = array(
				'id'         => $this->uuid->v4(),
				'servis'     => $this->input->post('service'),
				'keterangan' => $this->input->post('service_keterangan'),
				'created_by' => $nik,
				'created_at' => $now,
			);

			$this->db->insert('k_service', $data);
			redirect("masterdata/kendaraan");
		} else {

			redirect('login');
		}
	}

	public function edit_nopolisi($id)
	{
		$query = $this->db->query("SELECT * FROM k_nopolisi where id = '$id'")->row();

		// return response()->json($query,200);
		$data = array(
			'query'     => $query,
			'tgl_pajak' => date('d-m-Y',strtotime($query->pajak)),
			'tgl_stnk' => date('d-m-Y',strtotime($query->stnk)),
		);
		echo json_encode($data);
		// return $query;
	}

	public function edit_service($id)
	{
		$query = $this->db->query("SELECT * FROM k_service where id = '$id'")->row();

		// return response()->json($query,200);
		$data = array(
			'query' => $query,
		);
		echo json_encode($data);
		// return $query;
	}

	public function update_nopolisi()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {
			$id   = $this->input->post('edit_nopolisi_id');
			$data = array(
				'no_polisi' => $this->input->post('edit_nopolisi'),
				'type'      => $this->input->post('edit_nopolisi_type'),
				'tahun'     => $this->input->post('edit_nopolisi_tahun'),
				'pajak'     => date('Y-m-d',strtotime($this->input->post('edit_nopolisi_pajak'))),
				'stnk'     => date('Y-m-d',strtotime($this->input->post('edit_nopolisi_stnk'))),
			);
					
			$this->db->where('id', $id);
			$this->db->update('k_nopolisi', $data);
			redirect("masterdata/kendaraan");
		} else {

			redirect('login');
		}
		
	}

	public function update_service()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {		
			$id   = $this->input->post('edit_service_id');
			$data = array(
				'servis'    => $this->input->post('edit_service'),
				'keterangan' => $this->input->post('edit_service_keterangan'),
			);

					
			$this->db->where('id', $id);
			$this->db->update('k_service', $data);
			redirect("masterdata/kendaraan");
		} else {

			redirect('login');
		}
		
	}

	public function delete_nopolisi($id=null)
    {   
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {		
			
			if (!isset($id)) show_404();
			else{				
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('k_nopolisi', $data);
		
				// $this->db->delete('k_nopolisi',$data);
				
				redirect("masterdata/kendaraan");
			}
		} else {

			redirect('login');
		}
	}
	
	public function delete_service($id=null)
    {   
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {		
			
			if (!isset($id)) show_404();
			else{
				
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('k_service', $data);
				
				redirect("masterdata/kendaraan");
			}
		} else {

			redirect('login');
		}
		
	}
	
	// ===========================================rumah dinas===========================================

	public function rumahdinas()
	{
		// $data['kendaraan'] = $this->Kendaraan_model->view();

		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {		
			$this->load->view('masterdata/rumahdinas');
		} else {
			redirect('login');
		}
	}
	
	public function data_alamat()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'alamat'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$totalData = $this->Rumdin_model->all_alamat_count();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Rumdin_model->all_alamat_data($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Rumdin_model->search_alamat_data($limit, $start, $order, $dir, $search);

			$totalFiltered = $this->Rumdin_model->search_alamat_count($search);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp           = '"'.$value->id.'"';
				$nestedData['nama_penghuni'] = ucwords($value->nama_penghuni);
				$nestedData['alamat']        = ucwords($value->alamat);
				$nestedData['keterangan']    = ucwords($value->keterangan);
				$nestedData['action']        = "<center>
				<a onclick='return edit_alamat($_temp)' class='btn btn-xs btn-warning'><i class='fa fa-pencil'></i> Edit</a>
				<a onclick='return delete_alamat($_temp)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function data_kerusakan()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'kerusakan'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$totalData = $this->Rumdin_model->all_kerusakan_count();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Rumdin_model->all_kerusakan_data($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Rumdin_model->search_kerusakan_data($limit, $start, $order, $dir, $search);

			$totalFiltered = $this->Rumdin_model->search_kerusakan_count($search);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp        = '"'.$value->id.'"';
				$nestedData['kerusakan']  = ucwords($value->kerusakan);
				$nestedData['keterangan'] = ucwords($value->keterangan);
				$nestedData['action']     = "<center>
				<a onclick='return edit_kerusakan($_temp)' href='javascript:void(0)' class='btn btn-xs btn-warning'><i class='fa fa-pencil'></i> Edit</a>
				<a onclick='return delete_kerusakan($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function simpan_alamat()
	{
		
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {		
			
			$nik = $this->session->userdata('nik');
			$now = date('Y-m-d H:i:s');

			$data = array(
				'id'            => $this->uuid->v4(),
				'alamat'        => $this->input->post('alamat'),
				'nama_penghuni' => $this->input->post('nama_penghuni'),
				'keterangan'    => $this->input->post('alamat_keterangan'),
				'created_by'    => $nik,
				'created_at'    => $now,
			);

			$this->db->insert('rd_alamat', $data);
			redirect("masterdata/rumahdinas");
		} else {
			redirect('login');
		}
		
	}

	public function simpan_kerusakan()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {		
			$nik = $this->session->userdata('nik');
			$now = date('Y-m-d H:i:s');

			$data = array(
				'id'         => $this->uuid->v4(),
				'kerusakan'  => $this->input->post('kerusakan'),
				'keterangan' => $this->input->post('kerusakan_keterangan'),
				'created_by' => $nik,
				'created_at' => $now,
			);

			$this->db->insert('rd_kerusakan', $data);
			redirect("masterdata/rumahdinas");
		} else {
			redirect('login');
		}
		
	}

	public function edit_alamat($id)
	{
		$query = $this->db->query("SELECT * FROM rd_alamat where id = '$id'")->row();

		// return response()->json($query,200);
		$data = array(
			'query' => $query,
		);
		echo json_encode($data);
		// return $query;
	}

	public function edit_kerusakan($id)
	{
		$query = $this->db->query("SELECT * FROM rd_kerusakan where id = '$id'")->row();

		// return response()->json($query,200);
		$data = array(
			'query' => $query,
		);
		echo json_encode($data);
		// return $query;
	}

	public function update_alamat()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {		
			$id   = $this->input->post('edit_alamat_id');
			$data = array(
				'alamat'        => $this->input->post('edit_alamat'),
				'nama_penghuni' => $this->input->post('edit_nama_penghuni'),
				'keterangan'    => $this->input->post('edit_alamat_keterangan'),
			);

			// var_dump($this->input->post());
			// var_dump($data);
			// die();

					
			$this->db->where('id', $id);
			$this->db->update('rd_alamat', $data);
			redirect("masterdata/rumahdinas");
		} else {
			redirect('login');
		}
		
	}

	public function update_kerusakan()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {			
			$id   = $this->input->post('edit_kerusakan_id');
			$data = array(
				'kerusakan'  => $this->input->post('edit_kerusakan'),
				'keterangan' => $this->input->post('edit_kerusakan_keterangan'),
			);

					
			$this->db->where('id', $id);
			$this->db->update('rd_kerusakan', $data);
			redirect("masterdata/rumahdinas");
		} else {
			redirect('login');
		}
		
	}

	public function delete_alamat($id=null)
    {   
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {			
			if (!isset($id)) show_404();
			else{
				
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('rd_alamat', $data);
				
				redirect("masterdata/rumahdinas");
			}
		} else {
			redirect('login');
		}
		
	}
	
	public function delete_kerusakan($id=null)
    {   
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {			
			
			if (!isset($id)) show_404();
			else{
				
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('rd_kerusakan', $data);
				redirect("masterdata/rumahdinas");
			}
		} else {
			redirect('login');
		}
		
	}
	
	// =========================================== gedung ===========================================

	public function gedung()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {			
			$this->load->view('masterdata/gedung');
		} else {
			redirect('login');
		}
	}
	
	public function data_lantai()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'lantai'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$totalData = $this->Gedung_model->all_lantai_count();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Gedung_model->all_lantai_data($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Gedung_model->search_lantai_data($limit, $start, $order, $dir, $search);

			$totalFiltered = $this->Gedung_model->search_lantai_count($search);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp        = '"'.$value->id.'"';
				$nestedData['lantai']     = ucwords($value->lantai);
				$nestedData['keterangan'] = ucwords($value->keterangan);
				$nestedData['action']     = "<center>
				<a onclick='return edit_lantai($_temp)' class='btn btn-xs btn-warning'><i class='fa fa-pencil'></i> Edit</a>
				<a onclick='return delete_lantai($_temp)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function data_kerusakan_gedung()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'kerusakan_gedung'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$totalData = $this->Gedung_model->all_kerusakan_gedung_count();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Gedung_model->all_kerusakan_gedung_data($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Gedung_model->search_kerusakan_gedung_data($limit, $start, $order, $dir, $search);

			$totalFiltered = $this->Gedung_model->search_kerusakan_gedung_count($search);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp              = '"'.$value->id.'"';
				$nestedData['kerusakan_gedung'] = ucwords($value->kerusakan_gedung);
				$nestedData['keterangan']       = ucwords($value->keterangan);
				$nestedData['action']           = "<center>
				<a onclick='return edit_kerusakan_gedung($_temp)' href='javascript:void(0)' class='btn btn-xs btn-warning'><i class='fa fa-pencil'></i> Edit</a>
				<a onclick='return delete_kerusakan_gedung($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function simpan_lantai()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {				
			$nik = $this->session->userdata('nik');
			$now = date('Y-m-d H:i:s');

			$data = array(
				'id'         => $this->uuid->v4(),
				'lantai'     => $this->input->post('lantai'),
				'keterangan' => $this->input->post('lantai_keterangan'),
				'created_by' => $nik,
				'created_at' => $now,
			);

			$this->db->insert('g_lantai', $data);
			redirect("masterdata/gedung");
		} else {
			redirect('login');
		}
		
	}

	public function simpan_kerusakan_gedung()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {				
			$nik = $this->session->userdata('nik');
			$now = date('Y-m-d H:i:s');

			$data = array(
				'id'               => $this->uuid->v4(),
				'kerusakan_gedung' => $this->input->post('kerusakan_gedung'),
				'keterangan'       => $this->input->post('kerusakan_gedung_keterangan'),
				'created_by'       => $nik,
				'created_at'       => $now,
			);

			$this->db->insert('g_kerusakan', $data);
			redirect("masterdata/gedung");
		} else {
			redirect('login');
		}
		
	}

	public function edit_lantai($id)
	{
		$query = $this->db->query("SELECT * FROM g_lantai where id = '$id'")->row();

		// return response()->json($query,200);
		$data = array(
			'query' => $query,
		);
		echo json_encode($data);
		// return $query;
	}

	public function edit_kerusakan_gedung($id)
	{
		$query = $this->db->query("SELECT * FROM g_kerusakan where id = '$id'")->row();

		// return response()->json($query,200);
		$data = array(
			'query' => $query,
		);
		echo json_encode($data);
		// return $query;
	}

	public function update_lantai()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {				
			
			$id   = $this->input->post('edit_lantai_id');
			$data = array(
				'lantai'     => $this->input->post('edit_lantai'),
				'keterangan' => $this->input->post('edit_lantai_keterangan'),
			);

			$this->db->where('id', $id);
			$this->db->update('g_lantai', $data);
			redirect("masterdata/gedung");
		} else {
			redirect('login');
		}
		
	}

	public function update_kerusakan_gedung()
	{
		$check_session = $this->session->userdata('nik');
		
		// var_dump($this->input->post());
		// die();
		if ($check_session != null) {				
		
			$id   = $this->input->post('edit_kerusakan_gedung_id');
			$data = array(
				'kerusakan_gedung' => $this->input->post('edit_kerusakan_gedung'),
				'keterangan'       => $this->input->post('edit_kerusakan_gedung_keterangan'),
			);
	
					
			$this->db->where('id', $id);
			$this->db->update('g_kerusakan', $data);
			redirect("masterdata/gedung");
		} else {
			redirect('login');
		}
		
	}

	public function delete_lantai($id=null)
    {   
		
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {					
			if (!isset($id)) show_404();
			else{
				
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('g_lantai', $data);
				
				redirect("masterdata/gedung");
			}
		} else {
			redirect('login');
		}
	}
	
	public function delete_kerusakan_gedung($id=null)
    {   
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {					
			if (!isset($id)) show_404();
			else{
				
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('g_kerusakan', $data);
				redirect("masterdata/gedung");
			}
		} else {
			redirect('login');
		}
	}
	
	
	// =========================================== gedung ===========================================

	public function mesin()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {			
			$this->load->view('masterdata/mesin');
		} else {
			redirect('login');
		}
	}
	
	public function data_jenis_mesin()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'jenis_mesin'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$totalData = $this->Mesin_model->all_jenis_mesin_count();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Mesin_model->all_jenis_mesin_data($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Mesin_model->search_jenis_mesin_data($limit, $start, $order, $dir, $search);

			$totalFiltered = $this->Mesin_model->search_jenis_mesin_count($search);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp         = '"'.$value->id.'"';
				$nestedData['jenis_mesin'] = ucwords($value->jenis_mesin);
				$nestedData['no_aset']     = ucwords($value->no_aset);
				$nestedData['keterangan']  = ucwords($value->keterangan);
				$nestedData['action']      = "<center>
				<a onclick='return edit_jenis_mesin($_temp)' class='btn btn-xs btn-warning'><i class='fa fa-pencil'></i> Edit</a>
				<a onclick='return delete_jenis_mesin($_temp)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function data_kerusakan_mesin()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'kerusakan_mesin'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$totalData = $this->Mesin_model->all_kerusakan_mesin_count();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Mesin_model->all_kerusakan_mesin_data($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Mesin_model->search_kerusakan_mesin_data($limit, $start, $order, $dir, $search);

			$totalFiltered = $this->Mesin_model->search_kerusakan_mesin_count($search);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp              = '"'.$value->id.'"';
				$nestedData['kerusakan_mesin'] = ucwords($value->kerusakan_mesin);
				$nestedData['keterangan']      = ucwords($value->keterangan);
				$nestedData['action']          = "<center>
				<a onclick='return edit_kerusakan_mesin($_temp)' href='javascript:void(0)' class='btn btn-xs btn-warning'><i class='fa fa-pencil'></i> Edit</a>
				<a onclick='return delete_kerusakan_mesin($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function simpan_jenis_mesin()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {				
			$nik = $this->session->userdata('nik');
			$now = date('Y-m-d H:i:s');

			$data = array(
				'id'          => $this->uuid->v4(),
				'no_aset'     => $this->input->post('no_aset'),
				'jenis_mesin' => $this->input->post('jenis_mesin'),
				'keterangan'  => $this->input->post('jenis_mesin_keterangan'),
				'created_by'  => $nik,
				'created_at'  => $now,
			);

			$this->db->insert('m_jenis_mesin', $data);
			redirect("masterdata/mesin");
		} else {
			redirect('login');
		}
		
	}

	public function simpan_kerusakan_mesin()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {				
			$nik = $this->session->userdata('nik');
			$now = date('Y-m-d H:i:s');

			$data = array(
				'id'               => $this->uuid->v4(),
				'kerusakan_mesin' => $this->input->post('kerusakan_mesin'),
				'keterangan'       => $this->input->post('kerusakan_mesin_keterangan'),
				'created_by'       => $nik,
				'created_at'       => $now,
			);

			$this->db->insert('m_kerusakan', $data);
			redirect("masterdata/mesin");
		} else {
			redirect('login');
		}
		
	}

	public function edit_jenis_mesin($id)
	{
		$query = $this->db->query("SELECT * FROM m_jenis_mesin where id = '$id'")->row();

		// return response()->json($query,200);
		$data = array(
			'query' => $query,
		);
		echo json_encode($data);
		// return $query;
	}

	public function edit_kerusakan_mesin($id)
	{
		$query = $this->db->query("SELECT * FROM m_kerusakan where id = '$id'")->row();

		// return response()->json($query,200);
		$data = array(
			'query' => $query,
		);
		echo json_encode($data);
		// return $query;
	}

	public function update_jenis_mesin()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {				
			
			$id   = $this->input->post('edit_jenis_mesin_id');
			$data = array(
				'no_aset'     => $this->input->post('edit_no_aset'),
				'jenis_mesin' => $this->input->post('edit_jenis_mesin'),
				'keterangan'  => $this->input->post('edit_jenis_mesin_keterangan'),
			);

			$this->db->where('id', $id);
			$this->db->update('m_jenis_mesin', $data);
			redirect("masterdata/mesin");
		} else {
			redirect('login');
		}
		
	}

	public function update_kerusakan_mesin()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {				
		
			$id   = $this->input->post('edit_kerusakan_mesin_id');
			$data = array(
				'kerusakan_mesin' => $this->input->post('edit_kerusakan_mesin'),
				'keterangan'       => $this->input->post('edit_kerusakan_mesin_keterangan'),
			);
	
					
			$this->db->where('id', $id);
			$this->db->update('m_kerusakan', $data);
			redirect("masterdata/mesin");
		} else {
			redirect('login');
		}
		
	}

	public function delete_jenis_mesin($id=null)
    {   
		
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {					
			if (!isset($id)) show_404();
			else{
				
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('m_jenis_mesin', $data);
				
				redirect("masterdata/mesin");
			}
		} else {
			redirect('login');
		}
	}
	
	public function delete_kerusakan_mesin($id=null)
    {   
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {					
			if (!isset($id)) show_404();
			else{
				
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('m_kerusakan', $data);
				redirect("masterdata/mesin");
			}
		} else {
			redirect('login');
		}
	}

	// =========================================== SPBU ===========================================

	public function spbu()
	{
		// $data['kendaraan'] = $this->Kendaraan_model->view();
		$check_session = $this->session->userdata('nik');
		
		// var_dump($check_session);
		if ($check_session != null) {
			$this->load->view('masterdata/spbu');
		} else {

			redirect('login');
		}
	}
	
	public function data_spbu()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'spbu'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$totalData = $this->Kendaraan_model->all_spbu_count();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Kendaraan_model->all_spbu_data($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Kendaraan_model->search_spbu_data($limit, $start, $order, $dir, $search);

			$totalFiltered = $this->Kendaraan_model->search_spbu_count($search);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp       = '"'.$value->id.'"';
				$nestedData['spbu'] = ucwords($value->spbu);
				$nestedData['alamat']      = ucwords($value->alamat);
				$nestedData['keterangan']     = ucwords($value->keterangan);
				$nestedData['action']    = "<center>
				<a onclick='return edit_spbu($_temp)' class='btn btn-xs btn-warning'><i class='fa fa-pencil'></i> Edit</a>
				<a onclick='return delete_spbu($_temp)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function simpan_spbu()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {
			$nik = $this->session->userdata('nik');
			$now = date('Y-m-d H:i:s');

			$data = array(
				'id'         => $this->uuid->v4(),
				'spbu'  => $this->input->post('spbu'),
				'alamat'       => $this->input->post('spbu_alamat'),
				'keterangan'      => $this->input->post('spbu_keterangan'),
				'created_by' => $nik,
				'created_at' => $now,
			);

			$this->db->insert('spbu', $data);
			redirect("masterdata/spbu");
		} else {

			redirect('login');
		}
	}

	public function edit_spbu($id)
	{
		$query = $this->db->query("SELECT * FROM spbu where id = '$id'")->row();

		// return response()->json($query,200);
		$data = array(
			'query'     => $query,
		);
		echo json_encode($data);
		// return $query;
	}

	public function update_spbu()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {
			$id   = $this->input->post('edit_spbu_id');
			$data = array(
				'spbu' => $this->input->post('edit_spbu'),
				'alamat'      => $this->input->post('edit_spbu_alamat'),
				'keterangan'     => $this->input->post('edit_spbu_keterangan'),
			);
					
			$this->db->where('id', $id);
			$this->db->update('spbu', $data);
			redirect("masterdata/spbu");
		} else {

			redirect('login');
		}
		
	}

	public function delete_spbu($id=null)
    {   
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {		
			
			if (!isset($id)) show_404();
			else{				
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('spbu', $data);
		
				// $this->db->delete('k_nopolisi',$data);
				
				redirect("masterdata/spbu");
			}
		} else {

			redirect('login');
		}
	}
	
	// =========================================== BENGKEL ===========================================

	public function bengkel()
	{
		// $data['kendaraan'] = $this->Kendaraan_model->view();
		$check_session = $this->session->userdata('nik');
		
		// var_dump($check_session);
		if ($check_session != null) {
			$this->load->view('masterdata/bengkel');
		} else {

			redirect('login');
		}
	}
	
	public function data_bengkel()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'bengkel'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$totalData = $this->Kendaraan_model->all_bengkel_count();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Kendaraan_model->all_bengkel_data($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Kendaraan_model->search_bengkel_data($limit, $start, $order, $dir, $search);

			$totalFiltered = $this->Kendaraan_model->search_bengkel_count($search);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp       = '"'.$value->id.'"';
				$nestedData['bengkel'] = ucwords($value->bengkel);
				$nestedData['alamat']      = ucwords($value->alamat);
				$nestedData['keterangan']     = ucwords($value->keterangan);
				$nestedData['action']    = "<center>
				<a onclick='return edit_bengkel($_temp)' class='btn btn-xs btn-warning'><i class='fa fa-pencil'></i> Edit</a>
				<a onclick='return delete_bengkel($_temp)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function simpan_bengkel()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {
			$nik = $this->session->userdata('nik');
			$now = date('Y-m-d H:i:s');

			$data = array(
				'id'         => $this->uuid->v4(),
				'bengkel'  => $this->input->post('bengkel'),
				'alamat'       => $this->input->post('bengkel_alamat'),
				'keterangan'      => $this->input->post('bengkel_keterangan'),
				'created_by' => $nik,
				'created_at' => $now,
			);

			$this->db->insert('bengkel', $data);
			redirect("masterdata/bengkel");
		} else {

			redirect('login');
		}
	}

	public function edit_bengkel($id)
	{
		$query = $this->db->query("SELECT * FROM bengkel where id = '$id'")->row();

		// return response()->json($query,200);
		$data = array(
			'query'     => $query,
		);
		echo json_encode($data);
		// return $query;
	}

	public function update_bengkel()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {
			$id   = $this->input->post('edit_bengkel_id');
			$data = array(
				'bengkel'    => $this->input->post('edit_bengkel'),
				'alamat'     => $this->input->post('edit_bengkel_alamat'),
				'keterangan' => $this->input->post('edit_bengkel_keterangan'),
			);
					
			$this->db->where('id', $id);
			$this->db->update('bengkel', $data);
			redirect("masterdata/bengkel");
		} else {

			redirect('login');
		}
		
	}

	public function delete_bengkel($id=null)
    {   
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {		
			
			if (!isset($id)) show_404();
			else{				
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('bengkel', $data);
		
				// $this->db->delete('k_nopolisi',$data);
				
				redirect("masterdata/bengkel");
			}
		} else {

			redirect('login');
		}
	}

		// =========================================== Vendor ===========================================

		public function vendor()
		{
			// $data['kendaraan'] = $this->Kendaraan_model->view();
			$check_session = $this->session->userdata('nik');
			
			// var_dump($check_session);
			if ($check_session != null) {
				$this->load->view('masterdata/vendor');
			} else {
	
				redirect('login');
			}
		}
		
		public function data_vendor()
		{
			$columns = array(
				0 => 'created_at',
				1 => 'vendor'
			);
	
			$limit = $this->input->post('length');
			$start = $this->input->post('start');
			$order = $columns[$this->input->post('order')[0]['column']];
			$dir   = $this->input->post('order')[0]['dir'];
			$draw  = $this->input->post('draw');
	
			$totalData = $this->Mesin_model->all_vendor_count();
	
			$totalFiltered = $totalData;
	
			if (empty($this->input->post('search')['value'])) {
				$master = $this->Mesin_model->all_vendor_data($limit, $start, $order, $dir);
			} else {
				$search = $this->input->post('search')['value'];
	
				$master =  $this->Mesin_model->search_vendor_data($limit, $start, $order, $dir, $search);
	
				$totalFiltered = $this->Mesin_model->search_vendor_count($search);
			}
	
			$data       = array();
			$nomor_urut = 0;
			if (!empty($master)) {
				foreach ($master as $key => $value) {
					$_temp       = '"'.$value->id.'"';
					$nestedData['vendor'] = ucwords($value->vendor);
					$nestedData['alamat']      = ucwords($value->alamat);
					$nestedData['kategori']     = ucwords($value->kategori);
					$nestedData['keterangan']     = ucwords($value->keterangan);
					$nestedData['action']    = "<center>
					<a onclick='return edit_vendor($_temp)' class='btn btn-xs btn-warning'><i class='fa fa-pencil'></i> Edit</a>
					<a onclick='return delete_vendor($_temp)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";
	
					$data[] = $nestedData;
					$nomor_urut++;
				}
			}
	
			$json_data = array(
				"draw"            => intval($this->input->post('draw')),
				"recordsTotal"    => intval($totalData),
				"recordsFiltered" => intval($totalFiltered),
				"data"            => $data
			);
	
			echo json_encode($json_data);
		}
	
		public function simpan_vendor()
		{
			$check_session = $this->session->userdata('nik');
			
			if ($check_session != null) {
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');

			$list_kategori = $this->input->post('vendor_kategori');
			$kategori = '';
			foreach ($list_kategori as $key => $value) {
			$kategori .= $value.',';
			}
	
				$data = array(
					'id'         => $this->uuid->v4(),
					'vendor'  => $this->input->post('vendor'),
					'alamat'       => $this->input->post('vendor_alamat'),
					'kategori'        => $kategori,
					'keterangan'      => $this->input->post('vendor_keterangan'),
					'created_by' => $nik,
					'created_at' => $now,
				);
	
				$this->db->insert('vendor', $data);
				redirect("masterdata/vendor");
			} else {
	
				redirect('login');
			}
		}
	
		public function edit_vendor($id)
		{
			$query = $this->db->query("SELECT * FROM vendor where id = '$id'")->row();
	
			// return response()->json($query,200);
			$data = array(
				'query'     => $query,
			);
			echo json_encode($data);
			// return $query;
		}
	
		public function update_vendor()
		{
			$check_session = $this->session->userdata('nik');
			
			if ($check_session != null) {
				$id   = $this->input->post('edit_vendor_id');
				$data = array(
					'vendor' => $this->input->post('edit_vendor'),
					'alamat'      => $this->input->post('edit_vendor_alamat'),
					'kategori' => $this->input->post('edit_vendor_kategori'),
					'keterangan'     => $this->input->post('edit_vendor_keterangan'),
				);
						
				$this->db->where('id', $id);
				$this->db->update('vendor', $data);
				redirect("masterdata/vendor");
			} else {
	
				redirect('login');
			}
			
		}
	
		public function delete_vendor($id=null)
		{   
			$check_session = $this->session->userdata('nik');
			
			if ($check_session != null) {		
				
				if (!isset($id)) show_404();
				else{				
					$nik = $this->session->userdata('nik');
					$now = date('Y-m-d H:i:s');
		
					$data = array(
						'deleted_at' => $now,
						'deleted_by' => $nik,
					);
							
					$this->db->where('id', $id);
					$this->db->update('vendor', $data);
			
					// $this->db->delete('k_nopolisi',$data);
					
					redirect("masterdata/vendor");
				}
			} else {
	
				redirect('login');
			}
		}

	// public function data_jenis_pekerjaan()
	// {
	// 	$columns = array(
	// 		0 => 'created_at',
	// 		1 => 'jenis_pekerjaan'
	// 	);

	// 	$limit = $this->input->post('length');
	// 	$start = $this->input->post('start');
	// 	$order = $columns[$this->input->post('order')[0]['column']];
	// 	$dir   = $this->input->post('order')[0]['dir'];
	// 	$draw  = $this->input->post('draw');

	// 	$totalData = $this->Gedung_model->all_jenis_pekerjaan_count();

	// 	$totalFiltered = $totalData;

	// 	if (empty($this->input->post('search')['value'])) {
	// 		$master = $this->Gedung_model->all_jenis_pekerjaan_data($limit, $start, $order, $dir);
	// 	} else {
	// 		$search = $this->input->post('search')['value'];

	// 		$master =  $this->Gedung_model->search_jenis_pekerjaan_data($limit, $start, $order, $dir, $search);

	// 		$totalFiltered = $this->Gedung_model->search_jenis_pekerjaan_count($search);
	// 	}

	// 	$data       = array();
	// 	$nomor_urut = 0;
	// 	if (!empty($master)) {
	// 		foreach ($master as $key => $value) {
	// 			$_temp             = '"'.$value->id.'"';
	// 			$nestedData['jenis_pekerjaan'] = ucwords($value->jenis_pekerjaan);
	// 			$nestedData['keterangan']      = ucwords($value->keterangan);
	// 			$nestedData['action']          = "<center>
	// 			<a onclick='return edit_jenis_pekerjaan($_temp)' href='javascript:void(0)' class='btn btn-xs btn-warning'><i class='fa fa-pencil'></i> Edit</a>
	// 			<a onclick='return delete_jenis_pekerjaan($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

	// 			$data[] = $nestedData;
	// 			$nomor_urut++;
	// 		}
	// 	}

	// 	$json_data = array(
	// 		"draw"            => intval($this->input->post('draw')),
	// 		"recordsTotal"    => intval($totalData),
	// 		"recordsFiltered" => intval($totalFiltered),
	// 		"data"            => $data
	// 	);

	// 	echo json_encode($json_data);
	// }

	// public function simpan_jenis_pekerjaan()
	// {
	// 	$check_session = $this->session->userdata('nik');
		
	// 	if ($check_session != null) {				
	// 		$nik = $this->session->userdata('nik');
	// 		$now = date('Y-m-d H:i:s');

	// 		$data = array(
	// 			'id'              => $this->uuid->v4(),
	// 			'jenis_pekerjaan' => $this->input->post('jenis_pekerjaan'),
	// 			'keterangan'      => $this->input->post('jenis_pekerjaan_keterangan'),
	// 			'created_by'      => $nik,
	// 			'created_at'      => $now,
	// 		);

	// 		$this->db->insert('g_jenis_pekerjaan', $data);
	// 		redirect("masterdata/gedung");
	// 	} else {
	// 		redirect('login');
	// 	}
		
	// }

	// public function edit_jenis_pekerjaan($id)
	// {
	// 	$query = $this->db->query("SELECT * FROM g_jenis_pekerjaan where id = '$id'")->row();

	// 	// return response()->json($query,200);
	// 	$data = array(
	// 		'query' => $query,
	// 	);
	// 	echo json_encode($data);
	// 	// return $query;
	// }

	// public function update_jenis_pekerjaan()
	// {
	// 	$check_session = $this->session->userdata('nik');
		
	// 	if ($check_session != null) {				
		
	// 		$id   = $this->input->post('edit_jenis_pekerjaan_id');
	// 		$data = array(
	// 			'jenis_pekerjaan' => $this->input->post('edit_jenis_pekerjaan'),
	// 			'keterangan'       => $this->input->post('edit_jenis_pekerjaan_keterangan'),
	// 		);
	
					
	// 		$this->db->where('id', $id);
	// 		$this->db->update('g_jenis_pekerjaan', $data);
	// 		redirect("masterdata/gedung");
	// 	} else {
	// 		redirect('login');
	// 	}
	// }
	
	// public function delete_jenis_pekerjaan($id=null)
    // {   
		
	// 	$check_session = $this->session->userdata('nik');
		
	// 	if ($check_session != null) {					
	// 		if (!isset($id)) show_404();
	// 		else{
				
	// 			$nik = $this->session->userdata('nik');
	// 			$now = date('Y-m-d H:i:s');
	
	// 			$data = array(
	// 				'deleted_at' => $now,
	// 				'deleted_by' => $nik,
	// 			);
						
	// 			$this->db->where('id', $id);
	// 			$this->db->update('g_jenis_pekerjaan', $data);
	// 			redirect("masterdata/gedung");
	// 		}
	// 	} else {
	// 		redirect('login');
	// 	}
		
    // }
}
