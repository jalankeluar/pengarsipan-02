<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker.css') ?>" />
</head>

<body>
  <section id="container">
    <!-- ******************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        ******************************************************************************************************************************************************* -->
    <!--header start-->
    <header class="header black-bg">
      <?php $this->load->view("partials/navbar.php") ?>
    </header>
    <!--header end-->
    <!-- ******************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        ******************************************************************************************************************************************************* -->
    <!--sidebar start-->
    <aside>
      <?php $this->load->view("partials/sidebar.php") ?>
    </aside>
    <!--sidebar end-->
    <!-- ******************************************************************************************************************************************************
        MAIN CONTENT
        ******************************************************************************************************************************************************* -->
    <!--main content start-->
    <section id="main-content">
      <div class="site-container">
        <div class="site-content">
          <section class="wrapper">
              <div class="table-responsive container col-sm-12" style="margin-top:20px">
                <h1>Data <small>Gedung</small>
                  <div class="pull-right"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_add_new"> Tambah Data</a></div>
                </h1><br>
                <div class="alert alert-warning fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="icon-remove"></i>
                  </button>
                  <strong>INFORMASI</strong><br> Pencarian data berdasarkan <b>lantai</b> dan <b>kerusakan</b>.
                </div>
                <table class="table table-bordered table-striped" id="gedung-table" width="100%" cellspacing="0">
                  <thead>
                    <tr class="info">
                    <th style="text-align:center">No</th>
                      <th style="text-align:center">Lantai</th>
                      <th style="text-align:center">Kerusakan</th>
                      <!-- <th style="text-align:center">Jenis Pekerjaan</th> -->
                      <th style="text-align:center">keterangan</th>
                      <th style="text-align:center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
          </section>

        </div>
        <button type="button" class="hidden" id="refresh"></button>
        <!--footer start-->
        <footer class="site-footer">
          <?php $this->load->view("partials/footer.php") ?>
        </footer>
        <!--footer end-->
      </div>
    </section>
    <!--main content end-->
  </section>

  <!-- ============ MODAL ADD BARANG =============== -->
  <div class="modal fade" id="modal_add_new" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Tambah Data Gedung</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'Gedung/simpan' ?>">
          <div class="modal-body">

            <!-- <div class="form-group">
              <label class="control-label col-xs-3">Lantai</label>
              <div class="col-xs-8">
                <input name="lantai" class="form-control" type="text" placeholder="Lantai" required>
              </div>
            </div> -->

            <div class="form-group">
            <label class="control-label col-xs-3">Lantai</label>
              <div class="col-xs-8">
                <select class="form-control" style="width: 100%" name="lantai" id="lantai" required>
                  <option value="">Pilih Lantai</option>
                  <?php
                      foreach ($lantai as $l) {
                          echo '<option value="' . $l->id . '">';
                          echo $l->lantai;
                          echo '</option>';
                      }
                  ?>
                </select>
              </div>
            </div>

            <!-- <div class="form-group">
              <label class="control-label col-xs-3">Kerusakan</label>
              <div class="col-xs-8">
                <input name="kerusakan" class="form-control" type="text" placeholder="Kerusakan" required>
              </div>
            </div> -->

            <div class="form-group">
            <label class="control-label col-xs-3">Kerusakan</label>
              <div class="col-xs-8">
                <select class="form-control"  style="width: 100%" name="kerusakan" id="kerusakan" required>
                  <option value="">Pilih Kerusakan</option>
                  <?php
                      foreach ($kerusakan as $kg) {
                          echo '<option value="' . $kg->id . '">';
                          echo $kg->kerusakan_gedung;
                          echo '</option>';
                      }
                  ?>
                </select>
              </div>
            </div>

            <!-- <div class="form-group">
              <label class="control-label col-xs-3">Jenis Pekerjaan</label>
              <div class="col-xs-8">
                <input name="jenis_pekerjaan" class="form-control" type="text" placeholder="Jenis Pekerjaan..." required>
              </div>
            </div> -->

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="keterangan" name="keterangan" placeholder="max. 255 karakter"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <!-- js placed at the end of the document so the pages load faster -->
  <?php $this->load->view("partials/js.php") ?>
  <?php $this->load->view("partials/modal.php") ?>
  
  <script type="text/javascript">
    $(document).ready(function() {
      // $('.select-search').select2();
      $('#kerusakan').select2({
        placeholder: 'Pilih Kerusakan'
      });

      $('#lantai').select2({
        placeholder: 'Pilih Lantai'
      });

      var table = $('#gedung-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "gedung/data_gedung",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "lantai", "className": "text-center"},
          { "data": "kerusakan"},
          // { "data": "jenis_pekerjaan"},
          { "data": "keterangan", 'sortable': false},
          { "data": "action"},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#gedung-table_filter input').unbind();
      $('#gedung-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0) {
          table.search($(this).val()).draw();
        }
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#gedung-table').DataTable().ajax.reload();
      });

    });

    function delete_gedung(id){
      var url = '<?php echo site_url('gedung/delete_gedung/') ?>'+id;
      // console.log(url);
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }
  </script>

</body>

</html>