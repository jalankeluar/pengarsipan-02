<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Kendaraan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Kendaraan_model');
		$this->load->model('Login_model');
	}

	public function service()
	{
		$check_session = $this->session->userdata('nik');
		if ($check_session != null) {
			$data = array(
				'no_polisi'          => $this->Kendaraan_model->getNopol(),
				'service'            => $this->Kendaraan_model->getService(),
				'bengkel'            => $this->Kendaraan_model->getBengkel(),

			);
			$this->load->view('kendaraan/index', $data);
		} 
		else{
			redirect('login');
		}
		
	}

	public function data_kendaraan()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'no_polisi',
			2 => 'servis',
			3 => 'harga',
			4 => 'nama',
			5 => 'pengajuan_date'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$role = $this->session->userdata('role');

		$totalData = $this->Kendaraan_model->all_kendaraan_count($role);

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Kendaraan_model->all_kendaraan_data($limit, $start, $order, $dir, $role);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Kendaraan_model->search_kendaraan_data($limit, $start, $order, $dir, $search, $role);

			$totalFiltered = $this->Kendaraan_model->search_kendaraan_count($search, $role);
		}
		
		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp                  = '"'.$value->id.'"';
				$nestedData['no_polisi']      = ucwords($value->nopol);
				
				$service = explode(',',$value->service);
				$hasil_service = '';
				foreach ($service as $key => $detail) {
					$nama = $this->Kendaraan_model->getService_id($detail);
					if($nama!=null){
						$hasil_service .= ''.$nama->servis.', ';
					}
					// var_dump($nama->servis);
				}
				$hasil_service = rtrim($hasil_service, ", ");

				if($value->harga == null){
					$harga = '-';
				}
				else{
					$harga = 'Rp. '.number_format($value->harga ,0,",",".");
				}
				// $nestedData['service']        = ucwords($value->servis);
				$nestedData['service']        = ucwords($hasil_service);
				$nestedData['pengajuan_date'] = ucwords(date('d-m-Y',strtotime($value->pengajuan_date)));
				$nestedData['harga']          = $harga;
				$nestedData['bengkel']        = $value->nama;
				$nestedData['keterangan']     = ucwords($value->keterangan);

				if($value->status == 'approved'){
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-success'>DISETUJUI</a>
					</center>";
				}
				elseif($value->status == 'cancel'){
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-danger'>DITOLAK</a>
					</center>";
				}
				else{
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-warning'>MENUNGGU</a>
					</center>";
				}

				if($value->status_service == 'done'){
					
					$nestedData['status_service']     = "<center>
					<a class='btn btn-xs btn-success'>SELESAI</a>
					</center>";
				}
				elseif($value->status_service == 'process'){
					$nestedData['status_service']     = "<center>
					<a class='btn btn-xs btn-warning'>PROSES</a>
					</center>";
				}
				else{
					$nestedData['status_service']     = "";
				}
				// $nestedData['status']        = ucwords($value->status);
				
			
				if ($role == 'verifikator' || $role == 'admin' || $role == 'super_admin'){
					
					if($value->status == 'approved'){
						if($value->status_service == 'process'){
							$nestedData['action']     = "<center>
							<a onclick='return update_service_berkas($_temp)' class='btn btn-xs btn-default'><i class='fa fa-pencil'></i> Update Berkas</a>
							<br><a onclick='return download_catatan($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-file'></i> Catatan</a> 
							<br><a onclick='return update_service($_temp)' class='btn btn-xs btn-default'><i class='fa fa-pencil'></i> Update Service</a>
							<br><a onclick='return delete_service($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> 
							</center>";
						}
						else{
							$nestedData['action']     = "<center>
							<a onclick='return download_catatan($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-file'></i> Catatan</a> 
							<br><a onclick='return download_catatan2($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-file'></i> Surat Tugas</a> 
							<br><a onclick='return update_service($_temp)' class='btn btn-xs btn-default'><i class='fa fa-pencil'></i> Update Service</a>
							<br><a onclick='return delete_service($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> 
							</center>";
						}
					}
					else{
						
						// <a onclick='return update_service($_temp)' class='btn btn-xs btn-default'><i class='fa fa-pencil'></i> Update</a>
						$nestedData['action']     = "<center>
						<a onclick='return update_service_berkas($_temp)' class='btn btn-xs btn-default'><i class='fa fa-pencil'></i> Update Berkas</a>
						<br><a onclick='return delete_service($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

					}
				}
				else{
					if($value->status == 'approved'){
						if($value->status_service == 'process'){
							$nestedData['action']     = "<center>
							<a onclick='return download_catatan($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-file'></i> Catatan</a> 
							<br><a onclick='return delete_service($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> 
							</center>";
						}
						else{
							$nestedData['action']     = "<center>
							<a onclick='return download_catatan($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-file'></i> Catatan</a> 
							<br><a onclick='return download_catatan2($_temp)' href='javascript:void(0)' class='btn btn-xs btn-info'><i class='fa fa-file'></i> Surat Tugas</a> 
							<br><a onclick='return delete_service($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> 
							</center>";
						}
						
					}
					else{

						$nestedData['action']     = "<center>
						<a onclick='return delete_service($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

					}

				}
				
				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}


	public function add()
	{
		$check_session = $this->session->userdata('nik');
		if ($check_session != null) {
			$this->load->view("kendaraan/input");
		} 
		else{
			redirect('login');
		}
	}

	public function simpan()
	{
		$check_session = $this->session->userdata('nik');
		if ($check_session != null) {	
			$kendaraan   = $this->Kendaraan_model;
			$nik = $this->session->userdata('nik');
			$now = date('Y-m-d H:i:s');

			// var_dump(print_r($this->input->post('service')));
			$list_service = $this->input->post('service');
			$service = '';
			foreach ($list_service as $key => $value) {
				$service .= $value.',';
			}
			// var_dump($service);
			// die();

			$tgl_pengajuan = date("Y-m-d", strtotime($this->input->post('pengajuan_date')));

			$data = array(
				'id'             => $this->uuid->v4(),
				'no_polisi'      => $this->input->post('no_polisi'),
				// 'service'        => $this->input->post('service'),
				'service'        => $service,
				'pengajuan_date' => $tgl_pengajuan,
				'harga'          => $this->input->post('harga'),
				'keterangan'     => $this->input->post('keterangan'),
				'status'         => 'menunggu',
				'status_service' => '',
				'created_by'     => $nik,
				'created_at'     => $now,
			);

			$kendaraan->save($data);
			redirect("Kendaraan/service");
		} 
		else{
			redirect('login');
		}
	}
	
	
	public function edit_status_berkas($id)
	{
		$query = $this->db->query("SELECT * FROM kendaraan where id = '$id'")->row();

		$data = array(
			'query'     => $query,
		);
		echo json_encode($data);
	}

	
	public function edit_status($id)
	{
		$query = $this->db->query("SELECT * FROM kendaraan where id = '$id'")->row();

		$data = array(
			'query'     => $query,
		);
		echo json_encode($data);
	}

	public function update_status_berkas()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {
			$id     = $this->input->post('update_service_berkas_id');
			$status = $this->input->post('update_status_service_berkas');
			$bengkel = $this->input->post('update_bengkel_service_berkas');
			
			if($status=='approved'){
				$status_service = 'process';
			}
			else{
				$status_service = '';
			}
			$data = array(
				'status'         => $status,
				'status_service' => $status_service,
				'bengkel'        => $bengkel
			);
					
			$this->db->where('id', $id);
			$this->db->update('kendaraan', $data);
			redirect("kendaraan/service");
		} else {

			redirect('login');
		}
		
	}

	public function update_status()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {
			$id     = $this->input->post('update_service_id');
			$status = $this->input->post('update_status_service');
			$harga  = $this->input->post('update_harga_service');
			
			$data = array(
				'status_service' => $status,
				'harga'          => $harga
			);
					
			$this->db->where('id', $id);
			$this->db->update('kendaraan', $data);
			redirect("kendaraan/service");
		} else {

			redirect('login');
		}
		
	}

	public function delete_service($id=null)
		{   
			$check_session = $this->session->userdata('nik');
			
			if ($check_session != null) {		
				
				if (!isset($id)) show_404();
				else{				
					$nik = $this->session->userdata('nik');
					$now = date('Y-m-d H:i:s');
		
					$data = array(
						'deleted_at' => $now,
						'deleted_by' => $nik,
					);
							
					$this->db->where('id', $id);
					$this->db->update('kendaraan', $data);
			
					// $this->db->delete('k_nopolisi',$data);
					
					redirect("Kendaraan/service");
				}
			} else {
	
				redirect('login');
			}
		}

	//=============================================== BBM =====================================

	public function bahan_bakar()
	{

		$check_session = $this->session->userdata('nik');
		if ($check_session != null) {
			$data = array(
				'no_polisi'              => $this->Kendaraan_model->getNopol(),
				'no_polisi_selected'   => '',
			);
			$this->load->view('kendaraan/bahan_bakar',$data);
		} 
		else{
			redirect('login');
		}
		
		
	}

	public function data_bahan_bakar()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'no_nota',
			2 => 'no_polisi',
			3 => 'tgl',
			4 => 'spbu'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$role = $this->session->userdata('role');
		$totalData = $this->Kendaraan_model->all_bahan_bakar_count($role);

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Kendaraan_model->all_bahan_bakar_data($limit, $start, $order, $dir, $role);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Kendaraan_model->search_bahan_bakar_data($limit, $start, $order, $dir, $search, $role);

			$totalFiltered = $this->Kendaraan_model->search_bahan_bakar_count($search, $role);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp                  	  = '"'.$value->id.'"';
				$nestedData['no_nota']        = ucwords($value->no_nota);
				$nestedData['no_polisi']      = ucwords($value->nopol);
				$nestedData['tgl']        	  = ucwords(date('d-m-Y',strtotime($value->tgl)));
				$nestedData['spbu'] 		  = ucwords($value->spbu);
				$nestedData['harga']          = 'Rp. '.number_format($value->harga ,0,",",".");
				$nestedData['ttl_bayar']      = 'Rp. '.number_format($value->ttl_bayar ,0,",",".");
				$nestedData['jml_liter']      = ucwords($value->jml_liter);
				$nestedData['action']    = "<center>
				<a onclick='return delete_bahan_bakar($_temp)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function simpan_bahan_bakar()
	{
		$check_session = $this->session->userdata('nik');
		if ($check_session != null) {
			$nik = $this->session->userdata('nik');
			$now = date('Y-m-d H:i:s');

			$data = array(
				'id'         => $this->uuid->v4(),
				'no_nota'    => $this->input->post('no_nota'),
				'no_polisi'  => $this->input->post('no_polisi'),
				'tgl'        => date('Y-m-d',strtotime($this->input->post('tgl'))),
				'spbu'       => $this->input->post('spbu'),
				'harga'      => $this->input->post('harga'),
				'ttl_bayar'  => $this->input->post('ttl_bayar'),
				'jml_liter'  => $this->input->post('jml_liter'),
				// 'keterangan' => $this->input->post('keterangan'),
				'created_by' => $nik,
				'created_at' => $now,
			);

			$this->db->insert('k_bahan_bakar', $data);
			redirect("Kendaraan/bahan_bakar");
		} 
		else{
			redirect('login');
		}
		
	}

	public function delete_bahan_bakar($id=null)
    {   
		$check_session = $this->session->userdata('nik');
		if ($check_session != null) {
			if (!isset($id)) show_404();
			else{
				
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');

				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('k_bahan_bakar', $data);
				
				redirect("Kendaraan/bahan_bakar");
			}
		} 
		else{
			redirect('login');
		}
	}

	//========================================== REPORT SERVICE ==================================

	public function report_service()
	{
		$check_session = $this->session->userdata('nik');
		if ($check_session != null) {
			$this->load->view('kendaraan/report_service');
		} 
		else{
			redirect('login');
		}
		
	}

	public function data_report_service()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'pengajuan_date',
			2 => 'nopol',
			3 => 'servis',
			4 => 'harga',
			5 => 'nama',
			6 => 'name',
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$tanggal1 = $this->input->post('tanggal1');
		$tanggal2 = $this->input->post('tanggal2');
		$nopol = $this->input->post('nopol');
		$awal     = ($tanggal1!=''?date('Y-m-d',strtotime($tanggal1)):date('Y-m-d'));
		$akhir    = ($tanggal2!=''?date('Y-m-d',strtotime($tanggal2)):date('Y-m-d'));
		
		$totalData = $this->Kendaraan_model->all_report_service_count($awal, $akhir, $nopol);

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Kendaraan_model->all_report_service_data($limit, $start, $order, $dir, $awal, $akhir, $nopol);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Kendaraan_model->search_report_service_data($limit, $start, $order, $dir, $search, $awal, $akhir);

			$totalFiltered = $this->Kendaraan_model->search_report_service_count($search, $awal, $akhir);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp            = '"'.$value->id.'"';
				$nestedData['pengajuan_date'] = ucwords(date('d-m-Y',strtotime($value->pengajuan_date)));
				$nestedData['no_polisi']      = ucwords($value->nopol);

				
				$service = explode(',',$value->service);
				$hasil_service = '';
				foreach ($service as $key => $detail) {
					$nama = $this->Kendaraan_model->getService_id($detail);
					if($nama!=null){
						$hasil_service .= ''.$nama->servis.', ';
					}
				}
				$hasil_service = rtrim($hasil_service, ", ");
				
				// $nestedData['service']        = ucwords($value->servis);
				$nestedData['service']    = ucwords($hasil_service);
				$nestedData['harga']      = 'Rp. '.number_format($value->harga ,0,",",".");
				$nestedData['name']       = ucwords($value->name);
				$nestedData['bengkel']    = ucwords($value->nama);
				$nestedData['keterangan'] = ucwords($value->keterangan);

				if($value->status == 'approved'){
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-success'>DISETUJUI</a>
					</center>";
				}
				elseif($value->status == 'cancel'){
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-danger'>DITOLAK</a>
					</center>";
				}
				else{
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-warning'>MENUNGGU</a>
					</center>";
				}

				if($value->status_service == 'done'){
					
					$nestedData['status_service']     = "<center>
					<a class='btn btn-xs btn-success'>SELESAI</a>
					</center>";
				}
				elseif($value->status_service == 'process'){
					$nestedData['status_service']     = "<center>
					<a class='btn btn-xs btn-warning'>PROSES</a>
					</center>";
				}
				else{
					$nestedData['status_service']     = "";
				}

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	 
    public function export_report_service(){
		// include APPPATH.'third_party/Classes/PHPsheet.php';
        
        // include $_SERVER["DOCUMENT_ROOT"].'/pengarsipan-02/application/third_party/Classes/PHPsheet/IOFactory.php';

		$tanggal1 = $this->input->get('tanggal1');
		$tanggal2 = $this->input->get('tanggal2');
		$nopol = $this->input->get('nopol');
		$dari     = ($tanggal1!=''?date('Y-m-d',strtotime($tanggal1)):date('Y-m-d'));
		$sampai   = ($tanggal2!=''?date('Y-m-d',strtotime($tanggal2)):date('Y-m-d'));

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();;

		$sheet->setCellValue('A1', "NO");
		$sheet->getColumnDimension('A')->setAutoSize(true); 

		$sheet->setCellValue('B1', "TANGGAL PENGAJUAN");
		$sheet->getColumnDimension('B')->setAutoSize(true); 

		$sheet->setCellValue('C1', "NO POLISI");
		$sheet->getColumnDimension('C')->setAutoSize(true); 

		$sheet->setCellValue('D1', "SERVICE");
		$sheet->getColumnDimension('D')->setAutoSize(true); 

		$sheet->setCellValue('E1', "HARGA");
		$sheet->getColumnDimension('E')->setAutoSize(true); 

		$sheet->setCellValue('F1', "BENGKEL");
		$sheet->getColumnDimension('F')->setAutoSize(true); 

		$sheet->setCellValue('G1', "KETERANGAN");
		$sheet->getColumnDimension('G')->setAutoSize(true); 

		$sheet->setCellValue('H1', "DIAJUKAN OLEH (NIK)");
		$sheet->getColumnDimension('H')->setAutoSize(true); 

		$sheet->setCellValue('I1', "DIAJUKAN OLEH (NAMA)");
		$sheet->getColumnDimension('I')->setAutoSize(true); 
		
		$sheet->setCellValue('J1', "STATUS BERKAS");
		$sheet->getColumnDimension('J')->setAutoSize(true); 
		
		$sheet->setCellValue('K1', "STATUS SERVICE");
		$sheet->getColumnDimension('K')->setAutoSize(true); 

		$details = $this->Kendaraan_model->export_report_service($dari,$sampai,$nopol);

		$no     = 1;
		$numrow = 2;
		foreach ($details as $key => $detail) {

				
			$service = explode(',',$detail->service);
			$hasil_service = '';
			foreach ($service as $key => $value) {
				$nama = $this->Kendaraan_model->getService_id($value);
				if($nama!=null){
					$hasil_service .= ''.$nama->servis.', ';
				}
			}
			$hasil_service = rtrim($hasil_service, ", ");

			$sheet->setCellValue('A'.$numrow, $no);
			$sheet->setCellValue('B'.$numrow, date('d-m-Y',strtotime($detail->pengajuan_date)));
			$sheet->setCellValue('C'.$numrow, ucwords($detail->nopol));
			// $sheet->setCellValue('D'.$numrow, ucwords($detail->servis));
			$sheet->setCellValue('D'.$numrow, ucwords($hasil_service));
			$sheet->setCellValue('E'.$numrow, ucwords($detail->harga));
			$sheet->setCellValue('F'.$numrow, ucwords($detail->nama));
			$sheet->setCellValue('G'.$numrow, ucwords($detail->keterangan));
			$sheet->setCellValue('H'.$numrow, ucwords($detail->created_by));
			$sheet->setCellValue('I'.$numrow, ucwords($detail->name));
			if($detail->status == 'approved'){
				$status = 'disetujui';
			}
				elseif($detail->status == 'cancel'){
			$status = 'ditolak';
			}
			else{
				$status = 'menunggu';
			}

			if($detail->status_service == 'done'){		
				$status_service = 'selesai';
			}
			elseif($detail->status_service == 'process'){
				$status_service = 'proses';
			}
			else{
				$status_service = "-";
			}
			
			$sheet->setCellValue('J'.$numrow, ucwords($status));
			$sheet->setCellValue('K'.$numrow, ucwords($status_service));
			$no++;
			$numrow++;
		}

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		
		$sheet->getDefaultRowDimension()->setRowHeight(-1);

		$writer   = new Xlsx($spreadsheet);
		$filename = 'Laporan Service Kendaraan '.$tanggal1.' sd '.$tanggal2.' ';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
		
		ob_end_clean();+
		$writer->save('php://output');
    }
	
	//======================================= REPORT BAHAN BAKAR ==================================
	public function report_bahan_bakar()
	{
		$check_session = $this->session->userdata('nik');
		if ($check_session != null) {
			$this->load->view('kendaraan/report_bahan_bakar');
		} 
		else{
			redirect('login');
		}
		
	}

	public function data_report_bahan_bakar()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'tgl',
			2 => 'no_nota',
			3 => 'no_polisi',
			4 => 'spbu',
			5 => 'harga',
			6 => 'jml_liter',
			7 => 'ttl_bayar',
			8 => 'name',
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$tanggal1 = $this->input->post('tanggal1');
		$tanggal2 = $this->input->post('tanggal2');
		$nopol = $this->input->post('nopol');
		$awal     = ($tanggal1!=''?date('Y-m-d',strtotime($tanggal1)):date('Y-m-d'));
		$akhir    = ($tanggal2!=''?date('Y-m-d',strtotime($tanggal2)):date('Y-m-d'));
		
		$totalData = $this->Kendaraan_model->all_report_bbm_count($awal, $akhir, $nopol);

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Kendaraan_model->all_report_bbm_data($limit, $start, $order, $dir, $awal, $akhir, $nopol);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Kendaraan_model->search_report_bbm_data($limit, $start, $order, $dir, $search, $awal, $akhir);

			$totalFiltered = $this->Kendaraan_model->search_report_bbm_count($search, $awal, $akhir);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp            		 = '"'.$value->id.'"';
				$nestedData['tgl']       = ucwords(date('d-m-Y',strtotime($value->tgl)));
				$nestedData['no_polisi'] = ucwords($value->nopol);
				$nestedData['no_nota']   = ucwords($value->no_nota);
				$nestedData['spbu']      = ucwords($value->spbu);
				$nestedData['harga']     = 'Rp. '.number_format($value->harga ,0,",",".");
				$nestedData['jml_liter'] = ucwords($value->jml_liter);
				$nestedData['ttl_bayar'] = 'Rp. '.number_format($value->ttl_bayar ,0,",",".");
				$nestedData['name']      = ucwords($value->name);

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	 
    public function export_report_bahan_bakar(){
		// include APPPATH.'third_party/Classes/PHPsheet.php';
        
        // include $_SERVER["DOCUMENT_ROOT"].'/pengarsipan-02/application/third_party/Classes/PHPsheet/IOFactory.php';

		$tanggal1 = $this->input->get('tanggal1');
		$tanggal2 = $this->input->get('tanggal2');
		$nopol = $this->input->get('nopol');
		$dari     = ($tanggal1!=''?date('Y-m-d',strtotime($tanggal1)):date('Y-m-d'));
		$sampai   = ($tanggal2!=''?date('Y-m-d',strtotime($tanggal2)):date('Y-m-d'));

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();;

		$sheet->setCellValue('A1', "NO");
		$sheet->getColumnDimension('A')->setAutoSize(true); 

		$sheet->setCellValue('B1', "TANGGAL");
		$sheet->getColumnDimension('B')->setAutoSize(true); 

		$sheet->setCellValue('C1', "NO NOTA");
		$sheet->getColumnDimension('C')->setAutoSize(true); 

		$sheet->setCellValue('D1', "NO POLISI");
		$sheet->getColumnDimension('D')->setAutoSize(true); 

		$sheet->setCellValue('E1', "SPBU");
		$sheet->getColumnDimension('E')->setAutoSize(true); 

		$sheet->setCellValue('F1', "HARGA/LITER");
		$sheet->getColumnDimension('F')->setAutoSize(true); 

		$sheet->setCellValue('G1', "JUMLAH LITER");
		$sheet->getColumnDimension('G')->setAutoSize(true); 

		$sheet->setCellValue('H1', "TOTAL BAYAR");
		$sheet->getColumnDimension('H')->setAutoSize(true); 

		$sheet->setCellValue('I1', "DIAJUKAN OLEH (NIK)");
		$sheet->getColumnDimension('I')->setAutoSize(true); 

		$sheet->setCellValue('J1', "DIAJUKAN OLEH (NAMA)");
		$sheet->getColumnDimension('J')->setAutoSize(true); 

		$details = $this->Kendaraan_model->export_report_bbm($dari,$sampai,$nopol);

		$no     = 1;
		$numrow = 2;
		foreach ($details as $key => $detail) {

		  $sheet->setCellValue('A'.$numrow, $no);
		  $sheet->setCellValue('B'.$numrow, date('d-m-Y',strtotime($detail->tgl)));
          $sheet->setCellValue('C'.$numrow, ucwords($detail->no_nota));
		  $sheet->setCellValue('D'.$numrow, ucwords($detail->nopol));
		  $sheet->setCellValue('E'.$numrow, ucwords($detail->spbu));
		  $sheet->setCellValue('F'.$numrow, ucwords($detail->harga));
		  $sheet->setCellValue('G'.$numrow, ucwords($detail->jml_liter));
		  $sheet->setCellValue('H'.$numrow, ucwords($detail->ttl_bayar));
		  $sheet->setCellValue('I'.$numrow, ucwords($detail->created_by));
		  $sheet->setCellValue('J'.$numrow, ucwords($detail->name));
		  $no++;
		  $numrow++;
		}

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		
		$sheet->getDefaultRowDimension()->setRowHeight(-1);

		$writer   = new Xlsx($spreadsheet);
		$filename = 'Laporan Bahan Bakar Kendaraan '.$tanggal1.' sd '.$tanggal2.' ';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
		
		ob_end_clean();+
		$writer->save('php://output');
	}

	public function laporan_pdf(){

		$value = $this
		->db
		->limit(1)
        ->order_by('kendaraan.pengajuan_date, nopol, name')
		->where('kendaraan.deleted_at',null)
		->where('kendaraan.id','ec41b11e-c9be-49ec-81ce-9a8574794637')
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('k_service','k_service.id = kendaraan.service','left')
        ->join('users','users.nik = kendaraan.created_by','left')
        ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, k_service.servis, users.name')
        ->get('kendaraan')->row();
		
		
		$service = explode(',',$value->service);
		$hasil_service = '';
		foreach ($service as $key => $detail) {
			$nama = $this->Kendaraan_model->getService_id($detail);
			if($nama!=null){
				$hasil_service .= ''.$nama->servis.', ';
			}
		}
		$hasil_service = rtrim($hasil_service, ", ");
		
		
		if($value->status == 'approved'){
			$status     = "<center>
			<a class='btn btn-xs btn-success'>DISETUJUI</a>
			</center>";
		}
		elseif($value->status == 'cancel'){
			$status     = "<center>
			<a class='btn btn-xs btn-danger'>DITOLAK</a>
			</center>";
		}
		else{
			$status     = "<center>
			<a class='btn btn-xs btn-warning'>MENUNGGU</a>
			</center>";
		}
		
		$pengajuan_date = ucwords(date('d-m-Y',strtotime($value->pengajuan_date)));
		$no_polisi      = ucwords($value->nopol);
		$service        = ucwords($hasil_service);
		$harga          = 'Rp. '.number_format($value->harga ,0,",",".");
		$nama           = ucwords($value->name);
		$keterangan     = ucwords($value->keterangan);

		$data = array(
			'tanggal'    => $pengajuan_date,
			'nopol'      => $no_polisi,
			'service'    => $service,
			'harga'      => $harga,
			'nama'       => $nama,
			'keterangan' => $keterangan,	
		);

		// $this->load->view('kendaraan/catatan2',$data);
	
		$this->load->library('pdf');
	
		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "catatan.pdf";
		$this->pdf->load_view('kendaraan/catatan',$data);
	
	
	}

	public function test()
	{
		
	}
}
