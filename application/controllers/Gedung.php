<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Gedung extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Gedung_model');
		$this->load->model('Login_model');
	}

	public function index()
	{
		$check_session = $this->session->userdata('nik');
		if ($check_session != null) {
			$data = array(
				'lantai'             => $this->Gedung_model->getLantai(),
				'kerusakan'          => $this->Gedung_model->getKerusakan(),
			);

			$this->load->view('gedung/index',$data);
		} 
		else{
			redirect('login');
		}
	}

	public function data_gedung()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'lantai',
			2 => 'kerusakan'
			// 3 => 'jenis_pekerjaan'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$role = $this->session->userdata('role');

		$totalData = $this->Gedung_model->all_gedung_count($role);

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Gedung_model->all_gedung_data($limit, $start, $order, $dir, $role);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Gedung_model->search_gedung_data($limit, $start, $order, $dir, $search, $role);

			$totalFiltered = $this->Gedung_model->search_gedung_count($search, $role);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp             = '"'.$value->id.'"';
				$nestedData['lantai']          = ucwords($value->lantai_level);
				$nestedData['kerusakan']       = ucwords($value->kerusakan_gedung);
				// $nestedData['jenis_pekerjaan'] = ucwords($value->jenis_pekerjaan);
				$nestedData['keterangan']      = ucwords($value->keterangan);
				$nestedData['action']     = "<center>
				<a onclick='return delete_gedung($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}


	public function simpan()
	{
		
		$check_session = $this->session->userdata('nik');
		if ($check_session != null) {	
			$gedung   = $this->Gedung_model;
			$nik = $this->session->userdata('nik');
			$now = date('Y-m-d H:i:s');

			$data = array(
				'id'         => $this->uuid->v4(),
				'lantai'     => $this->input->post('lantai'),
				'kerusakan'  => $this->input->post('kerusakan'),
				'keterangan' => $this->input->post('keterangan'),
				'created_by' => $nik,
				'created_at' => $now,
				// 'jenis_pekerjaan' => $this->input->post('jenis_pekerjaan'),
			);

			$gedung->save($data);

			$this->load->view("gedung/index", $data);
			redirect("gedung");
		} 
		else{
			redirect('login');
		}
		
	}

	public function delete_gedung($id=null)
    {   
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {			
			if (!isset($id)) show_404();
			else{
				
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('gedung', $data);
				
				redirect("Gedung");
			}
		} else {
			redirect('login');
		}
		
	}
	
	//========================================= REPORT GEDUNG ====================================
	public function report_gedung()
	{
		$check_session = $this->session->userdata('nik');
		if ($check_session != null) {
			$this->load->view('gedung/report_gedung');
		} 
		else{
			redirect('login');
		}
		
	}

	public function data_report_gedung()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'lantai',
			2 => 'kerusakan_gedung',
			3 => 'keterangan',
			4 => 'name',
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$tanggal1 = $this->input->post('tanggal1');
		$tanggal2 = $this->input->post('tanggal2');
		$awal     = ($tanggal1!=''?date('Y-m-d',strtotime($tanggal1)):date('Y-m-d'));
		$akhir    = ($tanggal2!=''?date('Y-m-d',strtotime($tanggal2)):date('Y-m-d'));
		
		$totalData = $this->Gedung_model->all_report_gedung_count($awal, $akhir);

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Gedung_model->all_report_gedung_data($limit, $start, $order, $dir, $awal, $akhir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Gedung_model->search_report_gedung_data($limit, $start, $order, $dir, $search, $awal, $akhir);

			$totalFiltered = $this->Gedung_model->search_report_gedung_count($search, $awal, $akhir);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				            $_temp              = '"'.$value->id.'"';
				$nestedData['created_at']       = ucwords(date('d-m-Y',strtotime($value->created_at)));
				$nestedData['lantai']           = ucwords($value->lantai);
				$nestedData['kerusakan_gedung'] = ucwords($value->kerusakan_gedung);
				$nestedData['keterangan']       = ucwords($value->keterangan);
				$nestedData['name']             = ucwords($value->name);

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	 
    public function export_report_gedung(){
		// include APPPATH.'third_party/Classes/PHPsheet.php';
        
        // include $_SERVER["DOCUMENT_ROOT"].'/pengarsipan-02/application/third_party/Classes/PHPsheet/IOFactory.php';

		$tanggal1 = $this->input->get('tanggal1');
		$tanggal2 = $this->input->get('tanggal2');
		$dari     = ($tanggal1!=''?date('Y-m-d',strtotime($tanggal1)):date('Y-m-d'));
		$sampai   = ($tanggal2!=''?date('Y-m-d',strtotime($tanggal2)):date('Y-m-d'));

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();;

		$sheet->setCellValue('A1', "NO");
		$sheet->getColumnDimension('A')->setAutoSize(true); 

		$sheet->setCellValue('B1', "TANGGAL");
		$sheet->getColumnDimension('B')->setAutoSize(true); 

		$sheet->setCellValue('C1', "LANTAI");
		$sheet->getColumnDimension('C')->setAutoSize(true); 

		$sheet->setCellValue('D1', "KERUSAKAN");
		$sheet->getColumnDimension('D')->setAutoSize(true); 

		$sheet->setCellValue('E1', "KETERANGAN");
		$sheet->getColumnDimension('E')->setAutoSize(true); 

		$sheet->setCellValue('F1', "DIAJUKAN OLEH (NIK)");
		$sheet->getColumnDimension('F')->setAutoSize(true); 

		$sheet->setCellValue('G1', "DIAJUKAN OLEH (NAMA)");
		$sheet->getColumnDimension('G')->setAutoSize(true); 

		$details = $this->Gedung_model->export_report_gedung($dari,$sampai);

		$no     = 1;
		$numrow = 2;
		foreach ($details as $key => $detail) {

		  $sheet->setCellValue('A'.$numrow, $no);
		  $sheet->setCellValue('B'.$numrow, date('d-m-Y',strtotime($detail->created_at)));
		  $sheet->setCellValue('C'.$numrow, ucwords($detail->lantai));
          $sheet->setCellValue('D'.$numrow, ucwords($detail->kerusakan_gedung));
		  $sheet->setCellValue('E'.$numrow, ucwords($detail->keterangan));
		  $sheet->setCellValue('F'.$numrow, ucwords($detail->created_by));
		  $sheet->setCellValue('G'.$numrow, ucwords($detail->name));
		  $no++;
		  $numrow++;
		}

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		
		$sheet->getDefaultRowDimension()->setRowHeight(-1);

		$writer   = new Xlsx($spreadsheet);
		$filename = 'Laporan Gedung '.$tanggal1.' sd '.$tanggal2.' ';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
		
		ob_end_clean();+
		$writer->save('php://output');
    }
}
