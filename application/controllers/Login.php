<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Login_model');
	}


	public function index()
	{
		// die();
		$check_session = $this->session->userdata('nik');
		
		// var_dump($check_session);
		if ($check_session != null) {
			redirect('Dashboard');
		} else {

			$this->load->view("login");
		}
	}

	public function logon($post=NULL)
	{
		$nik      = $this->input->post("nik");
		$password = $this->input->post('password');

		$pass     = $password;
		$checking = $this->Login_model->check_user($nik,$pass);

		if ($checking->num_rows() > 0) {
			foreach ($checking->result() as $apps) {
				
				$session_data = array(
					'nik'      => $apps->nik,
					'name'     => $apps->name,
					'email'    => $apps->email,
					'role'     => $apps->role,
				);
				//set session userdata
				$this->session->set_userdata($session_data);
				$status = 200;
			}
		} else {
			$status = 422;
		}

		
		$data = array(
			'status'     => $status, 
		);
		echo json_encode($data);
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
}
