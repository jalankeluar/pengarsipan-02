<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Users_model');
		$this->load->model('Login_model');
	}

	public function index()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {					
			$this->load->view('users/index');
		} else {
			redirect('login');
		}
	}

	public function data_users()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'nik',
			2 => 'name'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$totalData = $this->Users_model->all_users_count();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Users_model->all_users_data($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Users_model->search_users_data($limit, $start, $order, $dir, $search);

			$totalFiltered = $this->Users_model->search_users_count($search);
		}

		$data       = array();
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp   = '"'.$value->id.'"';
				$nestedData['nik']   = ucwords($value->nik);
				$nestedData['name']  = ucwords($value->name);
				$nestedData['email'] = ucwords($value->email);
				$nestedData['role']     = ucwords($value->role);
				$nestedData['action']      = "<center>
				<a onclick='return edit_users($_temp)' class='btn btn-xs btn-warning'><i class='fa fa-pencil'></i> Edit</a>
				<a onclick='return delete_users($_temp)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

				$data[] = $nestedData;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function simpan()
	{
		
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {					
				
            $nik = $this->session->userdata('nik');
		    $now = date('Y-m-d H:i:s');
			$users   = $this->Users_model;

			$data = array(
			'id'        => $this->uuid->v4(),
			'nik'      => $this->input->post('nik'),
			'name'    => $this->input->post('name'),
			'email'	=> $this->input->post('email'),
            'password'=> $this->input->post('password'),
			'role'=> $this->input->post('role'),
			'created_by' => $nik,
			'created_at' => $now,

			);

			$users->save($data);

			$this->load->view("users/index", $data);
			redirect("users");
		} else {
			redirect('login');
		}

		
	}

	public function edit_users($id)
	{
		$query = $this->db->query("SELECT * FROM users where id = '$id'")->row();

		// return response()->json($query,200);
		$data = array(
			'query' => $query,
		);
		echo json_encode($data);
		// return $query;
	}

	public function update_users()
	{
		$id   = $this->input->post('edit_users_id');
		$data = array(
			'nik'  => $this->input->post('edit_nik'),
			'name' => $this->input->post('edit_name'),
			'email' => $this->input->post('edit_email'),
			'password' => $this->input->post('edit_password'),
			'role' => $this->input->post('edit_role'),
		);

		// var_dump($this->input->post());
		// var_dump($data);
		// die();

				
		$this->db->where('id', $id);
		$this->db->update('users', $data);
		redirect("Users");
	}

	public function delete_users($id=null)
    {   
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {		
			
			if (!isset($id)) show_404();
			else{				
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('users', $data);
		
				// $this->db->delete('k_nopolisi',$data);
				
				redirect("users");
			}
		} else {

			redirect('login');
		}
	}

	
}
