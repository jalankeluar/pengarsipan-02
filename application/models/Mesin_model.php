<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mesin_model extends CI_Model
{

  private $_table = "mesin";

  public function view()
  {
    return $this->db->query('SELECT * from mesin')->result();
  }

  public function save($data)
  {
    $this->db->insert($this->_table, $data);
  }

  public function update($where, $data, $_table)
  {
    $this->db->where($where);
    $this->db->update($_table, $data);
  }

  public function delete($id)
  {
    return $this->db->delete($this->_table, array("id" => $id));
  }

  public function all_mesin_count($role)
  {
    $nik  = $this->session->userdata('nik');

    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->get('mesin');
    }
    else{
      $query = $this
      ->db
      ->where('deleted_at',null)
      ->where('created_by',$nik)
      ->get('mesin');
    }

    return $query->num_rows();
  }

  public function all_mesin_data($limit, $start, $col, $dir, $role)
  {
    $nik  = $this->session->userdata('nik');

    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('mesin.deleted_at',null)
        ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
        ->join('m_kerusakan','m_kerusakan.id = mesin.kerusakan','left')
        ->join('vendor','vendor.id = mesin.vendor','left')
        ->select('mesin.id, mesin.created_at, vendor.vendor, mesin.kerusakan, mesin.harga, mesin.keterangan, m_jenis_mesin.jenis_mesin, mesin.status, mesin.status_service, m_kerusakan.kerusakan_mesin, m_jenis_mesin.no_aset')
        ->get('mesin');
    }
    else{
      $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('mesin.deleted_at',null)
      ->where('mesin.created_by',$nik)
      ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
      ->join('m_kerusakan','m_kerusakan.id = mesin.kerusakan','left')
      ->select('mesin.id, mesin.created_at,  vendor.vendor, mesin.kerusakan, mesin.harga, mesin.keterangan, m_jenis_mesin.jenis_mesin, mesin.status, mesin.status_service, m_kerusakan.kerusakan_mesin, m_jenis_mesin.no_aset')
      ->get('mesin');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_mesin_count($search, $role)
  {
    $nik  = $this->session->userdata('nik');

    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->like('m_jenis_mesin.jenis_mesin', $search)
        ->or_like('m_kerusakan.kerusakan_mesin', $search)
        ->where('mesin.deleted_at',null)
        ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
        ->join('m_kerusakan','m_kerusakan.id = mesin.kerusakan','left')
        ->select('mesin.id, mesin.created_at,  vendor.vendor, mesin.kerusakan, mesin.harga, mesin.keterangan, m_jenis_mesin.jenis_mesin, mesin.status, mesin.status_service, m_kerusakan.kerusakan_mesin, m_jenis_mesin.no_aset')
        ->get('mesin');
    }
    else{
      $query = $this
        ->db
        ->like('m_jenis_mesin.jenis_mesin', $search)
        ->or_like('m_kerusakan.kerusakan_mesin', $search)
        ->where('mesin.deleted_at',null)
        ->where('mesin.created_by',$nik)
        ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
        ->join('m_kerusakan','m_kerusakan.id = mesin.kerusakan','left')
        ->select('mesin.id, mesin.created_at,  vendor.vendor, mesin.kerusakan, mesin.harga, mesin.keterangan, m_jenis_mesin.jenis_mesin, mesin.status, mesin.status_service, m_kerusakan.kerusakan_mesin, m_jenis_mesin.no_aset')
        ->get('mesin');
    }

    return $query->num_rows();
  }

  public function search_mesin_data($limit, $start, $col, $dir, $search, $role)
  {
    $nik  = $this->session->userdata('nik');

    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->like('m_jenis_mesin.jenis_mesin', $search)
        ->or_like('m_kerusakan.kerusakan_mesin', $search)
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('mesin.deleted_at',null)
        ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
        ->join('m_kerusakan','m_kerusakan.id = mesin.kerusakan','left')
        ->select('mesin.id, mesin.created_at,  vendor.vendor, mesin.kerusakan, mesin.harga, mesin.keterangan, m_jenis_mesin.jenis_mesin, mesin.status, mesin.status_service, m_kerusakan.kerusakan_mesin, m_jenis_mesin.no_aset')
        ->get('mesin');
    }
    else{
      $query = $this
        ->db
        ->like('m_jenis_mesin.jenis_mesin', $search)
        ->or_like('m_kerusakan.kerusakan_mesin', $search)
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('mesin.deleted_at',null)
        ->where('mesin.created_by',$nik)
        ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
        ->join('m_kerusakan','m_kerusakan.id = mesin.kerusakan','left')
        ->select('mesin.id, mesin.created_at,  vendor.vendor, mesin.kerusakan, mesin.harga, mesin.keterangan, m_jenis_mesin.jenis_mesin, mesin.status, mesin.status_service, m_kerusakan.kerusakan_mesin, m_jenis_mesin.no_aset')
        ->get('mesin');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function all_jenis_mesin_count()
  {
    $query = $this
      ->db
      ->where('deleted_at',null)
      ->get('m_jenis_mesin');

    return $query->num_rows();
  }

  public function all_jenis_mesin_data($limit, $start, $col, $dir)
  {
    $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('m_jenis_mesin');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_jenis_mesin_count($search)
  {
    $query = $this
      ->db
      ->like('jenis_mesin', $search)
      ->where('deleted_at',null)
      ->get('m_jenis_mesin');

    return $query->num_rows();
  }

  public function search_jenis_mesin_data($limit, $start, $col, $dir, $search)
  {
    $query = $this
      ->db
      ->like('jenis_mesin', $search)
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('m_jenis_mesin');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }
  
  public function all_kerusakan_mesin_count()
  {
    $query = $this
      ->db
      ->where('deleted_at',null)
      ->get('m_kerusakan');

    return $query->num_rows();
  }

  public function all_kerusakan_mesin_data($limit, $start, $col, $dir)
  {
    $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('m_kerusakan');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_kerusakan_mesin_count($search)
  {
    $query = $this
      ->db
      ->like('kerusakan_mesin', $search)
      ->where('deleted_at',null)
      ->get('m_kerusakan');

    return $query->num_rows();
  }

  public function search_kerusakan_mesin_data($limit, $start, $col, $dir, $search)
  {
    $query = $this
      ->db
      ->like('kerusakan_mesin', $search)
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('m_kerusakan');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }
  
  public function getJenisMesin()
  {
    return $this->db->query('SELECT * from m_jenis_mesin where deleted_at is null')->result();
  }
  
  public function getKerusakan()
  {
    return $this->db->query('SELECT * from m_kerusakan where deleted_at is null')->result();
  }

  public function getVendor()
  {
    return $this->db->query('SELECT * from vendor where kategori like "%mesin%" and deleted_at is null')->result();
  }
  
  public function getKerusakan_id($id)
  {
      return $this->db->query("SELECT * from m_kerusakan where id='$id' and deleted_at is null")->row();
  }
  
  
  public function all_report_mesin_count($awal,$akhir)
  {
    if($awal==$akhir){
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->where('date(mesin.created_at)', $awal)
        ->get('mesin');
    }
    else{
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->where('mesin.created_at >=', $awal)
        ->where('mesin.created_at <=', $akhir)
        ->get('mesin');
    }

    return $query->num_rows();
  }

  public function all_report_mesin_data($limit, $start, $col, $dir, $awal, $akhir)
  {
    if($awal==$akhir){
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('mesin.deleted_at',null)
        ->where('date(mesin.created_at)', $awal)
        ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
        ->join('m_kerusakan','m_kerusakan.id = mesin.kerusakan','left')
        ->join('users','users.nik = mesin.created_by','left')
        ->select('mesin.id, mesin.created_at,  vendor.vendor, mesin.kerusakan, mesin.harga, mesin.keterangan, mesin.status, mesin.status_service, m_jenis_mesin.jenis_mesin, m_kerusakan.kerusakan_mesin, users.name, m_jenis_mesin.no_aset')
        ->get('mesin');
    }
    else{
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('mesin.deleted_at',null)
        ->where('mesin.created_at >=', $awal)
        ->where('mesin.created_at <=', $akhir)
        ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
        ->join('m_kerusakan','m_kerusakan.id = mesin.kerusakan','left')
        ->join('users','users.nik = mesin.created_by','left')
        ->select('mesin.id, mesin.created_at,  vendor.vendor, mesin.kerusakan, mesin.harga, mesin.keterangan, mesin.status, mesin.status_service, m_jenis_mesin.jenis_mesin, m_kerusakan.kerusakan_mesin, users.name, m_jenis_mesin.no_aset')
        ->get('mesin');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_report_mesin_count($search, $awal, $akhir)
  {
    if($awal==$akhir){
      $query = $this
        ->db
        ->like('jenis_mesin', $search)
        ->or_like('kerusakan_mesin', $search)
        ->or_like('name', $search)
        ->where('mesin.deleted_at',null)
        ->where('date(mesin.created_at)', $awal)
        ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
        ->join('m_kerusakan','m_kerusakan.id = mesin.kerusakan','left')
        ->join('users','users.nik = mesin.created_by','left')
        ->select('mesin.id, mesin.created_at,  vendor.vendor, mesin.kerusakan, mesin.harga, mesin.keterangan, mesin.status, mesin.status_service, m_jenis_mesin.jenis_mesin, m_kerusakan.kerusakan_mesin, users.name, m_jenis_mesin.no_aset')
        ->get('mesin');
    }
    else{
      $query = $this
        ->db
        ->like('jenis_mesin', $search)
        ->or_like('kerusakan_mesin', $search)
        ->or_like('name', $search)
        ->where('mesin.deleted_at',null)
        ->where('mesin.created_at >=', $awal)
        ->where('mesin.created_at <=', $akhir)
        ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
        ->join('m_kerusakan','m_kerusakan.id = mesin.kerusakan','left')
        ->join('users','users.nik = mesin.created_by','left')
        ->select('mesin.id, mesin.created_at,  vendor.vendor, mesin.kerusakan, mesin.harga, mesin.keterangan, mesin.status, mesin.status_service, m_jenis_mesin.jenis_mesin, m_kerusakan.kerusakan_mesin, users.name, m_jenis_mesin.no_aset')
        ->get('mesin');
    }

    return $query->num_rows();
  }

  public function search_report_mesin_data($limit, $start, $col, $dir, $search, $awal, $akhir)
  {
    if($awal==$akhir){
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->like('jenis_mesin', $search)
        ->or_like('kerusakan_mesin', $search)
        ->or_like('name', $search)
        ->where('mesin.deleted_at',null)
        ->where('date(mesin.created_at)', $awal)
        ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
        ->join('m_kerusakan','m_kerusakan.id = mesin.kerusakan','left')
        ->join('users','users.nik = mesin.created_by','left')
        ->select('mesin.id, mesin.created_at,  vendor.vendor, mesin.kerusakan, mesin.harga, mesin.keterangan, mesin.status, mesin.status_service, m_jenis_mesin.jenis_mesin, m_kerusakan.kerusakan_mesin, users.name, m_jenis_mesin.no_aset')
        ->get('mesin');
    }
    else{
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->like('jenis_mesin', $search)
        ->or_like('kerusakan_mesin', $search)
        ->or_like('name', $search)
        ->where('mesin.deleted_at',null)
        ->where('mesin.created_at >=', $awal)
        ->where('mesin.created_at <=', $akhir)
        ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
        ->join('m_kerusakan','m_kerusakan.id = mesin.kerusakan','left')
        ->join('users','users.nik = mesin.created_by','left')
        ->select('mesin.id, mesin.created_at,  vendor.vendor, mesin.kerusakan, mesin.harga, mesin.keterangan, mesin.status, mesin.status_service, m_jenis_mesin.jenis_mesin, m_kerusakan.kerusakan_mesin, users.name, m_jenis_mesin.no_aset')
        ->get('mesin');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function export_report_mesin($awal, $akhir)
  {    
    if($awal==$akhir){
      $query = $this
        ->db
        ->order_by('mesin.created_at, mesin.harga, mesin.jenis_mesin, name')
        ->where('mesin.deleted_at',null)
        ->where('date(mesin.created_at)', $awal)
        ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
        ->join('m_kerusakan','m_kerusakan.id = mesin.kerusakan','left')
        ->join('users','users.nik = mesin.created_by','left')
        ->select('mesin.id, mesin.created_at,  vendor.vendor, mesin.kerusakan, mesin.harga, mesin.keterangan, mesin.status, mesin.status_service, m_jenis_mesin.jenis_mesin, m_kerusakan.kerusakan_mesin, users.name, m_jenis_mesin.no_aset')
        ->get('mesin');
    }
    else{
      $query = $this
        ->db
        ->order_by('mesin.created_at, mesin.harga, mesin.jenis_mesin, name')
        ->where('mesin.deleted_at',null)
        ->where('mesin.created_at >=', $awal)
        ->where('mesin.created_at <=', $akhir)
        ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
        ->join('m_kerusakan','m_kerusakan.id = mesin.kerusakan','left')
        ->join('users','users.nik = mesin.created_by','left')
        ->select('mesin.id, mesin.created_at,  vendor.vendor, mesin.kerusakan, mesin.harga, mesin.keterangan, mesin.status, mesin.status_service, m_jenis_mesin.jenis_mesin, m_kerusakan.kerusakan_mesin, users.name, m_jenis_mesin.no_aset')
        ->get('mesin');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

      // Vendor master data
      public function all_vendor_count()
      {
        $query = $this
          ->db      
          ->where('deleted_at',null)
          ->get('vendor');
    
        return $query->num_rows();
      }
    
      public function all_vendor_data($limit, $start, $col, $dir)
      {
        $query = $this
          ->db
          ->where('deleted_at',null)
          ->limit($limit, $start)
          ->order_by($col, $dir)
          ->get('vendor');
    
        if ($query->num_rows() > 0) {
          return $query->result();
        } else {
          return null;
        }
      }
    
      public function search_vendor_count($search)
      {
        $query = $this
          ->db
          ->where('deleted_at',null)
          ->like('vendor', $search)
          ->or_like('alamat', $search)
          ->or_like('keterangan', $search)
          ->get('vendor');
    
        return $query->num_rows();
      }
    
      public function search_vendor_data($limit, $start, $col, $dir, $search)
      {
        $query = $this
          ->db
          ->where('deleted_at',null)
          ->like('vendor', $search)
          ->or_like('alamat', $search)
          ->or_like('keterangan', $search)
          ->limit($limit, $start)
          ->order_by($col, $dir)
          ->get('vendor');
    
        if ($query->num_rows() > 0) {
          return $query->result();
        } else {
          return null;
        }
      }
}
