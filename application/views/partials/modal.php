 <!-- Modal -->
 <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Forgot Password ?</h4>
             </div>
             <div class="modal-body">
                 <p>Enter your e-mail address below to reset your password.</p>
                 <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
             </div>
             <div class="modal-footer">
                 <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                 <button class="btn btn-theme" type="button">Submit</button>
             </div>
         </div>
     </div>
 </div>
 <!-- modal -->

 
<!-- Logout Delete Confirmation-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Apakah kamu yakin?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <!-- <span aria-hidden="true">×</span> -->
        </button>
      </div>
      <div class="modal-body">Data akan dihapus dan tidak bisa dikembalikan.</div>
      <div class="modal-footer">
        <button class="btn btn-primary btn-sm" type="button" data-dismiss="modal">Kembali</button>
        <a id="btn-delete" class="btn btn-danger btn-sm" href="#">Hapus</a>
      </div>
    </div>
  </div>
</div>
