
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model
{
    function check_session()
    {
        return $this->session->userdata('nik');
    }

    function getRowKaryawan($nik)
    {
        $query = $this->db->select('nik_karyawan, id_subt, id_pos')->where('nik_karyawan',$nik)->get('karyawan');

        return $query->num_rows();
        
    }

    function check_user($nik,$password)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('nik',$nik);   
        $this->db->where('password',$password);
        $this->db->limit(1);
        $query = $this->db->get();
        
        return $query;
    }
}