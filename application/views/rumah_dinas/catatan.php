<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker.css') ?>" />
  <style>
       tr {
        line-height:33px;
        
      }
      ol {
        line-height:140%;
        margin-top:5px;
      }
      td{
        vertical-align:top;
      }
      .ttd{
        text-align:center;
      }
      .kosong{
        height:80px;
      }
  </style>
</head>

<body>
  <section id="container">
   <!--main content start-->
      <div class="site-container">
        <div class="site-content">
          <section class="wrapper">
              <center><h1>CATATAN</h1></center>
              <table>
                <tr>
                  <td>Kepada</td>
                  <td>:</td>
                  <td>Satuan Layanan dan Administrasi</td>
                </tr>
                <tr>
                  <td>Dari</td>
                  <td>:</td>
                  <td>$nama</td>
                </tr>
                <tr>
                  <td>Perihal</td>
                  <td>:</td>
                  <td>Pemeliharaan Kendaraan Dinas No Pol : $nopol</td>
                </tr>
              </table>
              <hr><hr>
              <p>Dengan ini kami mengajukan permohonan untuk pemeliharaan kendaraan Dinas sebagai berikut:</p>
              $serivice
              <br><br>
              <p>Keterangan :</p>
              $keterangan
              <br><br>
              <p>Catatan :</p>
              <div class="form-group" style="margin-bottom:70px">  
                <div class="col-md-12">
                  <textarea class="form-control" rows="5" id="keterangan" name="keterangan"></textarea>
                </div>
              </div>
              <br><br><br><br><br>
              <table class="col-sm-12"><td style="text-align:right">Semarang, $tanggal</td></table>
              <br><br><br><br>
              <table class="ttd col-sm-12">
                <tr>
                  <td width="25%">Verificator</td>
                  <td width="50%"></td>
                  <td width="25%">Pengguna</td>
                </tr>
                <tr class="kosong">
                  <td width="25%"></td>
                  <td width="50%"></td>
                  <td width="25%"></td>
                </tr>
                <tr>
                  <td width="25%">$nama</td>
                  <td width="50%"></td>
                  <td width="25%">$nama</td>
                </tr>
              </table>
              <br><br><br>
              <table class="col-sm-12">
                <td align="center">Mengetahui,</td>
              </table>            
            </section>

        </div>
        <!--footer start-->
        <footer class="site-footer">
          <?php $this->load->view("partials/footer.php") ?>
        </footer>
        <!--footer end-->
      </div>
    <!--main content end-->
  </section>
</body>

</html>