<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker.css') ?>" />
</head>

<body>
  <section id="container">
    <!-- ******************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        ******************************************************************************************************************************************************* -->
    <!--header start-->
    <header class="header black-bg">
      <?php $this->load->view("partials/navbar.php") ?>
    </header>
    <!--header end-->
    <!-- ******************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        ******************************************************************************************************************************************************* -->
    <!--sidebar start-->
    <aside>
      <?php $this->load->view("partials/sidebar.php") ?>
    </aside>
    <!--sidebar end-->
    <!-- ******************************************************************************************************************************************************
        MAIN CONTENT
        ******************************************************************************************************************************************************* -->
    <!--main content start-->
    <section id="main-content">
      <div class="site-container">
        <div class="site-content">
          <section class="wrapper">
              <div class="table-responsive container col-sm-12" style="margin-top:20px">
                <h1>Data <small>Mesin</small>
                  
                </h1><br>
                <div class="col-lg-12 mt">
                  <div class="row content-panel">
                    <div class="panel-heading">
                      <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                          <a data-toggle="tab" href="#jenis_mesin" onclick="changeTab('jenis_mesin')">Jenis Mesin</a>
                        </li>
                        <li>
                          <a data-toggle="tab" href="#kerusakan_mesin" onclick="changeTab('kerusakan_mesin')">Kerusakan Mesin</a>
                        </li>
                        <!-- <li>
                          <a data-toggle="tab" href="#jenis_pekerjaan" onclick="changeTab('jenis_pekerjaan')">Jenis Pekerjaan</a>
                        </li> -->
                      </ul>
                    </div>
                    <input type="text" id="tab-active" class="hidden" value="">

                    <!-- /panel-heading -->
                    <div class="panel-body">
                      <div class="tab-content">
                        <div id="jenis_mesin" class="tab-pane active">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="pull-right"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_jenis_mesin_add_new"> Tambah Data</a></div>
                              <br><br><br>
                              <div class="alert alert-warning fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                  <i class="icon-remove"></i>
                                </button>
                                <strong>INFORMASI</strong><br> Pencarian data berdasarkan <b>jenis mesin</b>.
                              </div>
                              <table class="table table-bordered table-striped" id="jenis_mesin-table" width="100%" cellspacing="0">
                                <thead>
                                  <tr class="info">
                                    <th style="text-align:center">No</th>
                                    <th style="text-align:center">No Aset</th>
                                    <th style="text-align:center">Jenis Mesin</th>
                                    <th style="text-align:center">Keterangan</th>
                                    <th style="text-align:center">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tbody>
                              </table>
                              <!-- /detailed -->
                            </div>
                            <!-- /col-md-6 -->
                          </div>
                          <!-- /OVERVIEW -->
                        </div>
                        <!-- /tab-pane -->
                        <div id="kerusakan_mesin" class="tab-pane">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="pull-right"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_kerusakan_mesin_add_new"> Tambah Data</a></div>
                              <br><br><br>
                              <div class="alert alert-warning fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                  <i class="icon-remove"></i>
                                </button>
                                <strong>INFORMASI</strong><br> Pencarian data berdasarkan <b>kerusakan mesin</b>.
                              </div>
                              <table class="table table-bordered table-striped" id="kerusakan_mesin-table" width="100%" cellspacing="0">
                                <thead>
                                  <tr class="info">
                                    <th style="text-align:center">No</th>
                                    <th style="text-align:center">Kerusakan Mesin</th>
                                    <th style="text-align:center">Keterangan</th>
                                    <th style="text-align:center">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tbody>
                              </table>
                              <!-- /detailed -->
                            </div>
                            <!-- /col-md-6 -->
                          </div>
                          <!-- /row -->
                        </div>
                        <!-- /tab-pane -->
                        
                        <!-- <div id="jenis_pekerjaan" class="tab-pane">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="pull-right"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_jenis_pekerjaan_add_new"> Tambah Data</a></div>
                              <br><br><br>
                              <table class="table table-bordered table-striped" id="jenis_pekerjaan-table" width="100%" cellspacing="0">
                                <thead>
                                  <tr class="info">
                                    <th style="text-align:center">No</th>
                                    <th style="text-align:center">Jenis Pekerjaan</th>
                                    <th style="text-align:center">Keterangan</th>
                                    <th style="text-align:center">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div> -->


                        <!-- /tab-pane -->
                      </div>
                      <!-- /tab-content -->
                    </div>
                    <!-- /panel-body -->
                  </div>
                <!-- </div> -->
               
              </div>
          </section>

        </div>
        <button type="button" class="hidden" id="refresh"></button>
        <!--footer start-->
        <footer class="site-footer">
          <?php $this->load->view("partials/footer.php") ?>
        </footer>
        <!--footer end-->
      </div>
    </section>
    <!--main content end-->
  </section>

  <!-- ============ MODAL ADD BARANG =============== -->
  <div class="modal fade" id="modal_jenis_mesin_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Tambah Data Jenis Mesin</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/simpan_jenis_mesin' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">No Aset</label>
              <div class="col-xs-8">
                <input name="no_aset" id="no_aset" class="form-control" type="text" placeholder="No Aset" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Jenis Mesin</label>
              <div class="col-xs-8">
                <input name="jenis_mesin" id="jenis_mesin" class="form-control" type="text" placeholder="Jenis Mesin" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="jenis_mesin_keterangan" name="jenis_mesin_keterangan" placeholder="max. 255 karakter"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <!-- ============ MODAL ADD BARANG =============== -->
  <div class="modal fade" id="modal_kerusakan_mesin_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Tambah Data Kerusakan Mesin</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/simpan_kerusakan_mesin' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">Kerusakan Mesin</label>
              <div class="col-xs-8">
                <input name="kerusakan_mesin" id="kerusakan_mesin" class="form-control" type="text" placeholder="Kerusakan Mesin" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="kerusakan_mesin_keterangan" name="kerusakan_mesin_keterangan" placeholder="max. 255 karakter"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <!-- ============ MODAL EDIT BARANG =============== -->
  <div class="modal fade" id="modal_jenis_mesin_edit_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Edit Data Jenis Mesin</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/update_jenis_mesin' ?>">
          <div class="modal-body">

          
            <div class="form-group">
              <label class="control-label col-xs-3">No Aset</label>
              <div class="col-xs-8">
                <input name="edit_no_aset" id="edit_no_aset" class="form-control" type="text" placeholder="No Aset" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Jenis Mesin</label>
              <div class="col-xs-8">
                <input name="edit_jenis_mesin" id="edit_jenis_mesin" class="form-control" type="text" placeholder="Jenis Mesin" required>
              </div>
            </div>

            <input type="text" id="edit_jenis_mesin_id" class="form-control hidden" name="edit_jenis_mesin_id">

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="edit_jenis_mesin_keterangan" name="edit_jenis_mesin_keterangan" placeholder="max. 255 karakter"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->
  
  <!-- ============ MODAL EDIT BARANG =============== -->
  <div class="modal fade" id="modal_kerusakan_mesin_edit_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Edit Data Kerusakan Mesin</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'MasterData/update_kerusakan_mesin' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">Kerusakan Mesin</label>
              <div class="col-xs-8">
                <input name="edit_kerusakan_mesin" id="edit_kerusakan_mesin" class="form-control" type="text" placeholder="Kerusakan" required>
              </div>
            </div>

            <input type="text" id="edit_kerusakan_mesin_id" class="form-control hidden" name="id">

            <div class="form-group">
              <label class="control-label col-xs-3">Keterangan</label>
              <div class="col-xs-8">
                <textarea class="form-control " id="edit_kerusakan_mesin_keterangan" name="edit_kerusakan_mesin_keterangan" placeholder="max. 255 karakter"></textarea>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD BARANG-->

  <?php $this->load->view("partials/modal.php") ?>

  <!-- js placed at the end of the document so the pages load faster -->
  <?php $this->load->view("partials/js.php") ?>

<!-- date -->
<script src="<?php echo base_url('assets/lib/jquery-ui-1.9.2.custom.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js')?>"></script>
  <!-- <script src="<?php echo base_url('assets/lib/advanced-form-components.js')?>"></script> -->
  
  <script type="text/javascript">
    $(document).ready(function() {
      changeTab('jenis_mesin');
      changeTab('kerusakan_mesin');
      // changeTab('jenis_pekerjaan');
    });
    
    // $('#tabs-active').change(function()
    $('#tab-active').on('change',function()
    {
      var active =  $('#tab-active').val();

      if(active=='jenis_mesin'){
        tableJenisMesin();
      }
      else if(active=='kerusakan_mesin'){
        tableKerusakanMesin();
      }
      // else{
      //   tableJenisPekerjaan();
      // }
    });

    function changeTab(kode) {
      // console.log(kode);
      $('#tab-active').val(kode).trigger('change');
    }
    
    function tableJenisMesin(){
      
      $('#jenis_mesin-table').DataTable().destroy();
      $('#jenis_mesin-table tbody').empty();
      var table = $('#jenis_mesin-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "data_jenis_mesin",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "no_aset"},
          { "data": "jenis_mesin"},
          { "data": "keterangan", 'sortable': false},
          { "data": "action", 'sortable': false},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#jenis_mesin-table_filter input').unbind();
      
			var dtable = $('#jenis_mesin-table').dataTable().api();
      $('#jenis_mesin-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
						// Call the API search function
						dtable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtable.search("").draw();
					}
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#jenis_mesin-table').DataTable().ajax.reload();
      });
    }

    function tableKerusakanMesin(){
      $('#kerusakan_mesin-table').DataTable().destroy();
      $('#kerusakan_mesin-table tbody').empty();
      var table = $('#kerusakan_mesin-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "data_kerusakan_mesin",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "kerusakan_mesin"},
          { "data": "keterangan", 'sortable': false},
          { "data": "action", 'sortable': false},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#kerusakan_mesin-table_filter input').unbind();
      $('#kerusakan_mesin-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0) {
          table.search($(this).val()).draw();
        }
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#kerusakan_mesin-table').DataTable().ajax.reload();
      });
    }
    

    function edit_jenis_mesin(id) {
      $.ajax({
        type: "get",
        url: 'edit_jenis_mesin/'+id,
      })
      .done(function (response) {
          var result = JSON.parse(response)
          
          $('#edit_jenis_mesin_id').val(result.query.id);
          $('#edit_no_aset').val(result.query.no_aset);
          $('#edit_jenis_mesin').val(result.query.jenis_mesin);
          $('#edit_jenis_mesin_keterangan').val(result.query.keterangan);
          
          $("#modal_jenis_mesin_edit_new").modal('show');
          
      });
    }

    function edit_kerusakan_mesin(id) {
      $.ajax({
        type: "get",
        url: 'edit_kerusakan_mesin/'+id,
      })
      .done(function (response) {
          var result = JSON.parse(response)


          $('#edit_kerusakan_mesin_id').val(result.query.id);
          $('#edit_kerusakan_mesin').val(result.query.kerusakan_mesin);
          $('#edit_kerusakan_mesin_keterangan').val(result.query.keterangan);
          
          $("#modal_kerusakan_mesin_edit_new").modal('show');
          
      });
    }


    function delete_jenis_mesin(id){
      var url = '<?php echo site_url('masterdata/delete_jenis_mesin/') ?>'+id;
      // console.log(url);
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }

    function delete_kerusakan_mesin(id){
      var url = '<?php echo site_url('masterdata/delete_kerusakan_mesin/') ?>'+id;
      // console.log(url);
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }
  </script>

</body>

</html>