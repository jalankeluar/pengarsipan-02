<!-- Button trigger modal -->
<button id="alert_warning_login" type="button" class="btn btn-primary hidden" data-toggle="modal" data-target="#warningLogin">
  Warning!
</button>

<!-- Modal -->
<div class="modal fade bd-example-modal-sm" id="warningLogin" tabindex="-1" role="dialog" aria-labelledby="warningLoginLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #cf4a40;">
        <!-- <h2 class="modal-title" id="warningLoginLabel">Warning!</h2> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
       Username dan password tidak valid!
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<!-- <div class="modal fade bd-example-modal-sm" id="warningLogin" tabindex="-1" role="dialog" aria-labelledby="warningLoginModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      ...
    </div>
  </div>
</div> -->