<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker.css') ?>" />
</head>

<body>
  <section id="container">
    <!-- ******************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        ******************************************************************************************************************************************************* -->
    <!--header start-->
    <header class="header black-bg">
      <?php $this->load->view("partials/navbar.php") ?>
    </header>
    <!--header end-->
    <!-- ******************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        ******************************************************************************************************************************************************* -->
    <!--sidebar start-->
    <aside>
      <?php $this->load->view("partials/sidebar.php") ?>
    </aside>
    <!--sidebar end-->
    <!-- ******************************************************************************************************************************************************
        MAIN CONTENT
        ******************************************************************************************************************************************************* -->
    <!--main content start-->
    <section id="main-content">
      <div class="site-container">
        <div class="site-content">
          <section class="wrapper">
              <div class="table-responsive container col-sm-12" style="margin-top:20px">
                <h1>Data <small>Users</small>
                  <div class="pull-right"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_add_new"> Tambah User</a></div>
                </h1><br>
                <table class="table table-bordered table-striped" id="users-table" width="100%" cellspacing="0">
                  <thead>
                    <tr class="info">
                    <th style="text-align:center">No</th>
                      <th style="text-align:center">Nik</th>
                      <th style="text-align:center">Nama</th>
                      <th style="text-align:center">Email</th>
                      <th style="text-align:center">Role</th>
                      <th style="text-align:center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
          </section>

        </div>
        <button type="button" class="hidden" id="refresh"></button>
        <!--footer start-->
        <footer class="site-footer">
          <?php $this->load->view("partials/footer.php") ?>
        </footer>
        <!--footer end-->
      </div>
    </section>
    <!--main content end-->
  </section>

  <!-- ============ MODAL ADD BARANG =============== -->
  <div class="modal fade" id="modal_add_new" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Tambah User</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'Users/simpan' ?>">
          <div class="modal-body">

            <div class="form-group">
              <label class="control-label col-xs-3">Nik</label>
              <div class="col-xs-8">
                <input name="nik" id="nik" class="form-control" type="text" placeholder="NIK" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Nama</label>
              <div class="col-xs-8">
                <input name="name" id="name" class="form-control" type="text" placeholder="Nama" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Email</label>
              <div class="col-xs-8">
                <input name="email" id="email" class="form-control" type="text" placeholder="Email" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Password</label>
              <div class="col-xs-8">
                <input name="password" id="password" class="form-control" type="password" placeholder="Password" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Role</label>
              <div class="col-xs-8">
                <select class="form-control" style="width: 100%" name="role" id="role" required>
                  <option value="">Pilih Role</option>
                  <option value="super_admin">Super Admin</option>
                  <option value="admin">Admin</option>
                  <option value="driver">Driver</option>
                  <option value="mechanical_engineer">Mechanical Engineer</option>
                  <option value="safety">Safety</option>
                  <option value="penghuni_rumah">Penghuni Rumah Dinas</option>
                </select>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL ADD Users-->

  <!-- ============ MODAL EDIT users =============== -->
  <div class="modal fade" id="modal_edit_users" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Edit User</h3>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url() . 'Users/update_users' ?>">
          <div class="modal-body">

            <input type="text" id="edit_users_id" class="form-control hidden" name="edit_users_id">

            <div class="form-group">
              <label class="control-label col-xs-3">Nik</label>
              <div class="col-xs-8">
                <input name="edit_nik" id="edit_nik" class="form-control" type="text" placeholder="Nik..." required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Nama</label>
              <div class="col-xs-8">
                <input name="edit_name" id="edit_name" class="form-control" type="text" placeholder="Nama..." required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Email</label>
              <div class="col-xs-8">
                <input name="edit_email" id="edit_email" class="form-control" type="text" placeholder="Email..." required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Password</label>
              <div class="col-xs-8">
                <input name="edit_password" id="edit_password" class="form-control" type="text" placeholder="Password" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-3">Role</label>
              <div class="col-xs-8">
                <select class="form-control" name="edit_role" style="width: 100%" id="edit_role">
                  <option value="super_admin">Super Admin</option>
                  <option value="admin">Admin</option>
                  <option value="driver">Driver</option>
                  <option value="mechanical_engineer">Mechanical Engineer</option>
                  <option value="safety">Safety</option>
                  <option value="penghuni_rumah">Penghuni Rumah Dinas</option>
                </select>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button class="btn btn-info">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END MODAL Edit Users-->

  <?php $this->load->view("partials/modal.php") ?>
  <!-- js placed at the end of the document so the pages load faster -->
  <?php $this->load->view("partials/js.php") ?>

<!-- date -->
<!-- <script src="<?php echo base_url('assets/lib/jquery-ui-1.9.2.custom.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js')?>"></script>
  <script src="<?php echo base_url('assets/lib/advanced-form-components.js')?>"></script> -->
  
  <script type="text/javascript">
    $(document).ready(function() {
      $('#role').select2({
        placeholder: 'Pilih Role'
      });
  
      $('#edit_role').select2({
        placeholder: 'Pilih Role'
      });

      var table = $('#users-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "Users/data_users",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "nik"},
          { "data": "name"},
          { "data": "email"},
          { "data": "role", 'sortable': false},
          { "data": "action", 'sortable': false},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#users-table_filter input').unbind();
      var dtable = $('#users-table').dataTable().api();
      $('#users-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
						// Call the API search function
						dtable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtable.search("").draw();
					}
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#users-table').DataTable().ajax.reload();
      });

     
    });
     // EDIT USERS
      function edit_users(id){
        $.ajax({
          type: "get",
          url: 'users/edit_users/'+id,
        })
        .done(function (response) {
            var result = JSON.parse(response)
            
            $('#edit_users_id').val(result.query.id);
            $('#edit_nik').val(result.query.nik);
            $('#edit_name').val(result.query.name);
            $('#edit_email').val(result.query.email);
            $('#edit_password').val(result.query.password);
            $('#edit_role').val(result.query.role);
            
            $("#modal_edit_users").modal('show');
            
        });
      }

      function delete_users(id){
        var url = '<?php echo site_url('users/delete_users/') ?>'+id;
        // console.log(url);
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
      }

  </script>

</body>

</html>