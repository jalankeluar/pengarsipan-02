<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker.css') ?>" />
</head>

<body>
  <section id="container">
    <!-- ******************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        ******************************************************************************************************************************************************* -->
    <!--header start-->
    <header class="header black-bg">
      <?php $this->load->view("partials/navbar.php") ?>
    </header>
    <!--header end-->
    <!-- ******************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        ******************************************************************************************************************************************************* -->
    <!--sidebar start-->
    <aside>
      <?php $this->load->view("partials/sidebar.php") ?>
    </aside>
    <!--sidebar end-->
    <!-- ******************************************************************************************************************************************************
        MAIN CONTENT
        ******************************************************************************************************************************************************* -->
    <!--main content start-->
    <section id="main-content">
      <div class="site-container">
        <div class="site-content">
          <section class="wrapper">
              <div class="table-responsive container col-sm-12" style="margin-top:20px">
                <h1>Report <small>Service Kendaraan</small></h1>
                <br>
                <div class="panel">
                  <div class="panel-body">
                    <!-- <div class="form-group col-md-12 "> -->
                      <div class="alert alert-warning fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon-remove"></i>
                        </button>
                        <strong>INFORMASI</strong><br> Input Tanggal Awal <b>harus</b> lebih kecil dari Tanggal Akhir
                      </div>
                    <!-- </div> -->

                    <div class="row justify-content-center col-md-4">
                      <div class="form-group col-md-12">
                        <!-- <label class="control-label col-xs-3">No. Polisi</label> -->
                        <div class="col-xs-12">
                          <input type="text" class="form-control" name="nopol" id="nopol" placeholder="No. polisi">
                        </div>
                      </div>
                    </div>`
                    <div class="col-md-8">
                      <div class="form-group col-md-6">
                        <label class="control-label col-xs-3">Tanggal Mulai</label>
                        <div class="col-xs-8">
                          <input type="text" class="form-control input-tanggal" name="tanggal1" id="tanggal1" readonly>
                          <span class="input-group-btn add-on">
                            <button class="btn btn-theme" type="button"><i class="fa fa-calendar"></i></button>
                          </span>
                        </div>
                      </div>
                      <div class="form-group col-md-6">
                        <label class="control-label col-xs-3">Tanggal Akhir</label>
                        <div class="col-xs-8">
                          <input type="text" class="form-control input-tanggal" name="tanggal2" id="tanggal2" readonly>
                          <span class="input-group-btn add-on">
                            <button class="btn btn-theme" type="button"><i class="fa fa-calendar"></i></button>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group col-md-12">
                      <a class="btn btn-sm btn-success col-md-12" id="pencarian"> Pencarian</a>
                      <br><br>
                      <a class="btn btn-sm btn-info col-md-12" id="laporan"> Export Excel</a>
                    </div>
                  </div>
                </div>

                <table class="table table-bordered table-striped" id="service-table" width="100%" cellspacing="0">
                  <thead>
                    <tr class="info">
                      <th style="text-align:center">No</th>
                      <th style="text-align:center">Tanggal Pengajuan</th>
                      <th style="text-align:center">No Polisi</th>
                      <th style="text-align:center">Service</th>
                      <th style="text-align:center">Harga</th>
                      <th style="text-align:center">Bengkel</th>
                      <th style="text-align:center">Keterangan</th>
                      <th style="text-align:center">Diajukan Oleh</th>
                      <th style="text-align:center">Status Berkas</th>
                      <th style="text-align:center">Status Service</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
          </section>

        </div>
        <button type="button" class="hidden" id="refresh"></button>
        <!--footer start-->
        <footer class="site-footer">
          <?php $this->load->view("partials/footer.php") ?>
        </footer>
        <!--footer end-->
      </div>
    </section>
    <!--main content end-->
  </section>

  <!-- js placed at the end of the document so the pages load faster -->
  <?php $this->load->view("partials/js.php") ?>
  
  <script type="text/javascript">
    $(document).ready(function() {
      $('.input-tanggal').daterangepicker({
        format          : "dd-mm-yyyy",
        singleDatePicker: true,
        autoApply       : true,
        todayHighlight  : true,
        locale: {
          format: "DD-MM-YYYY",
          // separator: " - ",
        }
      });

      var table = $('#service-table').DataTable({
        "processing": true,
        "serverSide": true,
        "searching": false,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "dataType": "json",
          "type": "POST",
          "url" : "data_report_service",
          "data": function(d) {
              return $.extend({}, d, {
                  "tanggal1": $('#tanggal1').val(),
                  "tanggal2": $('#tanggal2').val(),
                  "nopol": $('#nopol').val(),
              });
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "pengajuan_date", "className": "text-center"},
          { "data": "no_polisi", "className": "text-center"},
          { "data": "service"},
          { "data": "harga", "className": "text-right"},
          { "data": "bengkel", "className": "text-right"},
          { "data": "keterangan", 'sortable': false},
          { "data": "name"},
          { "data": "status", 'sortable': false},
          { "data": "status_service", 'sortable': false},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#service-table_filter input').unbind();
      $('#service-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0) {
          table.search($(this).val()).draw();
        }
      });
      $('#refresh').bind('click', function() {
        $('#service-table').DataTable().ajax.reload();
      });
      
      var dtable = $('#service-table').dataTable().api();
      $('#pencarian').click(function() {
          dtable.draw();
      });

      $('#laporan').click(function() {
        var tanggal1 = $('#tanggal1').val();
        var tanggal2 = $('#tanggal2').val();
        var nopol = $('#nopol').val();
        var base_url = "<?php echo base_url();?>";
        window.open(base_url+'Kendaraan/export_report_service?tanggal1='+tanggal1+'&tanggal2='+tanggal2+'&nopol='+nopol,'_blank');
      });

    });
  </script>
  <script> 
    function onlyNumberKey(evt) { 
          
        // Only ASCII charactar in that range allowed 
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode 
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57)) 
            return false; 
        return true; 
    } 
</script> 

</body>

</html>