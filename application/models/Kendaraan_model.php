<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Kendaraan_model extends CI_Model
{

  private $_table = "kendaraan";
  private $_table2 = "spbu";

  public function view()
  {
    return $this->db->query('SELECT * from kendaraan')->result();
  }

  public function save($data)
  {
    $this->db->insert($this->_table, $data);
  }

  public function update($where, $data, $_table)
  {
    $this->db->where($where);
    $this->db->update($_table, $data);
  }

  public function delete($id)
  {
    return $this->db->delete($this->_table, array("sub_id" => $id));
  }

  // Service
  public function all_kendaraan_count($role)
  {
    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->get('kendaraan');
    }
    else{
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->where('created_by',$nik)
        ->get('kendaraan');
    }

    return $query->num_rows();
  }

  public function all_kendaraan_data($limit, $start, $col, $dir, $role)
  {
    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('kendaraan.deleted_at',null)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('k_service','k_service.id = kendaraan.service','left')
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
        ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis')
        ->get('kendaraan');
    }
    else{
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('kendaraan.deleted_at',null)
        ->where('kendaraan.created_by',$nik)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('k_service','k_service.id = kendaraan.service','left')
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
        ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis')
        ->get('kendaraan');
    }
    
    //atau

    // $query = $this->db->query("SELECT k.*, ks.servis from kendaraan k
    // left join k_service ks on ks.id = k.service
    // where deleted_at is null
    // order by $col $dir limit $limit $start ");


    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_kendaraan_count($search, $role)
  {
    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->like('k_nopolisi.no_polisi', $search)
        ->or_like('servis', $search)
        ->where('kendaraan.deleted_at',null)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('k_service','k_service.id = kendaraan.service','left')
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
        ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis')
        ->get('kendaraan');
    }
    else{
      $query = $this
      ->db
      ->like('k_nopolisi.no_polisi', $search)
      ->or_like('servis', $search)
      ->where('kendaraan.deleted_at',null)
      ->where('kendaraan.created_by',$nik)
      ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
      ->join('k_service','k_service.id = kendaraan.service','left')
      ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
      ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis')
      ->get('kendaraan');
    }

    return $query->num_rows();
  }

  public function search_kendaraan_data($limit, $start, $col, $dir, $search, $role)
  {
    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->like('k_nopolisi.no_polisi', $search)
        ->or_like('servis', $search)
        ->where('kendaraan.deleted_at',null)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('k_service','k_service.id = kendaraan.service','left')
        
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
        ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis')
        ->get('kendaraan');
    }
    else{
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->like('k_nopolisi.no_polisi', $search)
        ->or_like('servis', $search)
        ->where('kendaraan.deleted_at',null)
        ->where('kendaraan.created_by',$nik)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('k_service','k_service.id = kendaraan.service','left')
        
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
        ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis')
        ->get('kendaraan');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  // No Polisi master data
  public function all_nopolisi_count()
  {
    $query = $this
      ->db      
      ->where('deleted_at',null)
      ->get('k_nopolisi');

    return $query->num_rows();
  }

  public function all_nopolisi_data($limit, $start, $col, $dir)
  {
    $query = $this
      ->db
      ->where('deleted_at',null)
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->get('k_nopolisi');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_nopolisi_count($search)
  {
    $query = $this
      ->db
      ->where('deleted_at',null)
      ->like('no_polisi', $search)
      ->or_like('tahun', $search)
      ->or_like('type', $search)
      ->get('k_nopolisi');

    return $query->num_rows();
  }

  public function search_nopolisi_data($limit, $start, $col, $dir, $search)
  {
    $query = $this
      ->db
      ->where('deleted_at',null)
      ->like('no_polisi', $search)
      ->or_like('tahun', $search)
      ->or_like('type', $search)
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->get('k_nopolisi');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }
  
  // Servis Master Data
  public function all_servis_count()
  {
    $query = $this
      ->db
      ->where('deleted_at',null)
      ->get('k_service');

    return $query->num_rows();
  }

  public function all_servis_data($limit, $start, $col, $dir)
  {
    $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('k_service');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_servis_count($search)
  {
    $query = $this
      ->db
      ->like('servis', $search)
      ->where('deleted_at',null)
      ->get('k_service');

    return $query->num_rows();
  }

  public function search_servis_data($limit, $start, $col, $dir, $search)
  {
    $query = $this
      ->db
      ->like('servis', $search)
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('k_service');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  // Bahan bakar
  public function all_bahan_bakar_count($role)
  {
    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->get('k_bahan_bakar');
    }
    else{
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->where('created_by',$nik)
        ->get('k_bahan_bakar');
    }

    return $query->num_rows();
  }

  public function all_bahan_bakar_data($limit, $start, $col, $dir, $role)
  {
    // $query = $this->db->query("SELECT a.no_nota, b.no_polisi, a.tgl, a.spbu, a.harga, a.ttl_bayar, a.jml_liter, a.keterangan FROM pengarsipan.k_bahan_bakar a 
    // LEFT JOIN pengarsipan.k_nopolisi b on a.no_polisi=b.id
    // where a.deleted_at is null
    // order by $col $dir limit $limit $start ");

    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('k_bahan_bakar.deleted_at',null)
        ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
        ->select('k_bahan_bakar.no_nota, k_bahan_bakar.id, k_bahan_bakar.tgl, k_bahan_bakar.spbu, k_bahan_bakar.harga,k_bahan_bakar.ttl_bayar, k_bahan_bakar.jml_liter, k_bahan_bakar.created_at, k_nopolisi.no_polisi as nopol')
        // ->select('k_bahan_bakar.*, k_nopolisi.no_polisi as nopol')
        ->get('k_bahan_bakar');
    }
    else{
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('k_bahan_bakar.deleted_at',null)
        ->where('k_bahan_bakar.created_by',$nik)
        ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
        // ->select('k_bahan_bakar.*, k_nopolisi.no_polisi as nopol')
        ->select('k_bahan_bakar.no_nota, k_bahan_bakar.id, k_bahan_bakar.tgl, k_bahan_bakar.spbu, k_bahan_bakar.harga,k_bahan_bakar.ttl_bayar, k_bahan_bakar.jml_liter, k_bahan_bakar.created_at, k_nopolisi.no_polisi as nopol')
        ->get('k_bahan_bakar');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_bahan_bakar_count($search,$role)
  {
    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){ 
      $query = $this
        ->db
        ->like('no_nota', $search)
        ->or_like('k_nopolisi.no_polisi', $search)
        ->or_like('spbu', $search)
        ->where('k_bahan_bakar.deleted_at',null)
        ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
        ->select('k_bahan_bakar.no_nota, k_bahan_bakar.id, k_bahan_bakar.tgl, k_bahan_bakar.spbu, k_bahan_bakar.harga,k_bahan_bakar.ttl_bayar, k_bahan_bakar.jml_liter, k_bahan_bakar.created_at, k_nopolisi.no_polisi as nopol')
        ->get('k_bahan_bakar');
    }
    else{
      $query = $this
        ->db
        ->like('no_nota', $search)
        ->or_like('k_nopolisi.no_polisi', $search)
        ->or_like('spbu', $search)
        ->where('k_bahan_bakar.deleted_at',null)
        ->where('k_bahan_bakar.created_by',$nik)
        ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
        ->select('k_bahan_bakar.no_nota, k_bahan_bakar.id, k_bahan_bakar.tgl, k_bahan_bakar.spbu, k_bahan_bakar.harga,k_bahan_bakar.ttl_bayar, k_bahan_bakar.jml_liter, k_bahan_bakar.created_at, k_nopolisi.no_polisi as nopol')
        ->get('k_bahan_bakar');
    }

    return $query->num_rows();
  }

  public function search_bahan_bakar_data($limit, $start, $col, $dir, $search, $role)
  { 
    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){ 
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->like('no_nota', $search)
        ->or_like('k_nopolisi.no_polisi', $search)
        ->or_like('spbu', $search)
        ->where('k_bahan_bakar.deleted_at',null)
        ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
        ->select('k_bahan_bakar.no_nota, k_bahan_bakar.id, k_bahan_bakar.tgl, k_bahan_bakar.spbu, k_bahan_bakar.harga,k_bahan_bakar.ttl_bayar, k_bahan_bakar.jml_liter, k_bahan_bakar.created_at, k_nopolisi.no_polisi as nopol')
        ->get('k_bahan_bakar');
    }
    else{
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->like('no_nota', $search)
        ->or_like('k_nopolisi.no_polisi', $search)
        ->or_like('spbu', $search)
        ->where('k_bahan_bakar.deleted_at',null)
        ->where('k_bahan_bakar.created_by',$nik)
        ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
        ->select('k_bahan_bakar.no_nota, k_bahan_bakar.id, k_bahan_bakar.tgl, k_bahan_bakar.spbu, k_bahan_bakar.harga,k_bahan_bakar.ttl_bayar, k_bahan_bakar.jml_liter, k_bahan_bakar.created_at, k_nopolisi.no_polisi as nopol')
        ->get('k_bahan_bakar');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function getNopol()
  {
      return $this->db->query('SELECT * from k_nopolisi where deleted_at is null')->result();
  }

  public function getService()
  {
      return $this->db->query('SELECT * from k_service where deleted_at is null')->result();
  }

  public function getBengkel()
  {
      return $this->db->query('SELECT * from bengkel where deleted_at is null')->result();
  }

  public function getService_id($id)
  {
      return $this->db->query("SELECT * from k_service where id='$id' and deleted_at is null")->row();
  }

  public function all_report_service_count($awal,$akhir,$nopol)
  {
    if($nopol==null){
      if($awal==$akhir){
        $query = $this
          ->db
          ->where('deleted_at',null)
          ->where('date(kendaraan.pengajuan_date)', $awal)
          ->get('kendaraan');
      }
      else{
        $query = $this
          ->db
          ->where('deleted_at',null)
          ->where('kendaraan.pengajuan_date >=', $awal)
          ->where('kendaraan.pengajuan_date <=', $akhir)
          ->get('kendaraan');
      }
    }
    else{
      if($awal==$akhir){
        $query = $this
          ->db
          ->where('kendaraan.deleted_at',null)
          ->where('date(kendaraan.pengajuan_date)', $awal)
          ->like('k_nopolisi.no_polisi', $nopol)
          ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
          ->get('kendaraan');
      }
      else{
        $query = $this
          ->db
          ->where('kendaraan.deleted_at',null)
          ->where('kendaraan.pengajuan_date >=', $awal)
          ->where('kendaraan.pengajuan_date <=', $akhir)
          ->like('k_nopolisi.no_polisi', $nopol)
          ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
          ->get('kendaraan');
      }
    }

    return $query->num_rows();
  }

  public function all_report_service_data($limit, $start, $col, $dir, $awal, $akhir, $nopol)
  {
    if($nopol==null){
      if($awal==$akhir){
        $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('kendaraan.deleted_at',null)
        ->where('date(kendaraan.pengajuan_date)', $awal)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('k_service','k_service.id = kendaraan.service','left')
        
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
        ->join('users','users.nik = kendaraan.created_by','left')
        ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis, users.name')
        ->get('kendaraan');
  
      }
      else{
        $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('kendaraan.deleted_at',null)
        ->where('kendaraan.pengajuan_date >=', $awal)
        ->where('kendaraan.pengajuan_date <=', $akhir)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('k_service','k_service.id = kendaraan.service','left')
        
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
        ->join('users','users.nik = kendaraan.created_by','left')
        ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis, users.name')
        ->get('kendaraan');
      }
    }
    else{
      if($awal==$akhir){
        $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('kendaraan.deleted_at',null)
        ->where('date(kendaraan.pengajuan_date)', $awal)
        ->like('k_nopolisi.no_polisi', $nopol)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('k_service','k_service.id = kendaraan.service','left')
        
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
        ->join('users','users.nik = kendaraan.created_by','left')
        ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis, users.name')
        ->get('kendaraan');
  
      }
      else{
        $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('kendaraan.deleted_at',null)
        ->where('kendaraan.pengajuan_date >=', $awal)
        ->where('kendaraan.pengajuan_date <=', $akhir)
        ->like('k_nopolisi.no_polisi', $nopol)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('k_service','k_service.id = kendaraan.service','left')
        
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
        ->join('users','users.nik = kendaraan.created_by','left')
        ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis, users.name')
        ->get('kendaraan');
      }
    }
    
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_report_service_count($search, $awal, $akhir)
  {
    if($awal==$akhir){
      $query = $this
      ->db
      ->like('nopol', $search)
      ->or_like('servis', $search)
      ->or_like('name', $search)
      ->where('date(kendaraan.pengajuan_date)', $awal)
      ->where('kendaraan.deleted_at',null)
      ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
      ->join('k_service','k_service.id = kendaraan.service','left')
      
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
      ->join('users','users.nik = kendaraan.created_by','left')
      ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis, users.name')
      ->get('kendaraan');
    }
    else{
      $query = $this
        ->db
        ->like('nopol', $search)
        ->or_like('servis', $search)
        ->or_like('name', $search)
        ->where('kendaraan.pengajuan_date >=', $awal)
        ->where('kendaraan.pengajuan_date <=', $akhir)
        ->where('kendaraan.deleted_at',null)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('k_service','k_service.id = kendaraan.service','left')
        
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
        ->join('users','users.nik = kendaraan.created_by','left')
        ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis, users.name')
        ->get('kendaraan');
    }
  

    return $query->num_rows();
  }

  public function search_report_service_data($limit, $start, $col, $dir, $search, $awal, $akhir)
  {
    if($awal==$akhir){
      $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->like('nopol', $search)
      ->or_like('servis', $search)
      ->or_like('name', $search)
      ->where('kendaraan.deleted_at',null)
      ->where('date(kendaraan.pengajuan_date)', $awal)
      ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
      ->join('k_service','k_service.id = kendaraan.service','left')
      
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
      ->join('users','users.nik = kendaraan.created_by','left')
      ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis, users.name')
      ->get('kendaraan');
    }
    else{
      $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->like('nopol', $search)
      ->or_like('servis', $search)
      ->or_like('name', $search)
      ->where('kendaraan.deleted_at',null)
      ->where('kendaraan.pengajuan_date >=', $awal)
      ->where('kendaraan.pengajuan_date <=', $akhir)
      ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
      ->join('k_service','k_service.id = kendaraan.service','left')
      
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
      ->join('users','users.nik = kendaraan.created_by','left')
      ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis, users.name')
      ->get('kendaraan');
    }
    

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function export_report_service($awal, $akhir, $nopol)
  {
    if($nopol==null){
      if($awal==$akhir){
        $query = $this
          ->db
          ->order_by('kendaraan.pengajuan_date, nopol, name')
          ->where('kendaraan.deleted_at',null)
          ->where('date(kendaraan.pengajuan_date)', $awal)
          ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
          ->join('k_service','k_service.id = kendaraan.service','left')
          
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
          ->join('users','users.nik = kendaraan.created_by','left')
          ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis, users.name')
          ->get('kendaraan');
      }
      else{
        $query = $this
          ->db
          ->order_by('kendaraan.pengajuan_date, nopol, name')
          ->where('kendaraan.deleted_at',null)
          ->where('kendaraan.pengajuan_date >=', $awal)
          ->where('kendaraan.pengajuan_date <=', $akhir)
          ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
          ->join('k_service','k_service.id = kendaraan.service','left')
          
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
          ->join('users','users.nik = kendaraan.created_by','left')
          ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis, users.name')
          ->get('kendaraan');
      }
    }
    else{
      if($awal==$akhir){
        $query = $this
          ->db
          ->order_by('kendaraan.pengajuan_date, nopol, name')
          ->where('kendaraan.deleted_at',null)
          ->where('date(kendaraan.pengajuan_date)', $awal)
          ->like('k_nopolisi.no_polisi', $nopol)
          ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
          ->join('k_service','k_service.id = kendaraan.service','left')
          
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
          ->join('users','users.nik = kendaraan.created_by','left')
          ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis, users.name')
          ->get('kendaraan');
      }
      else{
        $query = $this
          ->db
          ->order_by('kendaraan.pengajuan_date, nopol, name')
          ->where('kendaraan.deleted_at',null)
          ->where('kendaraan.pengajuan_date >=', $awal)
          ->where('kendaraan.pengajuan_date <=', $akhir)
          ->like('k_nopolisi.no_polisi', $nopol)
          ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
          ->join('k_service','k_service.id = kendaraan.service','left')
          
        ->join('bengkel','bengkel.id = kendaraan.bengkel','left')
          ->join('users','users.nik = kendaraan.created_by','left')
          ->select('kendaraan.*, k_nopolisi.no_polisi as nopol, bengkel.nama, k_service.servis, users.name')
          ->get('kendaraan');
      }
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  
  public function all_report_bbm_count($awal,$akhir, $nopol)
  {
    if($nopol==null){
      if($awal==$akhir){
        $query = $this
        ->db
        ->where('k_bahan_bakar.deleted_at',null)
        ->where('k_bahan_bakar.tgl', $awal)
        ->get('k_bahan_bakar');
      }
      else{
        $query = $this
        ->db
        ->where('k_bahan_bakar.deleted_at',null)
        ->where('k_bahan_bakar.tgl >=', $awal)
        ->where('k_bahan_bakar.tgl <=', $akhir)
        ->get('k_bahan_bakar');
      }
    }
    else{
      if($awal==$akhir){
        $query = $this
        ->db
        ->where('k_bahan_bakar.deleted_at',null)
        ->where('k_bahan_bakar.tgl', $awal)
        ->like('k_nopolisi.no_polisi', $nopol)
        ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
        ->get('k_bahan_bakar');
      }
      else{
        $query = $this
        ->db
        ->where('k_bahan_bakar.deleted_at',null)
        ->where('k_bahan_bakar.tgl >=', $awal)
        ->where('k_bahan_bakar.tgl <=', $akhir)
        ->like('k_nopolisi.no_polisi', $nopol)
        ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
        ->get('k_bahan_bakar');
      }
    }

    return $query->num_rows();
  }

  public function all_report_bbm_data($limit, $start, $col, $dir, $awal, $akhir, $nopol)
  {
    if($nopol==null){
      if($awal==$akhir){
        $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
          ->where('k_bahan_bakar.deleted_at',null)
          ->where('date(k_bahan_bakar.tgl)', $awal)
          ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
          ->join('users','users.nik = k_bahan_bakar.created_by','left')
          ->select('k_bahan_bakar.*, k_nopolisi.no_polisi as nopol, users.name')
          ->get('k_bahan_bakar');
        }
      else{
        $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('k_bahan_bakar.deleted_at',null)
        ->where('k_bahan_bakar.tgl >=', $awal)
        ->where('k_bahan_bakar.tgl <=', $akhir)
        ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
        ->join('users','users.nik = k_bahan_bakar.created_by','left')
        ->select('k_bahan_bakar.*, k_nopolisi.no_polisi as nopol, users.name')
        ->get('k_bahan_bakar');
      }
    }
    else{
      if($awal==$akhir){
        $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('k_bahan_bakar.deleted_at',null)
        ->where('date(k_bahan_bakar.tgl)', $awal)
        ->like('k_nopolisi.no_polisi', $nopol)
          ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
          ->join('users','users.nik = k_bahan_bakar.created_by','left')
          ->select('k_bahan_bakar.*, k_nopolisi.no_polisi as nopol, users.name')
          ->get('k_bahan_bakar');
        }
      else{
        $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('k_bahan_bakar.deleted_at',null)
        ->where('k_bahan_bakar.tgl >=', $awal)
        ->where('k_bahan_bakar.tgl <=', $akhir)
        ->like('k_nopolisi.no_polisi', $nopol)
        ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
        ->join('users','users.nik = k_bahan_bakar.created_by','left')
        ->select('k_bahan_bakar.*, k_nopolisi.no_polisi as nopol, users.name')
        ->get('k_bahan_bakar');
      }
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_report_bbm_count($search, $awal, $akhir)
  {
      if($awal==$akhir){
        $query = $this
          ->db
          ->like('nopol', $search)
          ->or_like('name', $search)
          ->where('date(k_bahan_bakar.tgl)', $awal)
          ->where('k_bahan_bakar.deleted_at',null)
          ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
          ->join('users','users.nik = k_bahan_bakar.created_by','left')
          ->select('k_bahan_bakar.*, k_nopolisi.no_polisi as nopol, users.name')
          ->get('k_bahan_bakar');
      }
      else{
        $query = $this
          ->db
          ->like('nopol', $search)
          ->or_like('name', $search)
          ->where('k_bahan_bakar.tgl >=', $awal)
          ->where('k_bahan_bakar.tgl <=', $akhir)
          ->where('k_bahan_bakar.deleted_at',null)
          ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
          ->join('users','users.nik = k_bahan_bakar.created_by','left')
          ->select('k_bahan_bakar.*, k_nopolisi.no_polisi as nopol, users.name')
          ->get('k_bahan_bakar');
      }

    return $query->num_rows();
  }

  public function search_report_bbm_data($limit, $start, $col, $dir, $search, $awal, $akhir)
  {
      if($awal==$akhir){
        $query = $this
          ->db
          ->limit($limit, $start)
          ->order_by($col, $dir)
          ->like('nopol', $search)
          ->or_like('name', $search)
          ->where('k_bahan_bakar.deleted_at',null)
          ->where('date(k_bahan_bakar.tgl)', $awal)
          ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
          ->join('users','users.nik = k_bahan_bakar.created_by','left')
          ->select('k_bahan_bakar.*, k_nopolisi.no_polisi as nopol, users.name')
          ->get('k_bahan_bakar');
      }
      else{
        $query = $this
          ->db
          ->limit($limit, $start)
          ->order_by($col, $dir)
          ->like('nopol', $search)
          ->or_like('name', $search)
          ->where('k_bahan_bakar.deleted_at',null)
          ->where('k_bahan_bakar.tgl >=', $awal)
          ->where('k_bahan_bakar.tgl <=', $akhir)
          ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
          ->join('users','users.nik = k_bahan_bakar.created_by','left')
          ->select('k_bahan_bakar.*, k_nopolisi.no_polisi as nopol, users.name')
          ->get('k_bahan_bakar');
      }


    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function export_report_bbm($awal, $akhir, $nopol)
  {
    if($nopol==null){
      if($awal==$akhir){
        $query = $this
          ->db
          ->order_by('k_bahan_bakar.tgl, nopol, name')
          ->where('k_bahan_bakar.deleted_at',null)
          ->where('date(k_bahan_bakar.tgl)', $awal)
          ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
          ->join('users','users.nik = k_bahan_bakar.created_by','left')
          ->select('k_bahan_bakar.*, k_nopolisi.no_polisi as nopol, users.name')
          ->get('k_bahan_bakar');
      }
      else{
        $query = $this
          ->db
          ->order_by('k_bahan_bakar.tgl, nopol, name')
          ->where('k_bahan_bakar.deleted_at',null)
          ->where('k_bahan_bakar.tgl >=', $awal)
          ->where('k_bahan_bakar.tgl <=', $akhir)
          ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
          ->join('users','users.nik = k_bahan_bakar.created_by','left')
          ->select('k_bahan_bakar.*, k_nopolisi.no_polisi as nopol, users.name')
          ->get('k_bahan_bakar');
      }
    }
    else{
      if($awal==$akhir){
        $query = $this
          ->db
          ->order_by('k_bahan_bakar.tgl, nopol, name')
          ->where('k_bahan_bakar.deleted_at',null)
          ->where('date(k_bahan_bakar.tgl)', $awal)
          ->like('k_nopolisi.no_polisi', $nopol)
          ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
          ->join('users','users.nik = k_bahan_bakar.created_by','left')
          ->select('k_bahan_bakar.*, k_nopolisi.no_polisi as nopol, users.name')
          ->get('k_bahan_bakar');
      }
      else{
        $query = $this
          ->db
          ->order_by('k_bahan_bakar.tgl, nopol, name')
          ->where('k_bahan_bakar.deleted_at',null)
          ->where('k_bahan_bakar.tgl >=', $awal)
          ->where('k_bahan_bakar.tgl <=', $akhir)
          ->like('k_nopolisi.no_polisi', $nopol)
          ->join('k_nopolisi','k_nopolisi.id = k_bahan_bakar.no_polisi','left')
          ->join('users','users.nik = k_bahan_bakar.created_by','left')
          ->select('k_bahan_bakar.*, k_nopolisi.no_polisi as nopol, users.name')
          ->get('k_bahan_bakar');
      }
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

    // SPBU master data
    public function all_spbu_count()
    {
      $query = $this
        ->db      
        ->where('deleted_at',null)
        ->get('spbu');
  
      return $query->num_rows();
    }
  
    public function all_spbu_data($limit, $start, $col, $dir)
    {
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->get('spbu');
  
      if ($query->num_rows() > 0) {
        return $query->result();
      } else {
        return null;
      }
    }
  
    public function search_spbu_count($search)
    {
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->like('spbu', $search)
        ->or_like('alamat', $search)
        ->or_like('keterangan', $search)
        ->get('spbu');
  
      return $query->num_rows();
    }
  
    public function search_spbu_data($limit, $start, $col, $dir, $search)
    {
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->like('spbu', $search)
        ->or_like('alamat', $search)
        ->or_like('keterangan', $search)
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->get('spbu');
  
      if ($query->num_rows() > 0) {
        return $query->result();
      } else {
        return null;
      }
    }

    // Bengkel master data
    public function all_bengkel_count()
    {
      $query = $this
        ->db      
        ->where('deleted_at',null)
        ->get('bengkel');
  
      return $query->num_rows();
    }
  
    public function all_bengkel_data($limit, $start, $col, $dir)
    {
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->get('bengkel');
  
      if ($query->num_rows() > 0) {
        return $query->result();
      } else {
        return null;
      }
    }
  
    public function search_bengkel_count($search)
    {
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->like('bengkel', $search)
        ->or_like('alamat', $search)
        ->or_like('keterangan', $search)
        ->get('bengkel');
  
      return $query->num_rows();
    }
  
    public function search_bengkel_data($limit, $start, $col, $dir, $search)
    {
      $query = $this
        ->db
        ->where('deleted_at',null)
        ->like('bengkel', $search)
        ->or_like('alamat', $search)
        ->or_like('keterangan', $search)
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->get('bengkel');
  
      if ($query->num_rows() > 0) {
        return $query->result();
      } else {
        return null;
      }
    }

}
