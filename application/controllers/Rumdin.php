<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Rumdin extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Rumdin_model');
		$this->load->model('Login_model');
	}

	public function index()
	{	
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {	
			$data = array(
				'alamat'             => $this->Rumdin_model->getAlamat(),
				'kerusakan'          => $this->Rumdin_model->getKerusakan(),
			);				
			$this->load->view('rumah_dinas/index',$data);
		} else {
			redirect('login');
		}

	}

	public function data_rumdin()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'nama',
			2 => 'alamat',
			3 => 'kerusakan'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$role = $this->session->userdata('role');
		$nik  = $this->session->userdata('nik');

		$totalData = $this->Rumdin_model->all_rumdin_count($role);

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Rumdin_model->all_rumdin_data($limit, $start, $order, $dir, $role);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Rumdin_model->search_rumdin_data($limit, $start, $order, $dir, $search, $role);

			$totalFiltered = $this->Rumdin_model->search_rumdin_count($search, $role);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp           = '"'.$value->id.'"';
				$nestedData['nama_penghuni'] = ucwords($value->nama_penghuni);
				$nestedData['alamat']        = ucwords($value->jalan);
				$nestedData['kerusakan']     = ucwords($value->kerusakan);
				$nestedData['keterangan']    = ucwords($value->keterangan);
				$nestedData['dibuat']        = ucwords($value->name);

				if($value->status == 'approved'){
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-success'>DISETUJUI</a>
					</center>";
				}
				elseif($value->status == 'cancel'){
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-danger'>DITOLAK</a>
					</center>";
				}
				else{
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-warning'>MENUNGGU</a>
					</center>";
				}
				// $nestedData['status']        = ucwords($value->status);
				
			
				if ($role == 'verifikator' || $role == 'admin' || $role == 'super_admin'){
					$nestedData['action']     = "<center>
					<a onclick='return update_rumdin($_temp)' class='btn btn-xs btn-default'><i class='fa fa-pencil'></i> Update</a>
					<a onclick='return delete_rumdin($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> 
					</center>";
					
					// <a onclick='return delete_rumdin($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a>
				}
				else{
					$nestedData['action']     = "<center>
					<a onclick='return delete_rumdin($_temp)' href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</a> </center>";

				}

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}


	public function add()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {					
			$this->load->view("rumah_dinas/input");
		} else {
			redirect('login');
		}
		
		
	}

	public function simpan()
	{
		
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {					
				
			$rumdin   = $this->Rumdin_model;
			$nik = $this->session->userdata('nik');
			$now = date('Y-m-d H:i:s');

			$data = array(
				'id'         => $this->uuid->v4(),
				'alamat'     => $this->input->post('alamat'),
				'kerusakan'  => $this->input->post('kerusakan'),
				'keterangan' => $this->input->post('keterangan'),
				'status'     => 'menunggu',
				'created_by' => $nik,
				'created_at' => $now,
			);

			$rumdin->save($data);

			$this->load->view("rumah_dinas/index", $data);
			redirect("rumdin");
		} else {
			redirect('login');
		}
		
		
	}

	public function edit_status($id)
	{
		$query = $this->db->query("SELECT * FROM rumah_dinas where id = '$id'")->row();

		$data = array(
			'query'     => $query,
		);
		echo json_encode($data);
	}

	public function update_status()
	{
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {
			$id   = $this->input->post('update_rumdin_id');
			$data = array(
				'status' => $this->input->post('update_status_rumdin')
			);
					
			$this->db->where('id', $id);
			$this->db->update('rumah_dinas', $data);
			redirect("rumdin");
		} else {

			redirect('login');
		}
		
	}

	public function delete_rumdin($id=null)
    {   
		$check_session = $this->session->userdata('nik');
		
		if ($check_session != null) {			
			if (!isset($id)) show_404();
			else{
				
				$nik = $this->session->userdata('nik');
				$now = date('Y-m-d H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('rumah_dinas', $data);
				
				redirect("rumdin");
			}
		} else {
			redirect('login');
		}
		
	}

	//======================================= REPORT RUMAH DINAS ==================================
	public function report_rumah_dinas()
	{
		$check_session = $this->session->userdata('nik');
		if ($check_session != null) {
			$this->load->view('rumah_dinas/report_rumah_dinas');
		} 
		else{
			redirect('login');
		}
		
	}

	public function data_report_rumah_dinas()
	{
		$columns = array(
			0 => 'created_at',
			1 => 'alamat',
			2 => 'nama_penghuni',
			3 => 'kerusakan',
			4 => 'created_at',
			5 => 'name',
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$tanggal1 = $this->input->post('tanggal1');
		$tanggal2 = $this->input->post('tanggal2');
		$awal     = ($tanggal1!=''?date('Y-m-d',strtotime($tanggal1)):date('Y-m-d'));
		$akhir    = ($tanggal2!=''?date('Y-m-d',strtotime($tanggal2)):date('Y-m-d'));
		
		$totalData = $this->Rumdin_model->all_report_rumah_dinas_count($awal, $akhir);

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Rumdin_model->all_report_rumah_dinas_data($limit, $start, $order, $dir, $awal, $akhir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Rumdin_model->search_report_rumah_dinas_data($limit, $start, $order, $dir, $search, $awal, $akhir);

			$totalFiltered = $this->Rumdin_model->search_report_rumah_dinas_count($search, $awal, $akhir);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp           = '"'.$value->id.'"';
				$nestedData['created_at']    = ucwords(date('d-m-Y',strtotime($value->created_at)));
				$nestedData['nama_penghuni'] = ucwords($value->nama_penghuni);
				$nestedData['alamat']        = ucwords($value->alamat);
				$nestedData['kerusakan']     = ucwords($value->kerusakan);
				$nestedData['name']          = ucwords($value->name);

				if($value->status == 'approved'){
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-success'>DISETUJUI</a>
					</center>";
				}
				elseif($value->status == 'cancel'){
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-danger'>DITOLAK</a>
					</center>";
				}
				else{
					$nestedData['status']     = "<center>
					<a class='btn btn-xs btn-warning'>MENUNGGU</a>
					</center>";
				}

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	 
    public function export_report_rumah_dinas(){
		// include APPPATH.'third_party/Classes/PHPsheet.php';
        
        // include $_SERVER["DOCUMENT_ROOT"].'/pengarsipan-02/application/third_party/Classes/PHPsheet/IOFactory.php';

		$tanggal1 = $this->input->get('tanggal1');
		$tanggal2 = $this->input->get('tanggal2');
		$dari     = ($tanggal1!=''?date('Y-m-d',strtotime($tanggal1)):date('Y-m-d'));
		$sampai   = ($tanggal2!=''?date('Y-m-d',strtotime($tanggal2)):date('Y-m-d'));

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();;

		$sheet->setCellValue('A1', "NO");
		$sheet->getColumnDimension('A')->setAutoSize(true); 

		$sheet->setCellValue('B1', "TANGGAL");
		$sheet->getColumnDimension('B')->setAutoSize(true); 

		$sheet->setCellValue('C1', "ALAMAT");
		$sheet->getColumnDimension('C')->setAutoSize(true); 

		$sheet->setCellValue('D1', "NAMA PENGHUNI");
		$sheet->getColumnDimension('D')->setAutoSize(true); 

		$sheet->setCellValue('E1', "KERUSAKAN");
		$sheet->getColumnDimension('E')->setAutoSize(true); 

		$sheet->setCellValue('F1', "DIAJUKAN OLEH (NIK)");
		$sheet->getColumnDimension('F')->setAutoSize(true); 

		$sheet->setCellValue('G1', "DIAJUKAN OLEH (NAMA)");
		$sheet->getColumnDimension('G')->setAutoSize(true); 
		
		$sheet->setCellValue('H1', "STATUS");
		$sheet->getColumnDimension('H')->setAutoSize(true); 

		$details = $this->Rumdin_model->export_report_rumah_dinas($dari,$sampai);

		$no     = 1;
		$numrow = 2;
		foreach ($details as $key => $detail) {

		  $sheet->setCellValue('A'.$numrow, $no);
		  $sheet->setCellValue('B'.$numrow, date('d-m-Y',strtotime($detail->created_at)));
		  $sheet->setCellValue('C'.$numrow, ucwords($detail->nama_penghuni));
          $sheet->setCellValue('D'.$numrow, ucwords($detail->alamat));
		  $sheet->setCellValue('E'.$numrow, ucwords($detail->kerusakan));
		  $sheet->setCellValue('F'.$numrow, ucwords($detail->name));
		  $sheet->setCellValue('G'.$numrow, ucwords($detail->created_by));
		  if($detail->status == 'approved'){
			$status = 'disetujui';
		  }
		  elseif($detail->status == 'cancel'){
			$status = 'ditolak';
		  }
		  else{
			$status = 'menunggu';
		  }
		  $sheet->setCellValue('H'.$numrow, ucwords($status));
		  $no++;
		  $numrow++;
		}

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		
		$sheet->getDefaultRowDimension()->setRowHeight(-1);

		$writer   = new Xlsx($spreadsheet);
		$filename = 'Laporan Rumah Dinas '.$tanggal1.' sd '.$tanggal2.' ';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
		
		ob_end_clean();+
		$writer->save('php://output');
    }
}
