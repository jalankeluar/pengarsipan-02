/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : pengarsipan

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 17/09/2020 21:15:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bengkel
-- ----------------------------
DROP TABLE IF EXISTS `bengkel`;
CREATE TABLE `bengkel`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `nama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `alamat` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bengkel
-- ----------------------------
INSERT INTO `bengkel` VALUES ('f85e8ed6-f410-4abd-ac4a-e5a19105e97c', 'aaa', 'a', '', '1111', '2020-09-10 20:25:24.865420', NULL, NULL);
INSERT INTO `bengkel` VALUES ('f85e8ed6-f410-4abd-ac4a-e5a19105e97c', 'x', 'a', '', '1111', '2020-09-07 11:44:43.584659', NULL, NULL);
INSERT INTO `bengkel` VALUES ('fc3cced6-4a36-44f4-a9d6-909acf649dd9', 's', 'd', '', '1111', '2020-09-07 11:44:38.000000', NULL, NULL);

-- ----------------------------
-- Table structure for g_kerusakan
-- ----------------------------
DROP TABLE IF EXISTS `g_kerusakan`;
CREATE TABLE `g_kerusakan`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `kerusakan_gedung` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of g_kerusakan
-- ----------------------------
INSERT INTO `g_kerusakan` VALUES ('18b132f3-b1f9-4da9-8c63-720979d5a07d', 'lantai rusak', 'betulin dong ok', '1111', '2020-08-02 16:11:47.000000', NULL, NULL);
INSERT INTO `g_kerusakan` VALUES ('cb5e2162-4a20-4e78-bddd-82b66636e411', 'pintu patah', 'betulin', '1111', '2020-08-02 16:12:02.000000', NULL, NULL);

-- ----------------------------
-- Table structure for g_lantai
-- ----------------------------
DROP TABLE IF EXISTS `g_lantai`;
CREATE TABLE `g_lantai`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `lantai` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of g_lantai
-- ----------------------------
INSERT INTO `g_lantai` VALUES ('60bc2214-0645-45ea-94b4-41c349cf9f4a', '1', 'ground', '1111', '2020-08-02 16:11:31.000000', NULL, NULL);
INSERT INTO `g_lantai` VALUES ('de98b13f-2102-473b-b68c-6538c7846133', '10', 'rooftop', '1111', '2020-07-28 14:57:32.000000', NULL, NULL);

-- ----------------------------
-- Table structure for gedung
-- ----------------------------
DROP TABLE IF EXISTS `gedung`;
CREATE TABLE `gedung`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `lantai` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `kerusakan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gedung
-- ----------------------------
INSERT INTO `gedung` VALUES ('0c0bd411-5993-4a15-9e42-5bf4bf411cc0', '60bc2214-0645-45ea-94b4-41c349cf9f4a', NULL, '', '', '2020-08-02 21:12:17.607031', NULL, NULL);
INSERT INTO `gedung` VALUES ('3dffab98-e1b5-45b4-a642-9d0a788f7e5e', 'de98b13f-2102-473b-b68c-6538c7846133', 'cb5e2162-4a20-4e78-bddd-82b66636e411', 'm', '1111', '2020-08-26 22:50:29.000000', NULL, NULL);
INSERT INTO `gedung` VALUES ('51e78a09-579a-4c83-ab37-560b03439a62', 'de98b13f-2102-473b-b68c-6538c7846133', '18b132f3-b1f9-4da9-8c63-720979d5a07d', '', '1111', '2020-08-04 05:40:26.000000', NULL, NULL);
INSERT INTO `gedung` VALUES ('67cc6eb8-87d7-479c-82a9-d6bb7918fcaf', '60bc2214-0645-45ea-94b4-41c349cf9f4a', '18b132f3-b1f9-4da9-8c63-720979d5a07d', '', '1111', '2020-08-04 05:40:18.000000', NULL, NULL);
INSERT INTO `gedung` VALUES ('7ea2f079-4b93-4e80-bdc8-e35664345ba3', 'de98b13f-2102-473b-b68c-6538c7846133', 'cb5e2162-4a20-4e78-bddd-82b66636e411', '', '1111', '2020-08-02 16:20:45.000000', NULL, NULL);
INSERT INTO `gedung` VALUES ('9e5fc9c3-a279-4cde-99c2-b453595acae4', '', '', '', '1111', '2020-08-02 16:20:50.000000', NULL, NULL);
INSERT INTO `gedung` VALUES ('d3092ab5-2c9e-4fdf-b2ec-1f846948afed', 'de98b13f-2102-473b-b68c-6538c7846133', '18b132f3-b1f9-4da9-8c63-720979d5a07d', '', '1111', '2020-08-02 16:21:15.000000', NULL, NULL);
INSERT INTO `gedung` VALUES ('ecabcc06-63c1-4a6f-9577-21ebf7366d8d', '60bc2214-0645-45ea-94b4-41c349cf9f4a', 'cb5e2162-4a20-4e78-bddd-82b66636e411', 'ajajjaja', '1111', '2020-08-04 05:54:43.000000', NULL, NULL);

-- ----------------------------
-- Table structure for k_bahan_bakar
-- ----------------------------
DROP TABLE IF EXISTS `k_bahan_bakar`;
CREATE TABLE `k_bahan_bakar`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `no_nota` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `no_polisi` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tgl` timestamp(6) NULL DEFAULT current_timestamp(6),
  `spbu` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `harga` int(13) NULL DEFAULT NULL,
  `ttl_bayar` int(100) NULL DEFAULT NULL,
  `jml_liter` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of k_bahan_bakar
-- ----------------------------
INSERT INTO `k_bahan_bakar` VALUES ('34c320e3-260b-436a-b53b-2c8d00880fdd', 'A123', '1', '2020-08-04 00:00:00.000000', 'pamularsih', 12333, 20000, '1.62166545', '1111', '2020-08-04 06:34:31.000000', NULL, NULL);
INSERT INTO `k_bahan_bakar` VALUES ('4fada61a-ce08-4535-bf7f-9ac7e4d62aec', 'W99', '6d4d77ee-d60f-419a-bbc5-5b1588b8897f', '2020-08-03 00:00:00.000000', 'pamularsih', 5000, 90000, '18', '1111', '2020-08-03 04:37:21.000000', NULL, NULL);
INSERT INTO `k_bahan_bakar` VALUES ('7da922f4-2454-4686-81f0-1e4a755f36a7', 'A123', '6d4d77ee-d60f-419a-bbc5-5b1588b8897f', '2020-08-04 00:00:00.000000', 'pamularsih', 5000, 90000, '18', '1111', '2020-08-03 04:37:35.000000', NULL, NULL);

-- ----------------------------
-- Table structure for k_nopolisi
-- ----------------------------
DROP TABLE IF EXISTS `k_nopolisi`;
CREATE TABLE `k_nopolisi`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `no_polisi` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pajak` date NULL DEFAULT NULL,
  `stnk` date NULL DEFAULT NULL,
  `tahun` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of k_nopolisi
-- ----------------------------
INSERT INTO `k_nopolisi` VALUES ('1', '1299', '2020-08-13', NULL, '2005asd', '123asd', '1111', '2020-07-01 23:31:33.000000', NULL, NULL);
INSERT INTO `k_nopolisi` VALUES ('6d4d77ee-d60f-419a-bbc5-5b1588b8897f', 'H 4553 IN', '2020-08-27', NULL, '2020', 'porsche', '1111', '2020-08-02 22:37:56.000000', NULL, NULL);

-- ----------------------------
-- Table structure for k_service
-- ----------------------------
DROP TABLE IF EXISTS `k_service`;
CREATE TABLE `k_service`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `servis` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of k_service
-- ----------------------------
INSERT INTO `k_service` VALUES ('123', '132sssssssssssssss', '12asdasd', '1111', '2020-07-01 23:31:50.353653', NULL, NULL);
INSERT INTO `k_service` VALUES ('1fe39840-10d7-4e9a-818c-ae09dea00e01', 'cuci mobil', 'halo', '1111', '2020-08-02 22:35:22.000000', NULL, NULL);
INSERT INTO `k_service` VALUES ('5cf3ed29-b14e-4ed4-b9be-d03186fcdd95', 'hal', 'la', '1111', '2020-08-04 08:26:44.000000', NULL, NULL);
INSERT INTO `k_service` VALUES ('63666b78-0833-4429-a6e7-cab4735ec82f', 'cuci mobil', '', '1111', '2020-07-03 14:14:04.000000', '1111', '2020-07-05 07:18:17.000000');

-- ----------------------------
-- Table structure for kendaraan
-- ----------------------------
DROP TABLE IF EXISTS `kendaraan`;
CREATE TABLE `kendaraan`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `no_polisi` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `service` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pengajuan_date` timestamp(6) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `bengkel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `harga` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status_service` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL,
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kendaraan
-- ----------------------------
INSERT INTO `kendaraan` VALUES ('123', '123', '123', '2020-06-27 13:14:20.000000', '1', NULL, '12', 'approved', 'done', '2020-06-27 13:14:32.094883', 'hussein', NULL, NULL);
INSERT INTO `kendaraan` VALUES ('42b96608-2149-4fb6-ba0c-fa5b8f70af29', '6d4d77ee-d60f-419a-bbc5-5b1588b8897f', '123,1fe39840-10d7-4e9a-818c-ae09dea00e01,', '2020-09-01 00:00:00.000000', '', NULL, '50000', 'cancel', NULL, '2020-09-01 19:55:03.000000', '5555', NULL, NULL);
INSERT INTO `kendaraan` VALUES ('62710a97-76aa-4c9e-872e-34821e483b77', '1', '123', '2020-07-03 00:00:00.000000', '', 'f85e8ed6-f410-4abd-ac4a-e5a19105e97c', '10000', 'approved', 'process', '2020-07-31 04:05:33.872106', '', NULL, NULL);
INSERT INTO `kendaraan` VALUES ('7913b9aa-1b51-459a-92c2-400bb61bec04', '1', '1', '2020-07-07 00:00:00.000000', 'asd', NULL, '12333', 'approved', 'done', '2020-07-07 21:58:03.908938', '', NULL, NULL);
INSERT INTO `kendaraan` VALUES ('82a10124-8953-4672-ad29-2cdad314c446', '6d4d77ee-d60f-419a-bbc5-5b1588b8897f', '1fe39840-10d7-4e9a-818c-ae09dea00e01,5cf3ed29-b14e-4ed4-b9be-d03186fcdd95,', '2020-09-17 00:00:00.000000', '', NULL, NULL, 'menunggu', '', '2020-09-17 20:43:41.000000', '1111', NULL, NULL);
INSERT INTO `kendaraan` VALUES ('8371d98b-4544-4743-ac2c-4a104c53d9bd', '1', '123', '2020-08-04 00:00:00.000000', '', NULL, '5000', NULL, NULL, '2020-08-02 20:41:48.583910', '', NULL, NULL);
INSERT INTO `kendaraan` VALUES ('970f8c96-1e6c-4825-9b05-9e7c4e972f43', '1', '123', '2020-07-10 00:00:00.000000', 'ma', '1fe39840-10d7-4e9a-818c-ae09dea00e01', '12333', 'approved', 'process', '2020-07-10 16:40:59.712100', '', NULL, NULL);
INSERT INTO `kendaraan` VALUES ('976d5c2e-203d-4742-8e44-12813aa90c80', '6d4d77ee-d60f-419a-bbc5-5b1588b8897f', '1fe39840-10d7-4e9a-818c-ae09dea00e01', '2020-08-03 00:00:00.000000', '', NULL, '5000', 'approved', NULL, '2020-08-03 01:47:54.000000', '1111', NULL, NULL);
INSERT INTO `kendaraan` VALUES ('9db07a89-92bc-4cf1-a7b7-98ba673ec10a', '6d4d77ee-d60f-419a-bbc5-5b1588b8897f', '1fe39840-10d7-4e9a-818c-ae09dea00e01,5cf3ed29-b14e-4ed4-b9be-d03186fcdd95,', '2020-08-29 00:00:00.000000', 'tes', NULL, '5000', 'approved', NULL, '2020-08-29 08:43:23.000000', '1111', NULL, NULL);
INSERT INTO `kendaraan` VALUES ('bfda8871-300e-43a7-8058-5a576a15dc76', '1', '123', '2020-08-02 00:00:00.000000', '', NULL, '5000', NULL, NULL, '2020-08-02 15:44:12.000000', '1111', NULL, NULL);
INSERT INTO `kendaraan` VALUES ('eb0319a1-552b-411f-b5c0-d99c923fb091', '6d4d77ee-d60f-419a-bbc5-5b1588b8897f', '123', '2020-08-05 00:00:00.000000', '', NULL, '5000', NULL, NULL, '2020-08-05 14:53:15.000000', '1111', NULL, NULL);
INSERT INTO `kendaraan` VALUES ('ec41b11e-c9be-49ec-81ce-9a8574794637', '6d4d77ee-d60f-419a-bbc5-5b1588b8897f', '1fe39840-10d7-4e9a-818c-ae09dea00e01', '2020-08-04 00:00:00.000000', 'asd', NULL, '10000', NULL, NULL, '2020-08-04 06:34:13.000000', '1111', NULL, NULL);
INSERT INTO `kendaraan` VALUES ('f0d5717a-a44f-4f97-8bf7-211c8da680b8', '6d4d77ee-d60f-419a-bbc5-5b1588b8897f', '123,1fe39840-10d7-4e9a-818c-ae09dea00e01,5cf3ed29-b14e-4ed4-b9be-d03186fcdd95,', '2020-09-01 00:00:00.000000', '', NULL, '70000', 'approved', NULL, '2020-09-01 20:08:44.000000', '5555', NULL, NULL);
INSERT INTO `kendaraan` VALUES ('f4b20d6e-f5e6-4929-8192-9a63f2ab59c4', '6d4d77ee-d60f-419a-bbc5-5b1588b8897f', '1fe39840-10d7-4e9a-818c-ae09dea00e01,', '2020-09-10 00:00:00.000000', '2', '123', NULL, 'approved', 'process', '2020-09-10 20:06:36.000000', '1111', NULL, NULL);

-- ----------------------------
-- Table structure for m_jenis_mesin
-- ----------------------------
DROP TABLE IF EXISTS `m_jenis_mesin`;
CREATE TABLE `m_jenis_mesin`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `jenis_mesin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `no_aset` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_jenis_mesin
-- ----------------------------
INSERT INTO `m_jenis_mesin` VALUES ('37efbfd9-6633-4339-83da-ebdfba1bf500', 'asd', 'jjjj', 'asd', '1111', '2020-08-27 12:47:43.000000', NULL, NULL);
INSERT INTO `m_jenis_mesin` VALUES ('e7d42a4e-e298-49da-9e4b-5459d0b46fa8', 'as', NULL, '', '1111', '2020-08-04 07:00:58.000000', '1111', '2020-08-04 07:01:37.000000');
INSERT INTO `m_jenis_mesin` VALUES ('f3b2745c-0d79-4312-9fed-eaee23cfee43', 'x', 'ss', '', '1111', '2020-08-04 07:01:33.000000', NULL, NULL);

-- ----------------------------
-- Table structure for m_kerusakan
-- ----------------------------
DROP TABLE IF EXISTS `m_kerusakan`;
CREATE TABLE `m_kerusakan`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `kerusakan_mesin` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_kerusakan
-- ----------------------------
INSERT INTO `m_kerusakan` VALUES ('654f9724-09ec-4c95-a562-06953a81d8c3', 'zzzz', 'asd', '1111', '2020-08-04 08:30:22.000000', NULL, NULL);
INSERT INTO `m_kerusakan` VALUES ('9930ffc2-4603-4643-92c1-4c195b20fdb8', 'husss', '', '1111', '2020-08-04 07:01:13.000000', NULL, NULL);
INSERT INTO `m_kerusakan` VALUES ('cb8e8f7d-f2c6-4f28-afaf-681dac86b58f', 'gg', '', '1111', '2020-08-04 07:01:27.000000', NULL, NULL);

-- ----------------------------
-- Table structure for mesin
-- ----------------------------
DROP TABLE IF EXISTS `mesin`;
CREATE TABLE `mesin`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `jenis_mesin` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `kerusakan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `harga` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `vendor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status_service` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mesin
-- ----------------------------
INSERT INTO `mesin` VALUES ('5b3137ed-b66b-4f14-a66c-f739629d1936', '37efbfd9-6633-4339-83da-ebdfba1bf500', 'cb8e8f7d-f2c6-4f28-afaf-681dac86b58f', '90', '', '04681d02-ba78-4494-b99e-763c941948af', 'approved', 'done', '1111', '2020-09-01 20:36:53.000000', NULL, NULL);
INSERT INTO `mesin` VALUES ('9780308b-94f0-49a8-bec3-32517deaabca', '37efbfd9-6633-4339-83da-ebdfba1bf500', '654f9724-09ec-4c95-a562-06953a81d8c3,cb8e8f7d-f2c6-4f28-afaf-681dac86b58f,', NULL, 'testing', NULL, 'menunggu', '', '1111', '2020-09-17 20:38:31.000000', NULL, NULL);
INSERT INTO `mesin` VALUES ('986503f7-4c0e-44e2-8dc3-7580a5044068', '37efbfd9-6633-4339-83da-ebdfba1bf500', '654f9724-09ec-4c95-a562-06953a81d8c3', '100', '', '04681d02-ba78-4494-b99e-763c941948af', 'approved', 'done', '1111', '2020-08-27 12:50:23.000000', NULL, NULL);
INSERT INTO `mesin` VALUES ('c74e24fe-6ea6-4e01-b22e-f2538963f568', 'f3b2745c-0d79-4312-9fed-eaee23cfee43', 'cb8e8f7d-f2c6-4f28-afaf-681dac86b58f', '9000', '', '7c02038f-28fc-4efa-82d9-2eb2c6d3336b', 'approved', 'done', '1111', '2020-09-01 20:37:11.000000', NULL, NULL);
INSERT INTO `mesin` VALUES ('dd4fbd51-5089-4b10-b707-06b4698f441b', 'f3b2745c-0d79-4312-9fed-eaee23cfee43', 'cb8e8f7d-f2c6-4f28-afaf-681dac86b58f', '1000', '', '04681d02-ba78-4494-b99e-763c941948af', 'approved', 'done', '1111', '2020-08-04 07:32:08.000000', NULL, NULL);

-- ----------------------------
-- Table structure for rd_alamat
-- ----------------------------
DROP TABLE IF EXISTS `rd_alamat`;
CREATE TABLE `rd_alamat`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `nama_penghuni` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rd_alamat
-- ----------------------------
INSERT INTO `rd_alamat` VALUES ('c5bafed3-2922-488d-87fe-2c9f30f32047', 'll', 'qwe', '', '1111', '2020-07-30 22:49:27.000000', '1111', '2020-08-05 13:56:34.000000');
INSERT INTO `rd_alamat` VALUES ('ce0bd047-eda7-4918-992a-d9252108ccae', 'we', 'pkealongan', 'we', '1111', '2020-08-05 14:32:07.000000', NULL, NULL);
INSERT INTO `rd_alamat` VALUES ('dd729cbc-17d2-49c4-8d70-9b16ea392638', 'll', 'pemalang', 'ad', '1111', '2020-08-05 14:31:57.000000', NULL, NULL);
INSERT INTO `rd_alamat` VALUES ('ebfe0347-8273-4e86-982e-3fbe0edcff04', 'mmasd', 'b', '', '1111', '2020-07-30 21:40:18.000000', '1111', '2020-08-05 13:57:54.000000');

-- ----------------------------
-- Table structure for rd_kerusakan
-- ----------------------------
DROP TABLE IF EXISTS `rd_kerusakan`;
CREATE TABLE `rd_kerusakan`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `kerusakan` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rd_kerusakan
-- ----------------------------
INSERT INTO `rd_kerusakan` VALUES ('36c7d08d-a91b-4268-a8b9-cf418a0adea5', 'asd', 'asdasd', '1111', '2020-07-30 21:39:57.000000', NULL, NULL);
INSERT INTO `rd_kerusakan` VALUES ('8199f485-dc42-4bfc-9828-91c1dc26d9b0', 'pop', 'pop\r\n', '1111', '2020-08-04 08:30:38.000000', '1111', '2020-08-04 08:30:43.000000');
INSERT INTO `rd_kerusakan` VALUES ('beb6da00-6c4b-4d3b-9607-58f3485df1ef', 's', '', '1111', '2020-07-30 21:40:36.000000', '1111', '2020-08-04 08:28:14.000000');

-- ----------------------------
-- Table structure for rumah_dinas
-- ----------------------------
DROP TABLE IF EXISTS `rumah_dinas`;
CREATE TABLE `rumah_dinas`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kerusakan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rumah_dinas
-- ----------------------------
INSERT INTO `rumah_dinas` VALUES ('016a7b85-eba1-48fd-b775-78614126fde2', 'dd729cbc-17d2-49c4-8d70-9b16ea392638', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', '', 'menunggu', '27', '2020-09-01 20:25:11.000000', NULL, NULL);
INSERT INTO `rumah_dinas` VALUES ('1f0e97a1-b673-43fd-b4be-41ed09f598c8', 'dd729cbc-17d2-49c4-8d70-9b16ea392638', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', 'kk', 'cancel', '1111', '2020-08-27 11:00:22.000000', NULL, NULL);
INSERT INTO `rumah_dinas` VALUES ('4b7a44c5-7ad2-422f-ba3b-d560426a3235', 'dd729cbc-17d2-49c4-8d70-9b16ea392638', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', NULL, NULL, '1111', '2020-08-05 14:32:36.000000', NULL, NULL);
INSERT INTO `rumah_dinas` VALUES ('69a6d2b7-0df0-4ada-9426-01fb3b5d4402', 'dd729cbc-17d2-49c4-8d70-9b16ea392638', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', 'as', 'approved', '28', '2020-08-27 11:04:29.000000', NULL, NULL);
INSERT INTO `rumah_dinas` VALUES ('72847694-fea9-4538-bed3-9d08880b998f', 'ebfe0347-8273-4e86-982e-3fbe0edcff04', 'beb6da00-6c4b-4d3b-9607-58f3485df1ef', NULL, 'cancel', '', '2020-08-03 03:11:51.668770', NULL, NULL);
INSERT INTO `rumah_dinas` VALUES ('843580b8-b1a3-400e-96c0-ddabbe9826f8', 'ebfe0347-8273-4e86-982e-3fbe0edcff04', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', NULL, NULL, '1111', '2020-08-04 06:37:07.000000', NULL, NULL);
INSERT INTO `rumah_dinas` VALUES ('a62774d8-f81c-4460-88dc-07204007cea7', 'c5bafed3-2922-488d-87fe-2c9f30f32047', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', NULL, 'approved', '1111', '2020-08-04 05:11:38.000000', NULL, NULL);
INSERT INTO `rumah_dinas` VALUES ('b43af1f7-c8f9-4a15-ae64-b2cea3e9d7dc', 'ebfe0347-8273-4e86-982e-3fbe0edcff04', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', NULL, NULL, '1111', '2020-08-04 05:11:46.000000', NULL, NULL);
INSERT INTO `rumah_dinas` VALUES ('b7c73024-9f46-4c07-901c-032426df673f', 'dd729cbc-17d2-49c4-8d70-9b16ea392638', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', NULL, NULL, '1111', '2020-08-05 14:32:24.000000', NULL, NULL);
INSERT INTO `rumah_dinas` VALUES ('e7c87e7f-972d-4c6a-8b8a-96ed19d12e37', 'c5bafed3-2922-488d-87fe-2c9f30f32047', '36c7d08d-a91b-4268-a8b9-cf418a0adea5', NULL, 'approved', '', '2020-08-03 03:11:44.288182', NULL, NULL);

-- ----------------------------
-- Table structure for spbu
-- ----------------------------
DROP TABLE IF EXISTS `spbu`;
CREATE TABLE `spbu`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `spbu` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `alamat` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of spbu
-- ----------------------------
INSERT INTO `spbu` VALUES ('0d8fb378-7331-489f-9b35-ef1f60174ea4', 'pamularsihh', 'semarang', '', '1111', '2020-09-07 10:42:27.105680', NULL, NULL);
INSERT INTO `spbu` VALUES ('4ad24101-e3ff-42c4-8a42-374da9bd5417', 'gunung pati', 'semarang', '', '1111', '2020-09-07 10:42:21.000000', NULL, NULL);
INSERT INTO `spbu` VALUES ('f3cfd1ae-ab44-4a60-bf9e-4b4f039795f3', 'bawenn', 'bawen', '', '1111', '2020-09-07 10:42:33.755842', NULL, NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `nik` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `role` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('14638918-6edb-4d4c-961b-d9cdd2ff3472', '90', 'arya', 'arya@gmail.com', '90', 'mechanical_engineer', '1111', '2020-09-01 20:35:34.000000', NULL, NULL);
INSERT INTO `users` VALUES ('2e93a28a-87a1-4f8f-a3f2-0146a60f3f3f', '5555', 'Hussein', 'hussein@gmail.com', '5555', 'driver', '1111', '2020-09-01 19:53:15.000000', NULL, NULL);
INSERT INTO `users` VALUES ('3b8d5a9a-717b-4e27-9b9b-f0ea96afc839', '27', 'nanda', 'nanda@gmail.com', '27', 'penghuni_rumah', '1111', '2020-09-01 20:24:33.000000', NULL, NULL);
INSERT INTO `users` VALUES ('9987dc98-8237-4727-833c-c8f638704a55', '28', 'mm', 'math@gmail', '28', 'mechanical_engineer', '1111', '2020-08-09 15:36:36.000000', NULL, NULL);
INSERT INTO `users` VALUES ('a', '1111', 'super admin', 'tes@tes.com', '1111', 'super_admin', 'superadmin', '2020-06-21 13:49:22.519552', NULL, NULL);
INSERT INTO `users` VALUES ('b3384fc9-fbef-4bdd-af71-214f26ea2104', '2222', 'mat', 'mat@tes.com', 'hus', 'Admin', '', '2020-07-07 22:08:43.250602', '1111', '2020-07-30 15:54:43.000000');
INSERT INTO `users` VALUES ('d5b28d86-7be0-4bc5-8916-06fe119f3759', '5', 'math', 'math@gmail', '5555', 'admin', '1111', '2020-07-30 15:58:07.000000', NULL, NULL);

-- ----------------------------
-- Table structure for vendor
-- ----------------------------
DROP TABLE IF EXISTS `vendor`;
CREATE TABLE `vendor`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `vendor` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `alamat` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kategori` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `deleted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(6) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of vendor
-- ----------------------------
INSERT INTO `vendor` VALUES ('04681d02-ba78-4494-b99e-763c941948af', 'al', 'semarang', 'mesin,', '', '1111', '2020-09-17 20:18:14.047742', NULL, NULL);
INSERT INTO `vendor` VALUES ('4569e289-1997-477d-be2a-b5ce5b0f7bd2', 'hu', 'qwe', 'rumah_dinas,', '', '1111', '2020-09-17 20:18:15.948771', NULL, NULL);
INSERT INTO `vendor` VALUES ('7c02038f-28fc-4efa-82d9-2eb2c6d3336b', 's', 'semarang', 'mesin,rumah_dinas,', '', '1111', '2020-09-17 20:18:31.852106', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
