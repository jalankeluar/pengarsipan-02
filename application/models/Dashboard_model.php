<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Kendaraan_model extends CI_Model
{
  //======================================== Kendaran ===========================================
  public function all_kendaraan_count($role)
  {
    $nik  = $this->session->userdata('nik');

    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->where('kendaraan.deleted_at',null)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('users','users.nik = kendaraan.created_by','left')
        ->select('kendaraan.created_by, max(pengajuan_date) as max, k_nopolisi.no_polisi, users.name')
        ->group_by('k_nopolisi.no_polisi')
        ->get('kendaraan');
    }
    else{
      $query = $this
        ->db
        ->where('kendaraan.deleted_at',null)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('users','users.nik = kendaraan.created_by','left')
        ->select('kendaraan.created_by, max(pengajuan_date) as max, k_nopolisi.no_polisi, users.name')
        ->group_by('k_nopolisi.no_polisi')
        ->where('created_by',$nik)
        ->get('kendaraan');
    }

    return $query->num_rows();
  }

  public function all_kendaraan_data($limit, $start, $col, $dir, $role)
  {
    $nik  = $this->session->userdata('nik');
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('kendaraan.deleted_at',null)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('users','users.nik = kendaraan.created_by','left')
        ->select('kendaraan.created_by, max(pengajuan_date) as max, k_nopolisi.no_polisi, users.name')
        ->group_by('k_nopolisi.no_polisi')
        ->get('kendaraan');
    }
    else{
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('kendaraan.deleted_at',null)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('users','users.nik = kendaraan.created_by','left')
        ->select('kendaraan.created_by, max(pengajuan_date) as max, k_nopolisi.no_polisi, users.name')
        ->group_by('k_nopolisi.no_polisi')
        ->where('created_by',$nik)
        ->get('kendaraan');
    }
    
    //atau

    // $query = $this->db->query("SELECT k.*, ks.servis from kendaraan k
    // left join k_service ks on ks.id = k.service
    // where deleted_at is null
    // order by $col $dir limit $limit $start ");


    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_kendaraan_count($search, $role)
  {
    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->like('k_nopolisi.no_polisi', $search)
        ->where('kendaraan.deleted_at',null)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('users','users.nik = kendaraan.created_by','left')
        ->select('kendaraan.created_by, max(pengajuan_date) as max, k_nopolisi.no_polisi, users.name')
        ->group_by('k_nopolisi.no_polisi')
        ->get('kendaraan');
    }
    else{
      $query = $this
      ->db
      ->like('k_nopolisi.no_polisi', $search)
      ->where('kendaraan.created_by',$nik)
      ->where('kendaraan.deleted_at',null)
      ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
      ->join('users','users.nik = kendaraan.created_by','left')
      ->select('kendaraan.created_by, max(pengajuan_date) as max, k_nopolisi.no_polisi, users.name')
      ->group_by('k_nopolisi.no_polisi')
      ->get('kendaraan');
    }

    return $query->num_rows();
  }

  public function search_kendaraan_data($limit, $start, $col, $dir, $search, $role)
  {
    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->like('k_nopolisi.no_polisi', $search)
        ->where('kendaraan.deleted_at',null)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('users','users.nik = kendaraan.created_by','left')
        ->select('kendaraan.created_by, max(pengajuan_date) as max, k_nopolisi.no_polisi, users.name')
        ->group_by('k_nopolisi.no_polisi')
        ->get('kendaraan');
    }
    else{
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->like('k_nopolisi.no_polisi', $search)
        ->where('kendaraan.created_by',$nik)
        ->where('kendaraan.deleted_at',null)
        ->join('k_nopolisi','k_nopolisi.id = kendaraan.no_polisi','left')
        ->join('users','users.nik = kendaraan.created_by','left')
        ->select('kendaraan.created_by, max(pengajuan_date) as max, k_nopolisi.no_polisi, users.name')
        ->group_by('k_nopolisi.no_polisi')
        ->get('kendaraan');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }
  //============================================ RUMAH =========================================
  public function all_rumah_count($role)
  {
    $nik  = $this->session->userdata('nik');

    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->where('rumah_dinas.deleted_at',null)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.created_by, max(rumah_dinas.created_at) as max, rd_alamat.alamat, users.name')
        ->group_by('rd_alamat.alamat')
        ->get('rumah_dinas');
    }
    else{
      $query = $this
        ->db
        ->where('rumah_dinas.created_by',$nik)
        ->where('rumah_dinas.deleted_at',null)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.created_by, max(rumah_dinas.created_at) as max, rd_alamat.alamat, users.name')
        ->group_by('rd_alamat.alamat')
        ->get('rumah_dinas');
    }

    return $query->num_rows();
  }

  public function all_rumah_data($limit, $start, $col, $dir, $role)
  {
    $nik  = $this->session->userdata('nik');
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('rumah_dinas.deleted_at',null)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.created_by, max(rumah_dinas.created_at) as max, rd_alamat.alamat, users.name')
        ->group_by('rd_alamat.alamat')
        ->get('rumah_dinas');
    }
    else{
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->where('rumah_dinas.created_by',$nik)
        ->where('rumah_dinas.deleted_at',null)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.created_by, max(rumah_dinas.created_at) as max, rd_alamat.alamat, users.name')
        ->group_by('rd_alamat.alamat')
        ->get('rumah_dinas');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_rumah_count($search, $role)
  {
    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->like('rd_alamat.alamat', $search)
        ->where('rumah_dinas.deleted_at',null)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.created_by, max(rumah_dinas.created_at) as max, rd_alamat.alamat, users.name')
        ->group_by('rd_alamat.alamat')
        ->get('rumah_dinas');
    }
    else{
      $query = $this
      ->db
      ->like('rd_alamat.alamat', $search)
      ->where('rumah_dinas.created_by',$nik)
      ->where('rumah_dinas.deleted_at',null)
      ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
      ->join('users','users.nik = rumah_dinas.created_by','left')
      ->select('rumah_dinas.created_by, max(rumah_dinas.created_at) as max, rd_alamat.alamat, users.name')
      ->group_by('rd_alamat.alamat')
      ->get('rumah_dinas');
    }

    return $query->num_rows();
  }

  public function search_rumah_data($limit, $start, $col, $dir, $search, $role)
  {
    $nik  = $this->session->userdata('nik');
    
    if($role == 'admin' || $role =='super_admin'){
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->like('rd_alamat.alamat', $search)
        ->where('rumah_dinas.deleted_at',null)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.created_by, max(rumah_dinas.created_at) as max, rd_alamat.alamat, users.name')
        ->group_by('rd_alamat.alamat')
        ->get('rumah_dinas');
    }
    else{
      $query = $this
        ->db
        ->limit($limit, $start)
        ->order_by($col, $dir)
        ->like('rd_alamat.alamat', $search)
        ->where('rumah_dinas.created_by',$nik)
        ->where('rumah_dinas.deleted_at',null)
        ->join('rd_alamat','rd_alamat.id = rumah_dinas.alamat','left')
        ->join('users','users.nik = rumah_dinas.created_by','left')
        ->select('rumah_dinas.created_by, max(rumah_dinas.created_at) as max, rd_alamat.alamat, users.name')
        ->group_by('rd_alamat.alamat')
        ->get('rumah_dinas');
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }
   //============================================ LANTAI =========================================
   public function all_lantai_count($role)
   {
     $nik  = $this->session->userdata('nik');
 
     if($role == 'admin' || $role =='super_admin'){
       $query = $this
         ->db
         ->where('gedung.deleted_at',null)
         ->join('g_lantai','g_lantai.id = gedung.lantai','left')
         ->join('users','users.nik = gedung.created_by','left')
         ->select('gedung.created_by, max(gedung.created_at) as max, g_lantai.lantai, users.name')
         ->group_by('g_lantai.lantai')
         ->get('gedung');
     }
     else{
       $query = $this
         ->db
         ->where('gedung.created_by',$nik)
         ->where('gedung.deleted_at',null)
         ->join('g_lantai','g_lantai.id = gedung.lantai','left')
         ->join('users','users.nik = gedung.created_by','left')
         ->select('gedung.created_by, max(gedung.created_at) as max, g_lantai.lantai, users.name')
         ->group_by('g_lantai.lantai')
         ->get('gedung');
     }
 
     return $query->num_rows();
   }
 
   public function all_lantai_data($limit, $start, $col, $dir, $role)
   {
     $nik  = $this->session->userdata('nik');
     if($role == 'admin' || $role =='super_admin'){
       $query = $this
         ->db
         ->limit($limit, $start)
         ->order_by($col, $dir)
         ->where('gedung.deleted_at',null)
         ->join('g_lantai','g_lantai.id = gedung.lantai','left')
         ->join('users','users.nik = gedung.created_by','left')
         ->select('gedung.created_by, max(gedung.created_at) as max, g_lantai.lantai, users.name')
         ->group_by('g_lantai.lantai')
         ->get('gedung');
     }
     else{
       $query = $this
         ->db
         ->limit($limit, $start)
         ->order_by($col, $dir)
         ->where('gedung.created_by',$nik)
         ->where('gedung.deleted_at',null)
         ->join('g_lantai','g_lantai.id = gedung.lantai','left')
         ->join('users','users.nik = gedung.created_by','left')
         ->select('gedung.created_by, max(gedung.created_at) as max, g_lantai.lantai, users.name')
         ->group_by('g_lantai.lantai')
         ->get('gedung');
     }
 
     if ($query->num_rows() > 0) {
       return $query->result();
     } else {
       return null;
     }
   }
 
   public function search_lantai_count($search, $role)
   {
     $nik  = $this->session->userdata('nik');
     
     if($role == 'admin' || $role =='super_admin'){
       $query = $this
         ->db
         ->like('g_lantai.lantai', $search)
         ->where('gedung.deleted_at',null)
         ->join('g_lantai','g_lantai.id = gedung.lantai','left')
         ->join('users','users.nik = gedung.created_by','left')
         ->select('gedung.created_by, max(gedung.created_at) as max, g_lantai.lantai, users.name')
         ->group_by('g_lantai.lantai')
         ->get('gedung');
     }
     else{
       $query = $this
       ->db
       ->like('g_lantai.lantai', $search)
       ->where('gedung.created_by',$nik)
       ->where('gedung.deleted_at',null)
       ->join('g_lantai','g_lantai.id = gedung.lantai','left')
       ->join('users','users.nik = gedung.created_by','left')
       ->select('gedung.created_by, max(gedung.created_at) as max, g_lantai.lantai, users.name')
       ->group_by('g_lantai.lantai')
       ->get('gedung');
     }
 
     return $query->num_rows();
   }
 
   public function search_lantai_data($limit, $start, $col, $dir, $search, $role)
   {
     $nik  = $this->session->userdata('nik');
     
     if($role == 'admin' || $role =='super_admin'){
       $query = $this
         ->db
         ->limit($limit, $start)
         ->order_by($col, $dir)
         ->like('g_lantai.lantai', $search)
         ->where('gedung.deleted_at',null)
         ->join('g_lantai','g_lantai.id = gedung.lantai','left')
         ->join('users','users.nik = gedung.created_by','left')
         ->select('gedung.created_by, max(gedung.created_at) as max, g_lantai.lantai, users.name')
         ->group_by('g_lantai.lantai')
         ->get('gedung');
     }
     else{
       $query = $this
         ->db
         ->limit($limit, $start)
         ->order_by($col, $dir)
         ->like('g_lantai.lantai', $search)
         ->where('gedung.created_by',$nik)
         ->where('gedung.deleted_at',null)
         ->join('g_lantai','g_lantai.id = gedung.lantai','left')
         ->join('users','users.nik = gedung.created_by','left')
         ->select('gedung.created_by, max(gedung.created_at) as max, g_lantai.lantai, users.name')
         ->group_by('g_lantai.lantai')
         ->get('gedung');
     }
 
     if ($query->num_rows() > 0) {
       return $query->result();
     } else {
       return null;
     }
   }
   
   //============================================ MESIN =========================================
   public function all_mesin_count($role)
   {
     $nik  = $this->session->userdata('nik');
 
     if($role == 'admin' || $role =='super_admin'){
       $query = $this
         ->db
         ->where('mesin.deleted_at',null)
         ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
         ->join('users','users.nik = mesin.created_by','left')
         ->select('mesin.created_by, max(mesin.created_at) as max, m_jenis_mesin.jenis_mesin, users.name')
         ->group_by('m_jenis_mesin.jenis_mesin')
         ->get('mesin');
     }
     else{
       $query = $this
         ->db
         ->where('mesin.created_by',$nik)
         ->where('mesin.deleted_at',null)
         ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
         ->join('users','users.nik = mesin.created_by','left')
         ->select('mesin.created_by, max(mesin.created_at) as max, m_jenis_mesin.jenis_mesin, users.name')
         ->group_by('m_jenis_mesin.jenis_mesin')
         ->get('mesin');
     }
 
     return $query->num_rows();
   }
 
   public function all_mesin_data($limit, $start, $col, $dir, $role)
   {
     $nik  = $this->session->userdata('nik');
     if($role == 'admin' || $role =='super_admin'){
       $query = $this
         ->db
         ->limit($limit, $start)
         ->order_by($col, $dir)
         ->where('mesin.deleted_at',null)
         ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
         ->join('users','users.nik = mesin.created_by','left')
         ->select('mesin.created_by, max(mesin.created_at) as max, m_jenis_mesin.jenis_mesin, users.name')
         ->group_by('m_jenis_mesin.jenis_mesin')
         ->get('mesin');
     }
     else{
       $query = $this
         ->db
         ->limit($limit, $start)
         ->order_by($col, $dir)
         ->where('mesin.created_by',$nik)
         ->where('mesin.deleted_at',null)
         ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
         ->join('users','users.nik = mesin.created_by','left')
         ->select('mesin.created_by, max(mesin.created_at) as max, m_jenis_mesin.jenis_mesin, users.name')
         ->group_by('m_jenis_mesin.jenis_mesin')
         ->get('mesin');
     }
 
     if ($query->num_rows() > 0) {
       return $query->result();
     } else {
       return null;
     }
   }
 
   public function search_mesin_count($search, $role)
   {
     $nik  = $this->session->userdata('nik');
     
     if($role == 'admin' || $role =='super_admin'){
       $query = $this
         ->db
         ->like('m_jenis_mesin.jenis_mesin', $search)
         ->where('mesin.deleted_at',null)
         ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
         ->join('users','users.nik = mesin.created_by','left')
         ->select('mesin.created_by, max(mesin.created_at) as max, m_jenis_mesin.jenis_mesin, users.name')
         ->group_by('m_jenis_mesin.jenis_mesin')
         ->get('mesin');
     }
     else{
       $query = $this
       ->db
       ->like('m_jenis_mesin.jenis_mesin', $search)
       ->where('mesin.created_by',$nik)
       ->where('mesin.deleted_at',null)
       ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
       ->join('users','users.nik = mesin.created_by','left')
       ->select('mesin.created_by, max(mesin.created_at) as max, m_jenis_mesin.jenis_mesin, users.name')
       ->group_by('m_jenis_mesin.jenis_mesin')
       ->get('mesin');
     }
 
     return $query->num_rows();
   }
 
   public function search_mesin_data($limit, $start, $col, $dir, $search, $role)
   {
     $nik  = $this->session->userdata('nik');
     
     if($role == 'admin' || $role =='super_admin'){
       $query = $this
         ->db
         ->limit($limit, $start)
         ->order_by($col, $dir)
         ->like('m_jenis_mesin.jenis_mesin', $search)
         ->where('mesin.deleted_at',null)
         ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
         ->join('users','users.nik = mesin.created_by','left')
         ->select('mesin.created_by, max(mesin.created_at) as max, m_jenis_mesin.jenis_mesin, users.name')
         ->group_by('m_jenis_mesin.jenis_mesin')
         ->get('mesin');
     }
     else{
       $query = $this
         ->db
         ->limit($limit, $start)
         ->order_by($col, $dir)
         ->like('m_jenis_mesin.jenis_mesin', $search)
         ->where('mesin.created_by',$nik)
         ->where('mesin.deleted_at',null)
         ->join('m_jenis_mesin','m_jenis_mesin.id = mesin.jenis_mesin','left')
         ->join('users','users.nik = mesin.created_by','left')
         ->select('mesin.created_by, max(mesin.created_at) as max, m_jenis_mesin.jenis_mesin, users.name')
         ->group_by('m_jenis_mesin.jenis_mesin')
         ->get('mesin');
     }
 
     if ($query->num_rows() > 0) {
       return $query->result();
     } else {
       return null;
     }
   }
}
